package rax.alerter

import android.animation.ObjectAnimator
import android.annotation.TargetApi
import android.app.Activity
import android.content.Context
import android.graphics.Rect
import android.graphics.drawable.Drawable
import android.os.Build
import android.util.AttributeSet
import android.util.Log
import android.view.*
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.FrameLayout
import androidx.annotation.ColorInt
import androidx.annotation.DrawableRes
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.constraintlayout.widget.ConstraintSet
import androidx.core.view.ViewCompat
import com.sachtech.echelon.widget.alerter.SwipeDismissTouchListener

import kotlinx.android.synthetic.main.alerter_alert_view.view.*


/**
 * Custom Alert View
 *
 * @author Kevin Murphy, Tapadoo, Dublin, Ireland, Europe, Earth.
 * @since 26/01/2016
 * @changed Akash Saggu - akashsaggu@protonmail.com
 */
class Alert @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyle: Int = 0
) : FrameLayout(context, attrs, defStyle), View.OnClickListener, Animation.AnimationListener,
    SwipeDismissTouchListener.DismissCallbacks {

    private var onShowListener: OnShowAlertListener? = null
    internal var onHideListener: OnHideAlertListener? = null

    internal var enterAnimation: Animation =
        AnimationUtils.loadAnimation(context, R.anim.alerter_slide_in_from_top)
    internal var exitAnimation: Animation =
        AnimationUtils.loadAnimation(context, R.anim.alerter_slide_out_to_top)

    internal var duration =
        DISPLAY_TIME_IN_SECONDS

    private var enableInfiniteDuration: Boolean = false

    private var runningAnimation: Runnable? = null

    private var isDismissible = true

    private var hasDimBackground = true

    //fixme change to view
    private var llAlertBackground: ViewGroup? = null

    /**
     * Flag to ensure we only set the margins once
     */
    private var marginSet: Boolean = false

    /**
     * Flag to enable / disable haptic feedback
     */
    private var vibrationEnabled = true

    /**
     * Get and Sets the Vertical bias of the Alert
     */
    var verticalBias: Float
        get() {
            return (llAlertBackground?.layoutParams as? ConstraintLayout.LayoutParams)?.verticalBias
                ?: 0.0f
        }
        set(bias) {
            val params = llAlertBackground?.layoutParams as? ConstraintLayout.LayoutParams
            params?.topToTop = ConstraintSet.PARENT_ID
            params?.bottomToBottom = ConstraintSet.PARENT_ID
            params?.verticalBias = bias
            llAlertBackground?.layoutParams = params
            if (bias > 0.5f) {
                enterAnimation =
                    AnimationUtils.loadAnimation(context, R.anim.alerter_slide_in_from_bottom)
                exitAnimation =
                    AnimationUtils.loadAnimation(context, R.anim.alerter_slide_out_to_bottom)
            }
            llAlertBackground?.requestLayout()
        }

    init {
        inflate(context, R.layout.alerter_alert_view, this)
        isHapticFeedbackEnabled = true
        ViewCompat.setTranslationZ(this, Integer.MAX_VALUE.toFloat())
    }

    override fun onAttachedToWindow() {
        super.onAttachedToWindow()

        enterAnimation.setAnimationListener(this)
        showDimBackground()
        // Set Animation to be Run when View is added to Window
        animation = enterAnimation

    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        if (!marginSet) {
            marginSet = true

            // Add a negative top margin to compensate for overshoot enter animation
            val rectangle = Rect()
            val window = (context as? Activity)?.window
            window?.decorView?.getWindowVisibleDisplayFrame(rectangle)

            val navigationBarHeight = window?.decorView?.bottom!! - rectangle.bottom
            val statusBarHeight = rectangle.top
            val params = layoutParams as ViewGroup.MarginLayoutParams
            params.topMargin = statusBarHeight
            params.bottomMargin = navigationBarHeight
        }

        super.onMeasure(widthMeasureSpec, heightMeasureSpec)
    }

    // Release resources once view is detached.
    override fun onDetachedFromWindow() {
        super.onDetachedFromWindow()

        enterAnimation.setAnimationListener(null)
    }

    /* Override Methods */

    override fun onTouchEvent(event: MotionEvent): Boolean {
        super.performClick()
        return super.onTouchEvent(event)
    }

    override fun onClick(v: View) {
        if (isDismissible) {
            hide()
        }
    }

    override fun setOnClickListener(listener: View.OnClickListener?) {
        llAlertBackground?.setOnClickListener(listener)
    }

    override fun setVisibility(visibility: Int) {
        super.setVisibility(visibility)
        for (i in 0 until childCount) {
            getChildAt(i).visibility = visibility
        }
    }

    /* Interface Method Implementations */

    override fun onAnimationStart(animation: Animation) {
        if (!isInEditMode) {
            visibility = View.VISIBLE

            if (vibrationEnabled) {
                performHapticFeedback(HapticFeedbackConstants.VIRTUAL_KEY)
            }
        }
    }

    override fun onAnimationEnd(animation: Animation) {
        onShowListener?.onShow()

        startHideAnimation()
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    private fun startHideAnimation() {
        //Start the Handler to clean up the Alert
        if (!enableInfiniteDuration) {
            runningAnimation = Runnable { hide() }

            postDelayed(runningAnimation, duration)
        }
    }

    override fun onAnimationRepeat(animation: Animation) {
        //Ignore
    }

    /* Clean Up Methods */

    /**
     * Cleans up the currently showing alert view.
     */
    private fun hide() {
        try {
            exitAnimation.setAnimationListener(object : Animation.AnimationListener {
                override fun onAnimationStart(animation: Animation) {
                    llAlertBackground?.setOnClickListener(null)
                    llAlertBackground?.isClickable = false
                    removeDimBackground()
                }

                override fun onAnimationEnd(animation: Animation) {
                    removeFromParent()
                }

                override fun onAnimationRepeat(animation: Animation) {
                    //Ignore
                }
            })

            startAnimation(exitAnimation)
        } catch (ex: Exception) {
            Log.e(javaClass.simpleName, Log.getStackTraceString(ex))
        }
    }

    /**
     * Removes Alert View from its Parent Layout
     */
    internal fun removeFromParent() {
        clearAnimation()
        visibility = View.GONE

        postDelayed(object : Runnable {
            override fun run() {
                try {
                    if (parent != null) {
                        try {
                            (parent as ViewGroup).removeView(this@Alert)

                            onHideListener?.onHide()
                        } catch (ex: Exception) {
                            Log.e(javaClass.simpleName, "Cannot remove from parent layout")
                        }
                    }
                } catch (ex: Exception) {
                    Log.e(javaClass.simpleName, Log.getStackTraceString(ex))
                }
            }
        }, CLEAN_UP_DELAY_MILLIS.toLong())
    }

    /* Setters and Getters */

    /**
     * Sets the Alert Background colour
     *
     * @param color The qualified colour integer
     */
    fun setAlertBackgroundColor(@ColorInt color: Int) {
        llAlertBackground?.setBackgroundColor(color)
    }

    /**
     * Sets the Alert Background Drawable Resource
     *
     * @param resource The qualified drawable integer
     */
    fun setAlertBackgroundResource(@DrawableRes resource: Int) {
        llAlertBackground?.setBackgroundResource(resource)
    }

    /**
     * Sets the Alert Background Drawable
     *
     * @param drawable The qualified drawable
     */
    fun setAlertBackgroundDrawable(drawable: Drawable) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            llAlertBackground?.background = drawable
        } else {
            llAlertBackground?.setBackgroundDrawable(drawable)
        }
    }

    /**
     * Disable touches while the Alert is showing
     */
    fun disableOutsideTouch() {
        flClickShield.isClickable = true
    }


    /**
     * Set if the alerter is isDismissible or not
     *
     * @param dismissible True if alert can be dismissed
     */
    fun setDismissible(dismissible: Boolean) {
        this.isDismissible = dismissible
    }

    /**
     * Get if the alert is isDismissible
     * @return
     */
    fun isDismissable(): Boolean {
        return isDismissible
    }

    /**
     * Set whether to enable swipe to dismiss or not
     */
    fun enableSwipeToDismiss() {
        llAlertBackground.let {
            it?.setOnTouchListener(
                SwipeDismissTouchListener(
                    it,
                    object : SwipeDismissTouchListener.DismissCallbacks {
                        override fun canDismiss(): Boolean {
                            return true
                        }

                        override fun onDismiss(view: View) {
                            removeFromParent()
                        }

                        override fun onTouch(view: View, touch: Boolean) {
                            // Ignore
                        }
                    })
            )
        }
    }

    /**
     * Set if the duration of the alert is infinite
     *
     * @param enableInfiniteDuration True if the duration of the alert is infinite
     */
    fun setEnableInfiniteDuration(enableInfiniteDuration: Boolean) {
        this.enableInfiniteDuration = enableInfiniteDuration
    }

    /**
     * Set the alert's listener to be fired on the alert being fully shown
     *
     * @param listener Listener to be fired
     */
    fun setOnShowListener(listener: OnShowAlertListener) {
        this.onShowListener = listener
    }

    /**
     * Enable or Disable haptic feedback
     *
     * @param vibrationEnabled True to enable, false to disable
     */
    fun setVibrationEnabled(vibrationEnabled: Boolean) {
        this.vibrationEnabled = vibrationEnabled
    }

    override fun canDismiss(): Boolean {
        return isDismissible
    }

    override fun onDismiss(view: View) {
        flClickShield?.removeView(llAlertBackground)
    }

    override fun onTouch(view: View, touch: Boolean) {
        if (touch) {
            removeCallbacks(runningAnimation)
        } else {
            startHideAnimation()
        }
    }

    fun setContentView(layoutRes: Int): View {
        val contentView = LayoutInflater.from(context).inflate(layoutRes, flClickShield, false)
        llAlertBackground = contentView.rootView as? ViewGroup
        flClickShield.addView(contentView)
        llAlertBackground?.setOnClickListener(this)
        return contentView.rootView
    }

    fun disableDimBackground() {
        hasDimBackground = false
    }

    private fun showDimBackground() {
        if (hasDimBackground)
            with(ObjectAnimator.ofArgb(flClickShield, "backgroundColor", 0x00000000, 0x48013241)) {
                startDelay = 150
                duration = 500
                start()
            }
    }

    private fun removeDimBackground() {
        if (hasDimBackground)
            with(
                ObjectAnimator.ofArgb(
                    flClickShield,
                    "backgroundColor",
                    0x48013241,
                    0x00000000
                )
            ) {
                duration = 300
                start()
            }
    }

    companion object {

        private const val CLEAN_UP_DELAY_MILLIS = 100

        /**
         * The amount of time the alert will be visible on screen in seconds
         */
        private const val DISPLAY_TIME_IN_SECONDS: Long = 3000
    }
}
