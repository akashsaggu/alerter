package com.sachtech.zozocar.util.rxkotlin

/**
 * Created by Akash Saggu(R4X) on 17/9/18 at 15:53.
 * akashsaggu@protonmail.com
 * @Version 1 (17/9/18)
 * @Updated on 17/9/18
 */
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.subjects.BehaviorSubject

object EventBus {

    private val publisher = BehaviorSubject.create<Any>()


    fun publish(event: Any) {
        publisher.onNext(event)
    }

    // Listen should return an Observable and not the publisher
    // Using ofType we filter only events that match that class type
    fun <T> listen(eventType: Class<T>): Observable<T> = publisher.ofType(eventType)
        .observeOn(AndroidSchedulers.mainThread())
}