package com.sachtech.zozocar.util.reference

import java.lang.ref.SoftReference
import kotlin.reflect.KProperty


fun <T> soft() = SoftReferenceDelegate<T>()

fun <T> soft(value: () -> T) = SoftReferenceDelegate(value)

fun <T> soft(value: T) = SoftReferenceDelegate(value)

class SoftReferenceDelegate<T> {
    private var softReference: SoftReference<T>? = null

    constructor()
    constructor(value: () -> T) {
        softReference = SoftReference(value.invoke())
    }

    constructor(value: T) {
        softReference = SoftReference(value)
    }

    operator fun getValue(thisRef: Any, property: KProperty<*>): T? = softReference?.get()
    operator fun setValue(thisRef: Any, property: KProperty<*>, value: T) {
        softReference = SoftReference(value)
    }
}