package com.sachtech.zozocar.util.pagination

import androidx.core.widget.NestedScrollView


/**
 * Created by Akash Saggu(R4X) on 6/9/18 at 18:25.
 * akashsaggu@protonmail.com
 * @Version 1 (6/9/18)
 * @Updated on 6/9/18
 */
/**
 * For NestedScrollView
 * */
class ReachEndScrollListner(private val func: (page: String) -> Unit) :
    NestedScrollView.OnScrollChangeListener {
    private var currentPage = 1

    override fun onScrollChange(
        v: NestedScrollView?,
        scrollX: Int,
        scrollY: Int,
        oldScrollX: Int,
        oldScrollY: Int
    ) {
        if (v!!.getChildAt(v.childCount - 1) != null) {
            if ((scrollY >= (v.getChildAt(v.childCount - 1).measuredHeight - v.measuredHeight)) &&
                scrollY > oldScrollY
            ) {
                currentPage += 1
                func.invoke(currentPage.toString())
            }
        }
    }
}