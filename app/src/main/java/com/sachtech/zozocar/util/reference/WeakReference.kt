package com.sachtech.zozocar.util.reference

import java.lang.ref.WeakReference
import kotlin.reflect.KProperty

/**
 * Created by Akash Saggu(R4X) on 23/8/18 at 14:24.
 * akashsaggu@protonmail.com
 * @Version 1
 * @Updated on 23/8/18
 */

fun <T> weak() = WeakReferenceDelegate<T>()

fun <T> weak(value: () -> T) = WeakReferenceDelegate(value)

fun <T> weak(value: T) = WeakReferenceDelegate(value)

class WeakReferenceDelegate<T> {
    private var weakReference: WeakReference<T>? = null

    constructor()
    constructor(value: () -> T) {
        weakReference = WeakReference(value.invoke())
    }

    constructor(value: T) {
        weakReference = WeakReference(value)
    }

    operator fun getValue(thisRef: Any, property: KProperty<*>): T? = weakReference?.get()
    operator fun setValue(thisRef: Any, property: KProperty<*>, value: T) {
        weakReference = WeakReference(value)
    }
}