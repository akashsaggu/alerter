package com.sachtech.zozocar.util.recyclerview

interface ItemClickListener<T> {
    fun onItemClick(item: T)
}