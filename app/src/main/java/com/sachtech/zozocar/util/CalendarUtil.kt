package com.sachtech.zozocar.util


import java.text.SimpleDateFormat
import java.util.*


object CalendarUtil {

    fun getStartOfDay(): Long {
        val cal = Calendar.getInstance()
        with(cal) {
            set(Calendar.HOUR_OF_DAY, 0)
            set(Calendar.MINUTE, 0)
            set(Calendar.SECOND, 0)
            set(Calendar.MILLISECOND, 0)
        }
        return cal.timeInMillis
    }

    fun getEndOfDay(): Long {
        val cal = Calendar.getInstance()
        with(cal) {
            set(Calendar.HOUR_OF_DAY, 23)
            set(Calendar.MINUTE, 59)
            set(Calendar.SECOND, 59)
            set(Calendar.MILLISECOND, 999)
        }
        return cal.timeInMillis
    }

    fun isSameDay(cal1: Calendar?, cal2: Calendar?): Boolean {
        if (cal1 == null || cal2 == null) {
            throw IllegalArgumentException("The dates must not be null")
        }
        return cal1[Calendar.ERA] == cal2[Calendar.ERA] &&
                cal1[Calendar.YEAR] == cal2[Calendar.YEAR] &&
                cal1[Calendar.DAY_OF_YEAR] == cal2[Calendar.DAY_OF_YEAR]
    }

    fun isToday(timeStamp: Long): Boolean {
        return isSameDay(Calendar.getInstance().apply { timeInMillis = timeStamp }
            , Calendar.getInstance())
    }

    fun isYesterday(timeStamp: Long): Boolean {
        return isSameDay(Calendar.getInstance().apply { timeInMillis = timeStamp },
            Calendar.getInstance().apply { add(Calendar.DAY_OF_YEAR, -1) })
    }

}

fun Calendar.getDateString(seperator: String = "-"): String {
    return "" + get(Calendar.DAY_OF_MONTH) + seperator + get(Calendar.MONTH) + seperator + get(
        Calendar.YEAR
    )
}

fun getCurrentTimeString(): String {
    return SimpleDateFormat("yyyy-MM-dd").format(Date())
}

fun getDateWithCustomTime(p1: Int, p2: Int): String {
    val date = Date()
    date.hours = p1
    date.minutes = p2
    return SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(date)
}

fun getTime(p1: Int, p2: Int): String {
    val date = Date()
    date.hours = p1
    date.minutes = p2
    return SimpleDateFormat("HH:mm").format(date)
}


