package com.sachtech.zozocar.util

import android.content.Context
import android.net.ConnectivityManager

import com.sachtech.zozocar.app.App

/**
 * Created by Akash Saggu(R4X) on 24-04-2018.
 */
object NetworkUtil {

    @JvmStatic
    fun isNetworkAvailable(): Boolean {
        val activeNetwork = (App.getApp()
            .getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager).activeNetworkInfo
        return activeNetwork != null && activeNetwork.isConnected
    }
}