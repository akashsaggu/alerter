package com.sachtech.zozocar.database.dao


import androidx.room.Dao
import androidx.room.Query
import com.sachtech.zozocar.database.entity.UserTest


/**
 * Created by Akash Saggu(R4X) on 31/8/18 at 15:52.
 * akashsaggu@protonmail.com
 * @Version 1
 * @Updated on 31/8/18
 */


@Dao
interface UserDao {

    @Query("SELECT * FROM user where id = 0")
    fun getUser(): UserTest?

}
