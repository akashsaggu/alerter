package com.sachtech.zozocar.database

import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.sachtech.zozocar.app.App
import com.sachtech.zozocar.database.dao.UserDao
import com.sachtech.zozocar.database.entity.UserTest

@Database(entities = [UserTest::class], version = 1)
/*@TypeConverters(value = [LoginTypeConverter::class])*/
abstract class ZozoDatabase : RoomDatabase() {

    abstract fun userDao(): UserDao


    companion object {
        private var INSTANCE: ZozoDatabase? = null
        fun getInstance(): ZozoDatabase? {
            if (INSTANCE == null) {
                synchronized(ZozoDatabase::class) {
                    INSTANCE = Room.databaseBuilder(
                        App.getApp(),
                        ZozoDatabase::class.java, "zozocar.db"
                    )
                        /*.openHelperFactory(factory)*/
                        .allowMainThreadQueries()
                        .build()
                }
            }
            return INSTANCE
        }

        fun destroyInstance() {
            INSTANCE = null
        }
    }

}