package com.sachtech.zozocar.database.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey


/**
 * Created by Akash Saggu(R4X) on 31/8/18 at 15:06.
 * akashsaggu@protonmail.com
 * @Version 1
 * @Updated on 9/11/18
 */

/*@ColumnInfo(
    name = "loginType",
    typeAffinity = ColumnInfo.BLOB
)*/

@Entity(tableName = "user")
data class UserTest(
    @ColumnInfo(name = "id") @PrimaryKey val id: String = "0",
    @ColumnInfo(name = "name") val name: String = "",
    @ColumnInfo(name = "email") val email: String = "",
    @ColumnInfo(name = "phone") val phone: String = "",
    @ColumnInfo(name = "socialId") val socialId: String = ""
)

