package com.sachtech.zozocar.database.helper

import com.sachtech.zozocar.database.ZozoDatabase
import com.sachtech.zozocar.database.dao.UserDao

object DatabaseHelper {

    @Suppress("UNCHECKED_CAST")
    inline fun <reified T> getDao(): T? {
        return when (T::class) {
            UserDao::class -> {
                ZozoDatabase.getInstance()!!.userDao() as T
            }
            else -> return null
        }
    }
}
