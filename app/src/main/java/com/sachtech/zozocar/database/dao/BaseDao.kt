package com.sachtech.zozocar.database.dao

import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Update

interface BaseDao<in T> {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(data: T)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(vararg data: T)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(data: List<T>)

    @Update(onConflict = OnConflictStrategy.IGNORE)
    fun update(data: T)

    @Update(onConflict = OnConflictStrategy.ABORT)
    fun delete(data: T)

    @Update(onConflict = OnConflictStrategy.ABORT)
    fun delete(vararg data: T)

    @Update(onConflict = OnConflictStrategy.ABORT)
    fun deleteList(data: List<T>)
}

