package com.sachtech.zozocar.widget.ClearEditText;

public interface OnTextClearedListener {
    void onTextCleared();

    void onLeftIconClick();
}