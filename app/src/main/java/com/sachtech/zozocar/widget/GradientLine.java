package com.sachtech.zozocar.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.DashPathEffect;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.Shader;
import android.util.AttributeSet;
import android.view.View;


import com.sachtech.zozocar.R;

import java.lang.reflect.Field;

public class GradientLine extends View {

    static public int ORIENTATION_HORIZONTAL = 0;
    static public int ORIENTATION_VERTICAL = 1;
    private Paint mPaint;
    private int orientation;
    private LinearGradient gradient;

    public GradientLine(Context context, AttributeSet attrs) {
        super(context, attrs);
        int dashGap, dashLength, dashThickness;
        int startColor;
        int endColor;

        TypedArray a = context.getTheme().obtainStyledAttributes(attrs, R.styleable.GradientLine, 0, 0);

        try {
            dashGap = a.getDimensionPixelSize(R.styleable.GradientLine_dashGap, 5);
            dashLength = a.getDimensionPixelSize(R.styleable.GradientLine_dashLength, 5);
            dashThickness = a.getDimensionPixelSize(R.styleable.GradientLine_dashThickness, 3);
            startColor = a.getColor(R.styleable.GradientLine_startColor, 0xff000000);
            endColor = a.getColor(R.styleable.GradientLine_endColor, 0xff000000);
            orientation = a.getInt(R.styleable.GradientLine_orientation, ORIENTATION_HORIZONTAL);
        } finally {
            a.recycle();
        }

        mPaint = new Paint();
        mPaint.setAntiAlias(true);
        mPaint.setStyle(Paint.Style.STROKE);
        mPaint.setStrokeWidth(dashThickness);
        gradient = new LinearGradient(0, 0, 0, 150, new int[]{startColor, endColor}, null, Shader.TileMode.MIRROR);
        mPaint.setShader(gradient);
        mPaint.setPathEffect(new DashPathEffect(new float[]{dashLength, dashGap,}, 0));
    }

    public GradientLine(Context context) {
        this(context, null);
    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        super.onLayout(changed, left, top, right, bottom);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        try {
            Field field = gradient.getClass().getDeclaredField("mY1");
            field.setAccessible(true);
            field.set(gradient, getHeight());
            field.setAccessible(false);
        } catch (Exception e) {
        }
        if (orientation == ORIENTATION_HORIZONTAL) {
            float center = getHeight() * .5f;
            canvas.drawLine(0, center, getWidth(), center, mPaint);
        } else {
            float center = getWidth() * .5f;
            canvas.drawLine(center, 0, center, getHeight(), mPaint);
        }
    }
}