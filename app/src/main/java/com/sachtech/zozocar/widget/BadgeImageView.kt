package com.sachtech.echelon.widget

import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import android.util.AttributeSet
import android.widget.ImageView
import androidx.core.content.ContextCompat
import com.sachtech.zozocar.R

class BadgeImageView @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : ImageView(context, attrs, defStyleAttr) {


    private var paint: Paint = Paint()
    private var radius: Float = 2f

    init {
        initAttributes(context, attrs)
    }

    private fun initAttributes(context: Context, attrs: AttributeSet?) {
        var color: Int = ContextCompat.getColor(context, R.color.colorAccent)
        try {
            val a = context.theme.obtainStyledAttributes(attrs, R.styleable.BadgeImageView, 0, 0)
            color = a.getColor(
                R.styleable.BadgeImageView_badgeColor,
                ContextCompat.getColor(context, R.color.colorAccent)
            )
            radius = a.getDimension(R.styleable.BadgeImageView_badgeRadius, 2f)
            a.recycle()
        } catch (e: Exception) {
        }
        paint.color = color
        paint.isAntiAlias = true
    }

    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)
        val deltaX = right - radius
        val deltaY = top + radius
        canvas?.drawCircle(deltaX, deltaY, radius, paint)
    }
}