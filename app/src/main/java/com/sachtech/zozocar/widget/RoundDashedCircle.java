package com.sachtech.zozocar.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.DashPathEffect;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;

import androidx.core.content.ContextCompat;

import com.sachtech.zozocar.R;


public class RoundDashedCircle extends View {
    private Paint mPaint;
    private boolean animate;


    public RoundDashedCircle(Context context, AttributeSet attrs) {
        super(context, attrs);
        int dashGap, dashLength, dashThickness, color;
        TypedArray a = context.getTheme().obtainStyledAttributes(attrs, R.styleable.DashedCircle, 0, 0);

        try {
            dashGap = a.getDimensionPixelSize(R.styleable.DashedCircle_gap, 5);
            dashLength = a.getDimensionPixelSize(R.styleable.DashedCircle_length, 5);
            dashThickness = a.getDimensionPixelSize(R.styleable.DashedCircle_thickness, 3);
            color = a.getColor(R.styleable.DashedCircle_dashColor, ContextCompat.getColor(context, android.R.color.white));
        } finally {
            a.recycle();
        }
        mPaint = new Paint();
        mPaint.setAntiAlias(true);
        mPaint.setColor(color);
        mPaint.setStyle(Paint.Style.STROKE);
        mPaint.setStrokeWidth(dashThickness);
        mPaint.setPathEffect(new DashPathEffect(new float[]{dashLength, dashGap,}, 0));
    }

    public RoundDashedCircle(Context context) {
        this(context, null);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        canvas.drawCircle(getWidth() * .5f, getHeight() * .5f, getHeight() * .5f, mPaint);
    }
}