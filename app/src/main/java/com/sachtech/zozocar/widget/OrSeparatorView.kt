package com.sachtech.zozocar.widget

import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.Rect
import android.util.AttributeSet
import android.view.View
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import com.sachtech.zozocar.R
import com.sachtech.zozocar.util.extension.dpToPx
import kotlin.math.min


class OrSeparatorView @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : View(context, attrs, defStyleAttr) {


    private var linePaint: Paint = Paint()
    private var textPaint: Paint = Paint()
    private var textBounds: Rect = Rect()
    private val desireHeight = dpToPx(20)

    init {
        initAttributes(context, attrs)
    }

    private fun initAttributes(context: Context, attrs: AttributeSet?) {
        var lineColor: Int = ContextCompat.getColor(context, R.color.colorAccent)
        var textColor: Int = ContextCompat.getColor(context, R.color.colorAccent)
        var textSize: Float = dpToPx(16).toFloat()
        var lineWidth: Float = dpToPx(1).toFloat()
        try {
            val a = context.theme.obtainStyledAttributes(attrs, R.styleable.OrSeparatorView, 0, 0)
            lineColor = a.getColor(
                R.styleable.OrSeparatorView_lineColor,
                ContextCompat.getColor(context, R.color.colorAccent)
            )
            textSize = a.getDimension(R.styleable.OrSeparatorView_oRSize, dpToPx(20).toFloat())
            lineWidth = a.getDimension(R.styleable.OrSeparatorView_lineWidth, dpToPx(1).toFloat())
            a.recycle()
        } catch (e: Exception) {
        }
        val font = ResourcesCompat.getFont(context, R.font.montserrat_bold);
        textPaint.typeface = font
        textPaint.textSize = textSize
        textPaint.color = textColor
        linePaint.color = lineColor
        linePaint.isAntiAlias = true
        textPaint.getTextBounds("Or", 0, "Or".length, textBounds)
        textPaint.isAntiAlias = true
        linePaint.strokeWidth = lineWidth
    }

    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)
        val centerY = height * .5f
        val centerX = width * .5f
        canvas?.drawLine(0f, centerY, width.toFloat() * .50f - textBounds!!.width() - dpToPx(8), centerY, linePaint)
        canvas?.drawText("Or", centerX - (textBounds.width() / 2), height.toFloat() * .75f, textPaint)
        canvas?.drawLine(width * .50f +  textBounds!!.width() + dpToPx(8), centerY, width.toFloat(), centerY, linePaint)
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        val heightMode = MeasureSpec.getMode(heightMeasureSpec)
        val heightSize = when (heightMode) {
            MeasureSpec.EXACTLY -> min(MeasureSpec.getSize(heightMeasureSpec), desireHeight)
            MeasureSpec.AT_MOST -> min(MeasureSpec.getSize(heightMeasureSpec), desireHeight)
            else -> min(MeasureSpec.getSize(heightMeasureSpec), desireHeight)
        }
        setMeasuredDimension(MeasureSpec.getSize(widthMeasureSpec), heightSize)
    }
}