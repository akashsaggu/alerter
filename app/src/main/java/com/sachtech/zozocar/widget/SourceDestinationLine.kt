package com.sachtech.zozocar.widget

import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import android.util.AttributeSet
import android.view.View
import com.sachtech.zozocar.R
import com.sachtech.zozocar.util.extension.dpToPx
import com.sachtech.zozocar.util.extension.getColorCompat

class SourceDestinationLine @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : View(context, attrs, defStyleAttr) {

    private var circlePaint: Paint = Paint()
    private var linePaint: Paint = Paint()


    init {
        circlePaint.isAntiAlias = true
        circlePaint.color = context.getColorCompat(R.color.colorCyan)
        circlePaint.style = Paint.Style.STROKE
        linePaint.strokeCap = Paint.Cap.ROUND
        circlePaint.strokeWidth = dpToPx(3).toFloat()

        linePaint.isAntiAlias = true
        linePaint.color = context.getColorCompat(R.color.colorCyan)
        linePaint.style = Paint.Style.FILL
        linePaint.strokeCap = Paint.Cap.SQUARE
        linePaint.strokeWidth = dpToPx(4).toFloat()
    }

    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)
        canvas?.run {
            drawLine(width / 2f, (width / 2f) * 2, width / 2f, height - (width / 2f) * 2, linePaint)
            drawCircle(width / 2f, width / 2f, (width / 2f) - circlePaint.strokeWidth, circlePaint)
            drawCircle(
                width / 2f,
                height - (width / 2f),
                (width / 2f) - circlePaint.strokeWidth,
                circlePaint
            )
        }
    }
}