package com.sachtech.zozocar.constants

object PrefKey {

    const val FCMTOKEN = "fcmToken"
    const val USER = "user"

}