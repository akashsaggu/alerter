package com.sachtech.zozocar.constants

object FlowType {
    const val Normal = 0
    const val Return = 1
    const val PICKER = 2
}