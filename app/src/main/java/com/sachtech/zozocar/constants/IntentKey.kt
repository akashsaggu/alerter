package com.sachtech.zozocar.constants

//&#x00BB;
object IntentKey {

    val REPORTREASON: String?="report_reason"
    val RIDE="ride"
    const val ADDRESS = "Address"
    const val RIDETYPE = "FlowType"
    const val ACCOUNTOPTION="AccountOption"
    const val FLOWTYPE = "FlowType"
    const val LOCATIONTYPE = "LocationType"
    const val DATE = "Date"
    const val TIME = "Time"
    const val FRAGMENTOTYPE = "FragmentoType"
    const val DETAILTYPE="detailType"
    const val REPORTTYPE="reportType"
    const val RLTYPE = "rltype"
    const val CarSearchType = "carsearchType"
    const val CARMAKER = "carMaker"
}