package com.sachtech.zozocar.constants

object FragmentoType {

    const val DATETIMEPICKER = 0
    const val SEARCHRESULT = 1
    const val RIDEPLAN = 2
    const val SEARCHDETAIL = 3
    const val RideOfferSuccess = 4

}