package com.sachtech.zozocar.constants

object ReportType{
    val INAPPROPRIATE="Unsafe or inappropriate behaviour"
    val WRONG_WITH_RiDE_OR_PROFILE="Something wrong with this ride or profile"
    val SUSPICIOUS_PAYMENTS_OR_PRICE="Suspicious payments or pricing"

}