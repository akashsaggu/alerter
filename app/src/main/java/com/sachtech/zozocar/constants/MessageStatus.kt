package com.sachtech.zozocar.constants

object MessageStatus {
    val SENT="sent"
    val DELIVERED="delivered"
}