package com.sachtech.zozocar.constants

object DetailType{
    val EMAIL: String="email"
    val BIO="bio"
    val PREFERENCES="preferences"
    val VERIFY="verify"
    val MOBILE="mobile"
    val CAR="car"
    val PUBLICPROFILE="publicProfile"
}