package com.sachtech.zozocar.constants

object AccountOption {
    val RATING = "rating"
    val NOTIFICATION = "notification"
    val CHANGEPASSWORD = "changePassword"
    val POSTALADDRESS = "postalAddress"
    val AVAILABLEFUNDS="availableFunds"
    val PAYMENTS="payments"
    val BANKDETAILS="bankDetails"
    val PASTTRANSFERS="pastTransfers"
    val HELP="help"
    val TERMSNCONDITIONS="termsNCondition"
    val PRIVACYPOLICY="privacyPolicy"
    val LICENSES="licenses"
}