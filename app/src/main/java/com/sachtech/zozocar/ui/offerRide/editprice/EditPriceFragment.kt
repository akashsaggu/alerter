package com.sachtech.zozocar.ui.offerRide.editprice

import android.os.Bundle
import android.view.View
import com.sachtech.echelon.util.extension.setSolidRadiusRect
import com.sachtech.zozocar.R
import com.sachtech.zozocar.common.helper.RidePreparer
import com.sachtech.zozocar.ui.offerRide.returnback.ReturnBackFragment
import com.sachtech.zozocar.util.extension.getColorCompat
import gurtek.mrgurtekbase.KotlinBaseFragment
import kotlinx.android.synthetic.main.fragment_editprice.*

//todo minus plus (it would be range of max min he can set or max max he can set)
class EditPriceFragment : KotlinBaseFragment(R.layout.fragment_editprice) {


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        btnConfirm.setSolidRadiusRect(getColorCompat(R.color.colorPrimary), 12)
        setLocations()
        btnConfirm.setOnClickListener {
            RidePreparer.setPerSeatPrice(icoArrowSource.text.toString().substring(1).toFloat())
            baseListener.addFragment(ReturnBackFragment::class, animation = true)
        }
        btnClose.setOnClickListener {
            onBackPressed()
        }
    }


    private fun setLocations() {
        val ride = RidePreparer.getRide()
        txtLocations.text = "${ride.pickup?.locality ?: ride.pickup?.featureName
        ?: ""} \u00BB ${ride.dropOff?.locality ?: ride.dropOff?.featureName ?: ""}"
    }
}