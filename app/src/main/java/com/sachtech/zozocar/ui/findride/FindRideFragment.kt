package com.sachtech.zozocar.ui.findride

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.app.Activity
import android.content.Intent
import android.location.Address
import android.os.Bundle
import android.view.View
import com.sachtech.echelon.util.extension.setSolidRadiusRect
import com.sachtech.zozocar.R
import com.sachtech.zozocar.app.getPref
import com.sachtech.zozocar.common.activty.FindPlaceActivity
import com.sachtech.zozocar.common.activty.FragmentoActivity
import com.sachtech.zozocar.constants.FragmentoType
import com.sachtech.zozocar.constants.IntentKey
import com.sachtech.zozocar.constants.IntentRequest
import com.sachtech.zozocar.constants.PrefKey
import com.sachtech.zozocar.networking.model.UserModel
import com.sachtech.zozocar.ui.registerLogin.RegisterLoginFragment
import com.sachtech.zozocar.ui.registerLogin.interactors.LoginStatusInteractor
import com.sachtech.zozocar.util.extension.getColorCompat
import com.sachtech.zozocar.util.extension.isNotNull
import com.sachtech.zozocar.util.extension.openActivity
import com.sachtech.zozocar.util.extension.openActivityForResult
import gurtek.mrgurtekbase.KotlinBaseFragment
import gurtek.mrgurtekbase.visible
import kotlinx.android.synthetic.main.fragment_findride.*
import java.text.SimpleDateFormat
import java.util.*

class FindRideFragment : KotlinBaseFragment(R.layout.fragment_findride), LoginStatusInteractor {
    private var user = getPref().getprefObject<UserModel.Data>(PrefKey.USER)

    companion object LocationType {
        const val LEAVINGFROM = 0
        const val GOINGTO = 1
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if (user.isNotNull()) {
            setViews()
        } else {
            baseListener.addChildFragment(
                childFragmentManager,
                R.id.findRideContainer,
                animation = false,
                kClass = RegisterLoginFragment::class,
                bundle = Bundle().apply {
                    putInt(IntentKey.RLTYPE, arguments!!.getInt(IntentKey.RLTYPE))
                }
            )
        }
    }

    private fun setViews() {
        btnSearch.setSolidRadiusRect(getColorCompat(R.color.colorPrimary), 14)
        setClickListeners()
    }

    private fun setClickListeners() {
        edtSource.setOnClickListener {
            val intent = Intent(activity!!, FindPlaceActivity::class.java)
            intent.putExtra(IntentKey.LOCATIONTYPE, LEAVINGFROM)
            startActivityForResult(intent, FindPlaceActivity.FINDRIDEREQUEST)
        }
        edtDestination.setOnClickListener {
            val intent = Intent(activity!!, FindPlaceActivity::class.java)
            intent.putExtra(IntentKey.LOCATIONTYPE, GOINGTO)
            startActivityForResult(intent, FindPlaceActivity.FINDRIDEREQUEST)
        }

        txtDate.setOnClickListener {
            openActivityForResult<FragmentoActivity>(IntentRequest.DATETIMEPICKERREQUEST) {
                putExtra(IntentKey.FRAGMENTOTYPE, FragmentoType.DATETIMEPICKER)
            }
        }
        iconSwitch.setOnClickListener {
            it.animate().rotation(it.rotation + 180)
                .setListener(object : AnimatorListenerAdapter() {
                    override fun onAnimationEnd(animation: Animator?) {
                        super.onAnimationEnd(animation)
                        val sourceText = edtSource.text
                        val sourceTag = edtSource.tag
                        val destinationText = edtDestination.text
                        val destinationTag = edtDestination.tag
                        edtSource.text = destinationText
                        edtSource.tag = destinationTag
                        edtDestination.text = sourceText
                        edtDestination.tag = sourceTag
                    }
                }).start()
        }
        btnSearch.setOnClickListener {
            openActivity<FragmentoActivity> {
                putExtra(IntentKey.FRAGMENTOTYPE, FragmentoType.SEARCHRESULT)
            }
        }
    }

    override fun onResume() {
        super.onResume()
        if (edtSource.text.isNotBlank() && edtDestination.text.isNotBlank()) {
            iconSwitch.visible()
        }
        user = getPref().getprefObject<UserModel.Data>(PrefKey.USER)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK && data != null && data.extras != null) {
            when (requestCode) {
                FindPlaceActivity.FINDRIDEREQUEST -> {
                    with(data.extras!!) {
                        val address = getParcelable<Address>(IntentKey.ADDRESS)
                        when (getInt(IntentKey.LOCATIONTYPE, LEAVINGFROM)) {
                            LEAVINGFROM -> {
                                edtSource.tag = address
                                edtSource.text = address?.locality ?: address?.featureName
                                        ?: address?.getAddressLine(0) ?: ""
                            }
                            GOINGTO -> {
                                edtDestination.tag = address
                                edtDestination.text = address?.locality ?: address?.featureName
                                        ?: address?.getAddressLine(0) ?: ""
                            }
                        }
                    }
                }
                IntentRequest.DATETIMEPICKERREQUEST -> {
                    with(data.extras!!) {
                        val timeStamp = getLong(IntentKey.TIME)
                        txtDate.tag = getFormatedString(timeStamp)
                        txtDate.text = txtDate.tag as? String ?: ""
                    }
                }
            }
        }
    }


    // Tue. 21 Jan, 08:00
    fun getFormatedString(timeStamp: Long): String {
        val sdf = SimpleDateFormat("EEE. d MMM, HH:mm", Locale.getDefault())
        return sdf.format(Date(timeStamp))
    }

    override fun onLoginSuccess(dismissRegister: Boolean) {
        if (dismissRegister)
        childFragmentManager.popBackStackImmediate()
        setViews()
    }

}
