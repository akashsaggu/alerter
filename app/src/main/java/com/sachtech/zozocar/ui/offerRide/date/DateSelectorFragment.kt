package com.sachtech.zozocar.ui.offerRide.date

import android.os.Bundle
import android.view.View
import com.sachtech.zozocar.R
import com.sachtech.zozocar.common.helper.RidePreparer
import com.sachtech.zozocar.constants.FlowType
import com.sachtech.zozocar.constants.IntentKey
import com.sachtech.zozocar.ui.offerRide.time.TimeSelectorFragment
import com.sachtech.zozocar.util.getDateString
import gurtek.mrgurtekbase.KotlinBaseFragment
import kotlinx.android.synthetic.main.fragment_date_selector.*
import pl.rafman.scrollcalendar.contract.MonthScrollListener
import pl.rafman.scrollcalendar.contract.State
import pl.rafman.scrollcalendar.data.CalendarDay
import java.util.*


class DateSelectorFragment : KotlinBaseFragment(R.layout.fragment_date_selector) {


    private var flowType: Int = FlowType.Normal
    private var selected: Calendar? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        arguments?.let {
            flowType = it.getInt(IntentKey.FLOWTYPE, FlowType.Normal)
        }
        calendar.setMonthScrollListener(object : MonthScrollListener {
            override fun shouldAddNextMonth(
                lastDisplayedYear: Int,
                lastDisplayedMonth: Int
            ): Boolean {
                return doShouldAddNextMonth(lastDisplayedYear, lastDisplayedMonth)
            }

            override fun shouldAddPreviousMonth(
                firstDisplayedYear: Int,
                firstDisplayedMonth: Int
            ): Boolean {
                return false
                //return doShouldAddPreviousMonth(firstDisplayedYear, firstDisplayedMonth)
            }
        })

        calendar.setDateWatcher { year, month, day ->
            doGetStateForDate(
                year,
                month,
                day
            )
        }


        calendar.setOnDateClickListener { year, month, day ->
            doOnCalendarDayClicked(
                year,
                month,
                day
            )
        }

        btnClose.setOnClickListener {
            onBackPressed()
        }

    }

    private fun doOnCalendarDayClicked(year: Int, month: Int, day: Int) {
        val calendar = Calendar.getInstance()
        calendar.set(Calendar.YEAR, year)
        calendar.set(Calendar.MONTH, month)
        calendar.set(Calendar.DAY_OF_MONTH, day)
        calendar.set(Calendar.HOUR_OF_DAY, 0)
        calendar.set(Calendar.MINUTE, 0)
        calendar.set(Calendar.SECOND, 0)
        calendar.set(Calendar.MILLISECOND, 0)

        if (isInThePast(year, month, day)) {
            return
        }
        if (isWeekend(year, month, day)) {
            return
        }

        selected = if (selected != null && selected!!.equals(calendar)) {
            null
        } else {
            calendar
        }
        when (flowType) {
            FlowType.Normal -> {
                RidePreparer.setRideDate(
                    calendar.getDateString()
                )
            }
            FlowType.Return -> {
                RidePreparer.setReturnDate(
                    calendar.getDateString()
                )
            }
        }
        baseListener.addFragment(
            TimeSelectorFragment::class,
            animation = true,
            tag = System.currentTimeMillis().toString(),
            extras = Bundle().apply {
                putInt(IntentKey.FLOWTYPE, flowType)
                if (flowType == FlowType.PICKER)
                    putString(IntentKey.DATE, calendar.getDateString())
            })
    }

    @State
    private fun doGetStateForDate(year: Int, month: Int, day: Int): Int {
        if (isSelected(selected, year, month, day)) {
            return CalendarDay.SELECTED
        }
        if (isToday(year, month, day)) {
            return CalendarDay.TODAY
        }
        /*if (isUnavailable(year, month, day)) {
            return CalendarDay.UNAVAILABLE
        }*/
        return if (isInThePast(year, month, day) /*|| isWeekend(year, month, day)*/) {
            CalendarDay.DISABLED
        } else CalendarDay.DEFAULT
    }


    private fun isInThePast(year: Int, month: Int, day: Int): Boolean {
        val calendar = Calendar.getInstance()
        calendar.set(Calendar.HOUR_OF_DAY, 1)
        calendar.set(Calendar.MINUTE, 0)
        calendar.set(Calendar.SECOND, 0)
        calendar.set(Calendar.MILLISECOND, 0)

        val now = calendar.timeInMillis

        calendar.set(Calendar.YEAR, year)
        calendar.set(Calendar.MONTH, month)
        calendar.set(Calendar.DAY_OF_MONTH, day)

        val then = calendar.timeInMillis
        return now > then
    }

    private fun doShouldAddPreviousMonth(year: Int, month: Int): Boolean {
        val calendar = Calendar.getInstance()
        calendar.add(Calendar.MONTH, -10)
        calendar.set(Calendar.HOUR_OF_DAY, 0)
        calendar.set(Calendar.MINUTE, 0)
        calendar.set(Calendar.SECOND, 0)
        calendar.set(Calendar.MILLISECOND, 0)

        val target = calendar.timeInMillis

        calendar.set(Calendar.YEAR, year)
        calendar.set(Calendar.MONTH, month)

        return calendar.timeInMillis > target
    }

    private fun doShouldAddNextMonth(year: Int, month: Int): Boolean {
        val calendar = Calendar.getInstance()
        calendar.add(Calendar.MONTH, 3)
        calendar.set(Calendar.HOUR_OF_DAY, 0)
        calendar.set(Calendar.MINUTE, 0)
        calendar.set(Calendar.SECOND, 0)
        calendar.set(Calendar.MILLISECOND, 0)

        val target = calendar.timeInMillis

        calendar.set(Calendar.YEAR, year)
        calendar.set(Calendar.MONTH, month)

        return calendar.timeInMillis < target
    }

    private fun isSelected(selected: Calendar?, year: Int, month: Int, day: Int): Boolean {
        if (selected == null) {
            return false
        }

        val calendar = Calendar.getInstance()
        calendar.set(Calendar.YEAR, selected.get(Calendar.YEAR))
        calendar.set(Calendar.MONTH, selected.get(Calendar.MONTH))
        calendar.set(Calendar.DAY_OF_MONTH, selected.get(Calendar.DAY_OF_MONTH))
        calendar.set(Calendar.HOUR_OF_DAY, 0)
        calendar.set(Calendar.MINUTE, 0)
        calendar.set(Calendar.SECOND, 0)
        calendar.set(Calendar.MILLISECOND, 0)
        val millis = calendar.timeInMillis

        calendar.set(Calendar.YEAR, year)
        calendar.set(Calendar.MONTH, month)
        calendar.set(Calendar.DAY_OF_MONTH, day)
        val millis2 = calendar.timeInMillis

        return millis == millis2
    }

    private fun isUnavailable(year: Int, month: Int, day: Int): Boolean {
        val now = Calendar.getInstance()
        now.set(Calendar.YEAR, year)
        now.set(Calendar.MONTH, month)
        now.set(Calendar.DAY_OF_MONTH, day)
        now.set(Calendar.HOUR_OF_DAY, 0)
        now.set(Calendar.MINUTE, 0)
        now.set(Calendar.SECOND, 0)
        now.set(Calendar.MILLISECOND, 0)
        val millis = now.timeInMillis


        val calendar = Calendar.getInstance()
        calendar.set(Calendar.HOUR_OF_DAY, 0)
        calendar.set(Calendar.MINUTE, 0)
        calendar.set(Calendar.SECOND, 0)
        calendar.set(Calendar.MILLISECOND, 0)
        calendar.add(Calendar.DAY_OF_MONTH, 5)
        val millis2 = calendar.timeInMillis

        calendar.add(Calendar.DAY_OF_MONTH, 1)
        val millis3 = calendar.timeInMillis

        calendar.add(Calendar.DAY_OF_MONTH, 1)
        val millis4 = calendar.timeInMillis

        return millis == millis2 || millis == millis3 || millis == millis4
    }

    private fun isWeekend(year: Int, month: Int, day: Int): Boolean {

        val calendar = Calendar.getInstance()
        calendar.set(Calendar.YEAR, year)
        calendar.set(Calendar.MONTH, month)
        calendar.set(Calendar.DAY_OF_MONTH, day)
        calendar.set(Calendar.HOUR_OF_DAY, 0)
        calendar.set(Calendar.MINUTE, 0)
        calendar.set(Calendar.SECOND, 0)
        calendar.set(Calendar.MILLISECOND, 0)
        val dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK)
        return dayOfWeek == Calendar.SATURDAY || dayOfWeek == Calendar.SUNDAY
    }

    private fun isToday(year: Int, month: Int, day: Int): Boolean {

        val calendar = Calendar.getInstance()
        calendar.set(Calendar.HOUR_OF_DAY, 0)
        calendar.set(Calendar.MINUTE, 0)
        calendar.set(Calendar.SECOND, 0)
        calendar.set(Calendar.MILLISECOND, 0)

        // Today in milliseconds
        val today = calendar.time.time

        // Given day in milliseconds
        calendar.set(Calendar.YEAR, year)
        calendar.set(Calendar.MONTH, month)
        calendar.set(Calendar.DAY_OF_MONTH, day)

        val calendarMillis = calendar.time.time

        return today == calendarMillis
    }


}