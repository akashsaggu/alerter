package com.sachtech.zozocar.ui.profile.details.car.vm

import androidx.lifecycle.MutableLiveData
import com.sachtech.zozocar.base.BaseViewModel
import com.sachtech.zozocar.networking.model.CarBodyType
import com.sachtech.zozocar.networking.model.CarBrandModels
import com.sachtech.zozocar.networking.model.CarColors
import com.sachtech.zozocar.networking.model.CarMakerBrand

class AddCarVm : BaseViewModel() {
    private val carBodyTypes by lazy { MutableLiveData<List<CarBodyType.Data>>() }
    val bodyTypeLd: MutableLiveData<List<CarBodyType.Data>>
        get() {
            return carBodyTypes
        }

    private val carColors by lazy { MutableLiveData<List<CarColors.Data>>() }
    val colorsLd: MutableLiveData<List<CarColors.Data>>
        get() {
            return carColors
        }

    private val carMakerBrand by lazy { MutableLiveData<List<CarMakerBrand.Data>>() }
    val carBrandsLd: MutableLiveData<List<CarMakerBrand.Data>>
        get() {
            return carMakerBrand
        }

    private val carBrandModel by lazy { MutableLiveData<List<CarBrandModels.Data>>() }
    val carModelsLd: MutableLiveData<List<CarBrandModels.Data>>
        get() {
            return carBrandModel
        }


    fun getCarBodyType() {
        getRemoteRepo().getCarBodyTypes().subscribeWithProgressAndDisposable {
            carBodyTypes.value = it.data
        }
    }

    fun getCarColors() {
        getRemoteRepo().getCarColors().subscribeWithProgressAndDisposable {
            carColors.value = it.data
        }
    }

    fun getCarMakerBrands() {
        getRemoteRepo().getCarBrands().subscribeWithProgressAndDisposable {
            carMakerBrand.value = it.data
        }
    }


    fun getCarMakerBrandModels(brand: String) {
        getRemoteRepo().getCarBrandModel(brand).subscribeWithProgressAndDisposable {
            carBrandModel.value = it.data
        }
    }


}