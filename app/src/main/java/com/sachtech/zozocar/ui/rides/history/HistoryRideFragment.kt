package com.sachtech.zozocar.ui.rides.history

import android.os.Bundle
import android.view.View
import com.sachtech.echelon.util.extension.setSolidRadiusRect
import com.sachtech.zozocar.R
import com.sachtech.zozocar.ui.rides.history.adapter.HistoryAdapter
import com.sachtech.zozocar.util.extension.getColorCompat
import com.sachtech.zozocar.util.extension.gone
import com.sachtech.zozocar.util.extension.visible
import gurtek.mrgurtekbase.KotlinBaseFragment
import kotlinx.android.synthetic.main.fragment_history.*


class HistoryRideFragment : KotlinBaseFragment(R.layout.fragment_history) {

    private val archiveAdapter by lazy { HistoryAdapter() }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        btnArchiveRides.setSolidRadiusRect(getColorCompat(R.color.colorPrimary), 14)
        archieveRides.adapter = archiveAdapter
        btnArchiveRides.setOnClickListener {
            archieveRides.visible()
            btnArchiveRides.gone()
        }
    }

}