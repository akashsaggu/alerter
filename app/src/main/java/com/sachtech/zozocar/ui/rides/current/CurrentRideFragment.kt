package com.sachtech.zozocar.ui.rides.current

import android.os.Bundle
import android.view.View
import com.sachtech.echelon.util.extension.setSolidRadiusRect
import com.sachtech.zozocar.R
import com.sachtech.zozocar.ui.findride.FindRideFragment
import com.sachtech.zozocar.ui.offerRide.OfferRideActivity
import com.sachtech.zozocar.util.extension.getColorCompat
import com.sachtech.zozocar.util.extension.openActivity
import gurtek.mrgurtekbase.KotlinBaseFragment
import kotlinx.android.synthetic.main.include_emptycurrentride.*

class CurrentRideFragment : KotlinBaseFragment(R.layout.fragment_current) {


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        btnOfferRide.setSolidRadiusRect(getColorCompat(R.color.colorPrimary), 16)
        btnFindRide.setSolidRadiusRect(getColorCompat(R.color.colorPrimary), 16)
        btnOfferRide.setOnClickListener {
            openActivity<OfferRideActivity>()
        }
        btnFindRide.setOnClickListener {
            baseListener.addFragment(
                FindRideFragment::class,
                animation = false
            )
        }
    }

}