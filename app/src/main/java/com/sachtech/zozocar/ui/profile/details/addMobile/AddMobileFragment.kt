package com.sachtech.zozocar.ui.profile.details.addMobile

import android.os.Bundle
import android.view.View
import androidx.core.content.res.ResourcesCompat
import androidx.lifecycle.Observer
import com.google.android.material.appbar.AppBarLayout
import com.sachtech.zozocar.R
import com.sachtech.zozocar.app.getPref
import com.sachtech.zozocar.constants.PrefKey
import com.sachtech.zozocar.ui.profile.details.addMobile.vm.AddMobileVm
import com.wajahatkarim3.easyvalidation.core.view_ktx.validator
import gurtek.mrgurtekbase.KotlinBaseFragment
import gurtek.mrgurtekbase.gone
import kotlinx.android.synthetic.main.fragment_add_mobile.*
import org.koin.androidx.viewmodel.ext.android.viewModel


class AddMobileFragment : KotlinBaseFragment(R.layout.fragment_add_mobile) {

    private val viewModel: AddMobileVm by viewModel()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setViewModel(viewModel)
        setViews()
        setEventObservers()
    }

    private fun setViews() {

        fun setClickListeners() {
            addMobileToolbar.setNavigationOnClickListener {
                onBackPressed()
            }
            btnConfirm.setOnClickListener {
                if (validateMobile()) {
                    viewModel.addMobileNumber(edtPhone.text.toString())
                }
            }
        }


        setClickListeners()
        setAppBarOffset()
    }

    private fun setEventObservers() {
        viewModel.mobileLiveData.observe(viewLifecycleOwner, Observer {
            getPref().setprefObject(PrefKey.USER, it)
            parentFragment?.parentFragment?.childFragmentManager?.popBackStackImmediate()
        })
    }


    private fun setAppBarOffset() {
        val sp = ResourcesCompat.getFont(activity!!, R.font.montserrat_regular)
        collapseToolbartLayout.setCollapsedTitleTypeface(sp)
        collapseToolbartLayout.setExpandedTitleTypeface(sp)
        profile_appbar.addOnOffsetChangedListener(object : AppBarLayout.OnOffsetChangedListener {
            var isShow = false
            var scrollRange = -1
            override fun onOffsetChanged(p0: AppBarLayout?, p1: Int) {
                if (scrollRange == -1) {
                    scrollRange = p0?.totalScrollRange!!
                }
                if (scrollRange + p1 == 0) {
                    isShow = true
                    layoutTitle.transitionAlpha = 0.0f
                    collapseToolbartLayout.isTitleEnabled = true

                } else if (isShow) {
                    isShow = false
                    addMobileToolbar.title = "Add Mobile"
                    title_add_mobile.gone()
                    layoutTitle.transitionAlpha = 9.0f
                    collapseToolbartLayout.isTitleEnabled = false
                }
            }
        })
    }

    private fun validateMobile(): Boolean {
        return if (edtPhone.validator().maxLength(10).check()) {
            true
        } else {
            edtPhone.error = "Please enter valid mobile number."
            false
        }
    }

}