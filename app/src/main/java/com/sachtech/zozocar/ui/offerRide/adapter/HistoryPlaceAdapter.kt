package com.sachtech.zozocar.ui.offerRide.adapter

import com.google.android.libraries.places.api.model.AutocompletePrediction
import com.google.android.libraries.places.api.net.PlacesClient
import com.sachtech.zozocar.R
import com.sachtech.zozocar.common.adapter.PlacesAdapter
import com.sachtech.zozocar.util.recyclerview.ItemClickListener
import kotlinx.android.synthetic.main.item_place.view.*

class HistoryPlaceAdapter(
    placesClient: PlacesClient,
    private val itemClickListener: ItemClickListener<AutocompletePrediction>
) : PlacesAdapter(placesClient, R.layout.item_place) {

    override fun onBindViewHolder(holder: IViewHolder, position: Int) {
        holder.itemView.addressMain.text = list[position].getPrimaryText(null)
        holder.itemView.addressSub.text = list[position].getSecondaryText(null)
        holder.itemView.setOnClickListener {
            itemClickListener.onItemClick(item = list[position])
        }
    }
}