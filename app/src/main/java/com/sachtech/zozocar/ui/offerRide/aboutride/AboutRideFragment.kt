package com.sachtech.zozocar.ui.offerRide.aboutride

import android.os.Bundle
import android.transition.Fade
import android.view.View
import com.sachtech.echelon.util.extension.setSolidRadiusRect
import com.sachtech.zozocar.R
import com.sachtech.zozocar.common.activty.FragmentoActivity
import com.sachtech.zozocar.common.helper.RidePreparer
import com.sachtech.zozocar.common.helper.Rides
import com.sachtech.zozocar.constants.FragmentoType
import com.sachtech.zozocar.constants.IntentKey
import com.sachtech.zozocar.util.extension.delay
import com.sachtech.zozocar.util.extension.getColorCompat
import com.sachtech.zozocar.util.extension.openActivity
import com.sachtech.zozocar.util.extension.toast
import gurtek.mrgurtekbase.KotlinBaseFragment
import kotlinx.android.synthetic.main.fragment_aboutride.*

class AboutRideFragment : KotlinBaseFragment(R.layout.fragment_aboutride) {


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        btnPublish.setSolidRadiusRect(getColorCompat(R.color.colorPrimary), 12)
        btnPublish.setOnClickListener {
            val rides: Rides = RidePreparer.complete()
            if (rides.success) {
                openActivity<FragmentoActivity> {
                    putExtra(IntentKey.FRAGMENTOTYPE, FragmentoType.RideOfferSuccess)
                }
                with(activity!!) {
                    delay(1200) {
                        if (!this@AboutRideFragment.isDetached) {
                            activity?.window?.returnTransition = Fade()
                            activity?.window?.exitTransition = Fade()
                            finish()
                            overridePendingTransition(0, 0)
                        }
                    }
                }
            } else {
                toast(rides.message)
            }
        }
        btnClose.setOnClickListener {
            onBackPressed()
        }
    }
}