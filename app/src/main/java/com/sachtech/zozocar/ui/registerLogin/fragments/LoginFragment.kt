package com.sachtech.zozocar.ui.registerLogin.fragments

import android.os.Bundle
import android.view.View
import com.sachtech.echelon.util.extension.setSolidRadiusRect
import com.sachtech.zozocar.R
import com.sachtech.zozocar.ui.registerLogin.RegisterLoginFragment
import com.sachtech.zozocar.ui.registerLogin.interactors.RegisterLoginInteractor
import com.sachtech.zozocar.util.extension.getColorCompat
import com.wajahatkarim3.easyvalidation.core.view_ktx.validEmail
import com.wajahatkarim3.easyvalidation.core.view_ktx.validator
import gurtek.mrgurtekbase.KotlinBaseFragment
import kotlinx.android.synthetic.main.fragment_login.*

/**
 * Created by Sachtech Solutions.
 */
class LoginFragment : KotlinBaseFragment(R.layout.fragment_login) {


    private var registerLoginInteractor: RegisterLoginInteractor? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        registerLoginInteractor = parentFragment as? RegisterLoginFragment
        setViews()
    }

    private fun setViews() {
        btnLogin.setSolidRadiusRect(getColorCompat(R.color.colorPrimary), radius = 12)
        btnLogin.setOnClickListener {
            if (validateFields()) {
                registerLoginInteractor?.doEmailLogin(
                    edtEmail.text.toString(),
                    edtPass.text.toString()
                )
            }
        }

        facebookButton.setOnClickListener {
            registerLoginInteractor?.doFacebookLogin()
        }

        textNotAMemeber.setOnClickListener {
            registerLoginInteractor?.goToRegister()
        }
    }

    private fun validateFields(): Boolean {
        var isValid = true
        if (!edtEmail.validEmail()) {
            edtEmail.error = getString(R.string.validEmail)
            isValid = false
        }
        if (!edtPass.validator().nonEmpty().check()) {
            edtPass.error = getString(R.string.fieldReq)
            isValid = false
        }
        return isValid
    }
}