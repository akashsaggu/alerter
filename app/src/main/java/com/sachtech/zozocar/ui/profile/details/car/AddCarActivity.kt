package com.sachtech.zozocar.ui.profile.details.car

import android.os.Bundle
import com.sachtech.zozocar.common.helper.CarPreparer
import com.sachtech.zozocar.ui.profile.details.car.features.AddCarNumberPlateFragment
import com.sachtech.zozocar.ui.profile.details.car.vm.AddCarVm
import gurtek.mrgurtekbase.KotlinBaseActivity
import org.koin.androidx.viewmodel.ext.android.viewModel

class AddCarActivity : KotlinBaseActivity() {

    private val viewModel: AddCarVm by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setViewModel(viewModel)
        CarPreparer.clear()
        navigateToFragment<AddCarNumberPlateFragment>()
    }

}