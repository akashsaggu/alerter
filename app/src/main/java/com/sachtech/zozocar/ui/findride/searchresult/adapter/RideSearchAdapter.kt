package com.sachtech.zozocar.ui.findride.searchresult.adapter

import com.sachtech.zozocar.R
import com.sachtech.zozocar.common.activty.FragmentoActivity
import com.sachtech.zozocar.constants.FragmentoType
import com.sachtech.zozocar.constants.IntentKey
import com.sachtech.zozocar.util.extension.getActivity
import com.sachtech.zozocar.util.extension.openActivity
import gurtek.mrgurtekbase.adapter.ItemsBaseAdapter

class RideSearchAdapter : ItemsBaseAdapter<String>(R.layout.item_searchride) {
    override fun onBindViewHolder(holder: IViewHolder, position: Int) {
        holder.itemView.setOnClickListener {
            holder.getActivity<FragmentoActivity>()?.openActivity<FragmentoActivity> {
                putExtra(IntentKey.FRAGMENTOTYPE, FragmentoType.SEARCHDETAIL)
            }
        }
    }

    override fun getItemCount(): Int {
        return 2
    }
}