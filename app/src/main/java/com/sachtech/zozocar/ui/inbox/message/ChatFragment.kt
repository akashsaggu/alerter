package com.sachtech.zozocar.ui.inbox.message

import android.os.Bundle
import android.view.View
import com.google.gson.Gson
import com.sachtech.zozocar.R
import com.sachtech.zozocar.common.helper.Message
import com.sachtech.zozocar.common.helper.RideBooking
import com.sachtech.zozocar.constants.IntentKey
import com.sachtech.zozocar.util.extension.getDateInformat
import gurtek.mrgurtekbase.KotlinBaseFragment
import kotlinx.android.synthetic.main.fragment_chat.*
import java.util.*
import kotlin.collections.ArrayList

class ChatFragment : KotlinBaseFragment(R.layout.fragment_chat) {

    lateinit var chatAdapter: ChatAdapter
    lateinit var arrayList: ArrayList<Message>


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val ride = arguments?.getString(IntentKey.RIDE)
        val rideDetail = Gson().fromJson(ride, RideBooking::class.java)
        if(rideDetail!=null) {
            chatToolbar.title = rideDetail.rider
            chatRideSource.setText(rideDetail.pickOff)
            chatRideDestination.setText(rideDetail.dropOff)
        }
        arrayList = ArrayList()
        chatToolbar.setNavigationOnClickListener { onBackPressed() }
        arrayList.add(Message("Hello",false,Date().time.getDateInformat(),rideDetail.rider))
        chatAdapter = ChatAdapter(context!!, arrayList, baseListener)
        chatRv.adapter = chatAdapter

        info.setOnClickListener { showDialog(MessagingGuidelineDialog::class) }
        chatMessageSend.setOnClickListener {
            val messageString = chatMessage.text.toString()
            val time = Date().time.getDateInformat()
            if(messageString.isNotEmpty()) {
                val message = Message(messageString, true, time)
                arrayList.add(message)
                chatAdapter.notifyDataSetChanged()
                chatMessage.setText("")
            }
        }
    }
}