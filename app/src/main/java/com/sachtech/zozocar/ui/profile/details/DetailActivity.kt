package com.sachtech.zozocar.ui.profile.details

import android.os.Bundle
import com.sachtech.zozocar.R
import com.sachtech.zozocar.constants.DetailType
import com.sachtech.zozocar.constants.IntentKey
import com.sachtech.zozocar.ui.profile.details.addMobile.AddMobileFragment
import com.sachtech.zozocar.ui.profile.details.car.features.AddCarNumberPlateFragment
import gurtek.mrgurtekbase.KotlinBaseActivity

class DetailActivity:KotlinBaseActivity(R.id.detailContainer) {
    var detailType:String?=null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)
        detailType=intent?.getStringExtra(IntentKey.DETAILTYPE)

         if(detailType==DetailType.PREFERENCES)
            navigateToFragment<PreferencesFragment>()
        else if(detailType==DetailType.VERIFY)
            navigateToFragment<VerifyNameFragment>()
        else if(detailType==DetailType.MOBILE)
            navigateToFragment<AddMobileFragment>()
        else if(detailType==DetailType.CAR)
            navigateToFragment<AddCarNumberPlateFragment>()
        else if(detailType==DetailType.PUBLICPROFILE)
            navigateToFragment<PublicProfileFragment>()
    }
}