package com.sachtech.zozocar.ui.offerRide.pickup

import android.app.Activity
import android.content.Intent
import android.location.Address
import android.os.Bundle
import android.view.View
import androidx.core.content.ContextCompat
import com.google.android.libraries.places.api.Places
import com.google.android.libraries.places.api.model.AutocompletePrediction
import com.google.android.libraries.places.api.model.Place
import com.sachtech.zozocar.R
import com.sachtech.zozocar.app.App
import com.sachtech.zozocar.common.activty.ExactPlaceActivity
import com.sachtech.zozocar.common.helper.RidePreparer
import com.sachtech.zozocar.constants.IntentKey
import com.sachtech.zozocar.ui.offerRide.adapter.HistoryPlaceAdapter
import com.sachtech.zozocar.ui.offerRide.dropoff.DropOffFragment
import com.sachtech.zozocar.util.extension.*
import com.sachtech.zozocar.util.recyclerview.ItemClickListener
import com.sachtech.zozocar.widget.ClearEditText.OnTextClearedListener
import gurtek.mrgurtekbase.KotlinBaseFragment
import gurtek.mrgurtekbase.gone
import kotlinx.android.synthetic.main.fragment_pickup.*

class PickUpFragment : KotlinBaseFragment(R.layout.fragment_pickup),
    ItemClickListener<AutocompletePrediction> {


    override fun onItemClick(item: AutocompletePrediction) {
        if (item.placeTypes[0] == Place.Type.LOCALITY) {
            startActivityForResult(
                Intent(activity!!, ExactPlaceActivity::class.java),
                ExactPlaceActivity.EXACTADDRESSREQUEST
            )
            activity?.overridePendingTransition(0, 0)
        } else {
            val location = item.getAddress()
            RidePreparer.addPickUp(
                location
            )
            forceHideKeyboard()
            baseListener.addFragment(DropOffFragment::class, animation = true)
        }
    }


    private val placeAdapter by lazy {
        if (!Places.isInitialized()) {
            Places.initialize(App.getApp(), "AIzaSyDLDmvcZpQKHnbkEg76bk9jMd4QB_kZw4c")
        }
        HistoryPlaceAdapter(Places.createClient(activity!!), this)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setUpViews()
    }

    private fun setUpViews() {
        fun setUpAnimation() {
            edtPickUp.setOnFocusChangeListener { v, hasFocus ->
                if (hasFocus) {
                    btnCancel.gone()
                    txtPickup.gone()
                    edtPickUp.setmLeftIconDrawable(
                        ContextCompat.getDrawable(
                            context!!,
                            R.drawable.ic_left_arrow
                        )
                    )
                    activity?.delay(500) {
                        edtPickUp.requestFocus()
                    }
                }
            }
        }
        setUpAnimation()
        btnCancel.setOnClickListener {
            activity?.finish()
        }

        edtPickUp.setOnTextClearedListener(object : OnTextClearedListener {
            override fun onTextCleared() {
                toast("cleared")
            }

            override fun onLeftIconClick() {
                hideKeyboard()
                btnCancel.visible()
                txtPickup.visible()
                edtPickUp.setmLeftIconDrawable(
                    ContextCompat.getDrawable(
                        context!!,
                        R.drawable.ic_search_small
                    )
                )
            }

        })
        placesRecycler.adapter = placeAdapter
        edtPickUp.observeTextChange {
            placeAdapter.filter.filter(it)
        }

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK && requestCode == ExactPlaceActivity.EXACTADDRESSREQUEST) {
            data?.let {
                val location: Address = it.extras?.getParcelable(IntentKey.ADDRESS)!!
                RidePreparer.addPickUp(location)
                baseListener.hideProgress()
                forceHideKeyboard()
                hideKeyboard()
                edtPickUp.clearFocus()
                baseListener.addFragment(DropOffFragment::class, animation = true)
            }
        }
    }


}