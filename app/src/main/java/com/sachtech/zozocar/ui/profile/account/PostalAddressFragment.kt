package com.sachtech.zozocar.ui.profile.account

import android.os.Bundle
import android.view.View
import com.sachtech.zozocar.R
import gurtek.mrgurtekbase.KotlinBaseFragment
import kotlinx.android.synthetic.main.fragment_postal_address.*

class PostalAddressFragment : KotlinBaseFragment(R.layout.fragment_postal_address) {


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        postalAddressToolbar.setNavigationOnClickListener { onBackPressed() }
    }
}
