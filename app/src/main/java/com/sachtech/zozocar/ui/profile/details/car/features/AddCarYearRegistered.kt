package com.sachtech.zozocar.ui.profile.details.car.features

import android.content.Intent
import android.os.Bundle
import android.view.View
import com.sachtech.zozocar.R
import com.sachtech.zozocar.ui.home.HomeActivity
import gurtek.mrgurtekbase.KotlinBaseFragment
import kotlinx.android.synthetic.main.fragment_car_year_registered.*

class AddCarYearRegistered :KotlinBaseFragment(R.layout.fragment_car_year_registered) {


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        carToolbar.setNavigationOnClickListener { onBackPressed() }
        carYearSubmit.setOnClickListener { val intent=Intent(activity!!,HomeActivity::class.java)
        intent.flags=Intent.FLAG_ACTIVITY_CLEAR_TASK
        intent.flags=Intent.FLAG_ACTIVITY_CLEAR_TOP
        context?.startActivity(intent)}
    }
}