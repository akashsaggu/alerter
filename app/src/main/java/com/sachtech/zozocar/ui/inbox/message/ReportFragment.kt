package com.sachtech.zozocar.ui.inbox.message

import android.os.Bundle
import android.view.View
import com.sachtech.zozocar.R
import com.sachtech.zozocar.constants.IntentKey
import com.sachtech.zozocar.constants.ReportType
import gurtek.mrgurtekbase.KotlinBaseFragment
import kotlinx.android.synthetic.main.fragment_report.*

class ReportFragment :KotlinBaseFragment(R.layout.fragment_report) {


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        reportToolbar.setNavigationOnClickListener { onBackPressed() }
        reportUnsafe.setOnClickListener { navigateToSelectedReportTypeFragment(ReportType.INAPPROPRIATE) }
        reportWrongRideOrProfile.setOnClickListener { navigateToSelectedReportTypeFragment(ReportType.WRONG_WITH_RiDE_OR_PROFILE) }
        reportSuspiciousPayments.setOnClickListener { navigateToSelectedReportTypeFragment(ReportType.SUSPICIOUS_PAYMENTS_OR_PRICE) }
    }

    private fun navigateToSelectedReportTypeFragment(type: String) {
        baseListener.navigateToFragment(ReportTypeFragment::class,Bundle().apply {
            putString(IntentKey.REPORTTYPE,type)
        })
    }
}