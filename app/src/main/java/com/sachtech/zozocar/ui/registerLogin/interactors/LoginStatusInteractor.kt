package com.sachtech.zozocar.ui.registerLogin.interactors

/**
 * Created by Sachtech Solutions.
 */
interface LoginStatusInteractor {
    fun onLoginSuccess(dismissRegister: Boolean = true)
}