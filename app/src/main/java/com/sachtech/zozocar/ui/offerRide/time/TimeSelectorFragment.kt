package com.sachtech.zozocar.ui.offerRide.time

import android.annotation.SuppressLint
import android.app.Activity
import android.app.TimePickerDialog
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.TimePicker
import com.sachtech.zozocar.R
import com.sachtech.zozocar.common.helper.RidePreparer
import com.sachtech.zozocar.constants.FlowType
import com.sachtech.zozocar.constants.IntentKey
import com.sachtech.zozocar.ui.offerRide.aboutride.AboutRideFragment
import com.sachtech.zozocar.ui.offerRide.middleseat.MiddleSeatFragmnet
import com.sachtech.zozocar.util.extension.maintainTwoLength
import gurtek.mrgurtekbase.KotlinBaseFragment
import kotlinx.android.synthetic.main.fragment_time_select.*
import java.util.*

class TimeSelectorFragment : KotlinBaseFragment(R.layout.fragment_time_select),
    TimePickerDialog.OnTimeSetListener {

    private var flowType: Int = FlowType.Normal

    private val calendar by lazy { Calendar.getInstance() }
    private val timePickerDialog by lazy {
        TimePickerDialog(
            activity!!,
            R.style.MyDialogTheme,
            this,
            calendar.get(Calendar.HOUR_OF_DAY),
            calendar.get(Calendar.MINUTE),
            true
        )
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        arguments?.let {
            flowType = it.getInt(IntentKey.FLOWTYPE, FlowType.Normal)
        }
        txtTime.setOnClickListener {
            timePickerDialog.show()
        }
        forward.setOnClickListener {
            when (flowType) {
                FlowType.Normal -> {
                    RidePreparer.setRideTime(
                        txtTime.text.toString(),
                        getTimeStamp(RidePreparer.getRide().rideDate)
                    )
                    baseListener.addFragment(MiddleSeatFragmnet::class, animation = true)
                }
                FlowType.Return -> {
                    RidePreparer.setReturnTime(
                        txtTime.text.toString(),
                        getTimeStamp(RidePreparer.getRide().rideDate)
                    )
                    baseListener.addFragment(
                        AboutRideFragment::class,
                        animation = true,
                        tag = System.currentTimeMillis().toString()
                    )
                }

                FlowType.PICKER -> {
                    activity?.setResult(Activity.RESULT_OK, Intent().apply {
                        putExtra(
                            IntentKey.TIME, this@TimeSelectorFragment.getTimeStamp(
                                arguments!!.getString(IntentKey.DATE, "")
                            )
                        )
                    })
                    activity?.finish()
                }
            }

        }
        btnClose.setOnClickListener {
            onBackPressed()
        }
    }

    private fun getTimeStamp(date: String): Long {
        if (date.isBlank())
            return System.currentTimeMillis()

        val cal = Calendar.getInstance()
        if (date.isNotBlank() && date.contains("-")) {
            val dateSplit = date.split("-")
            val time = txtTime.text.toString().split(":")
            cal.set(Calendar.DAY_OF_MONTH, dateSplit[0].toInt())
            cal.set(Calendar.MONTH, dateSplit[1].toInt())
            cal.set(Calendar.YEAR, dateSplit[2].toInt())
            cal.set(Calendar.HOUR_OF_DAY, time[0].toInt())
            cal.set(Calendar.MINUTE, time[1].toInt())
        }
        return cal.timeInMillis
    }

    @SuppressLint("SetTextI18n")
    override fun onTimeSet(view: TimePicker?, hourOfDay: Int, minute: Int) {
        txtTime.text = "$hourOfDay:${minute.toString().maintainTwoLength()}"
    }


}