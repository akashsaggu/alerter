package com.sachtech.zozocar.ui.profile.details

import android.os.Bundle
import com.sachtech.zozocar.R
import gurtek.mrgurtekbase.KotlinBaseActivity
import kotlinx.android.synthetic.main.activity_profile_photo.*


class ChooseProfilePhotoActivity:KotlinBaseActivity(R.id.chooseProfileContainer) {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile_photo)
        navigateToFragment<ChooseProfileFragment> {  }
        chooseProfilePhotoToolbar.setNavigationOnClickListener { onBackPressed() }

    }

}
