package com.sachtech.zozocar.ui.rides.current.publication.editPublication.passengerContribution.adapter

import androidx.core.content.res.ResourcesCompat
import com.sachtech.zozocar.R
import gurtek.mrgurtekbase.adapter.ItemsBaseAdapter
import kotlinx.android.synthetic.main.item_location_price.view.*

class PriceContributionAdapter : ItemsBaseAdapter<String>(R.layout.item_location_price) {
    override fun onBindViewHolder(holder: IViewHolder, position: Int) {
        if (position == 0)
            holder.itemView.txtPrice.typeface =
                ResourcesCompat.getFont(holder.itemView.context, R.font.montserrat_bold)

        holder.itemView.txtPrice.text = list[position]

    }

}