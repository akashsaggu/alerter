package com.sachtech.zozocar.ui.offerRide.stopover

import android.app.Activity
import android.content.Intent
import android.location.Address
import android.os.Bundle
import android.view.View
import com.sachtech.echelon.util.extension.setSolidRadiusRect
import com.sachtech.zozocar.R
import com.sachtech.zozocar.common.helper.RidePreparer
import com.sachtech.zozocar.constants.IntentKey
import com.sachtech.zozocar.ui.offerRide.date.DateSelectorFragment
import com.sachtech.zozocar.ui.offerRide.stopover.adapter.StopOverAdapter
import com.sachtech.zozocar.ui.offerRide.stopover.model.StopOver
import com.sachtech.zozocar.util.extension.getColorCompat
import gurtek.mrgurtekbase.KotlinBaseFragment
import kotlinx.android.synthetic.main.fragment_stopover.*

//todo max 5 stopover
class StopOverFragment : KotlinBaseFragment(R.layout.fragment_stopover) {


    private val stopOverAdapter by lazy {
        StopOverAdapter()
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        forceHideKeyboard()
        setupViews()
    }

    private fun setupViews() {
        btnConfirm.setSolidRadiusRect(getColorCompat(R.color.colorPrimary), 12)
        stopOvers.adapter = stopOverAdapter
        edtStopOver.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                startActivityForResult(
                    Intent(context!!, EnterStopOverActivity::class.java),
                    EnterStopOverActivity.StopOverDestination
                )
            }
        }
        btnConfirm.setOnClickListener {
            RidePreparer.addStopOvers(stopOverAdapter.getOriginalList().map { it.address })
            baseListener.addFragment(
                DateSelectorFragment::class,
                animation = true,
                tag = System.currentTimeMillis().toString()
            )
        }
        btnBack.setOnClickListener {
            onBackPressed()
        }
    }

    override fun onResume() {
        super.onResume()
        forceHideKeyboard()
        edtStopOver.clearFocus()
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {

            data?.let {
                addStopOver(it.extras?.getParcelable<Address>(IntentKey.ADDRESS)!!)
            }
        }
    }

    private fun addStopOver(place: Address) {
        stopOverAdapter.addtoList(StopOver(place))
    }

}