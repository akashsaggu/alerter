package com.sachtech.zozocar.ui.profile.account

import android.os.Bundle
import com.sachtech.zozocar.R
import com.sachtech.zozocar.constants.AccountOption
import com.sachtech.zozocar.constants.IntentKey
import gurtek.mrgurtekbase.KotlinBaseActivity

class AccountActivity : KotlinBaseActivity(R.id.accountContainer) {
    var accountOption: String? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_account)
        accountOption = intent.getStringExtra(IntentKey.ACCOUNTOPTION)
        when (accountOption) {
            AccountOption.RATING -> navigateToFragment<RatingFragment>()
            AccountOption.NOTIFICATION -> navigateToFragment<NotificationFragment>()
            AccountOption.CHANGEPASSWORD -> navigateToFragment<ChangePasswordFragment>()
            AccountOption.POSTALADDRESS -> navigateToFragment<PostalAddressFragment>()
            AccountOption.AVAILABLEFUNDS -> navigateToFragment<AvailableFundsFragment>()
            AccountOption.PAYMENTS -> navigateToFragment<PaymentsFragment>()
            AccountOption.BANKDETAILS -> navigateToFragment<BankDetailsFragment>()
            AccountOption.PASTTRANSFERS -> navigateToFragment<PastTransfersFragment>()
            AccountOption.HELP->navigateToFragment<HelpFragment>()
        }
    }
}