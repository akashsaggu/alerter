package com.sachtech.zozocar.ui.offerRide.stopover

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.google.android.libraries.places.api.Places
import com.google.android.libraries.places.api.model.AutocompletePrediction
import com.sachtech.zozocar.R
import com.sachtech.zozocar.constants.IntentKey
import com.sachtech.zozocar.ui.offerRide.stopover.adapter.StopOverPlaceAdapter
import com.sachtech.zozocar.util.extension.getAddress
import com.sachtech.zozocar.util.extension.observeTextChange
import com.sachtech.zozocar.util.recyclerview.ItemClickListener
import com.sachtech.zozocar.widget.ClearEditText.OnTextClearedListener
import kotlinx.android.synthetic.main.activity_enter_stop_over.*

class EnterStopOverActivity : AppCompatActivity(), ItemClickListener<AutocompletePrediction> {

    companion object {
        const val StopOverDestination = 111
    }

    private val placeAdapter by lazy { StopOverPlaceAdapter(Places.createClient(this), this) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_enter_stop_over)
        setViews()
    }

    private fun setViews() {
        placeRecycler.adapter = placeAdapter
        edtStopOver.requestFocus()
        edtStopOver.observeTextChange {
            placeAdapter.filter.filter(it)
        }
        edtStopOver.setOnTextClearedListener(object : OnTextClearedListener {
            override fun onTextCleared() {

            }

            override fun onLeftIconClick() {
                onBackPressed()
            }

        })

    }

    override fun onItemClick(item: AutocompletePrediction) {
        setResult(Activity.RESULT_OK, Intent().apply {
            putExtra(IntentKey.ADDRESS, item.getAddress())
        })
        finish()
    }

}
