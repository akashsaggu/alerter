package com.sachtech.zozocar.ui.offerRide.stopover.adapter

import android.view.View
import com.google.android.libraries.places.api.model.AutocompletePrediction
import com.google.android.libraries.places.api.net.PlacesClient
import com.sachtech.zozocar.R
import com.sachtech.zozocar.common.adapter.PlacesAdapter
import com.sachtech.zozocar.util.recyclerview.ItemClickListener
import kotlinx.android.synthetic.main.item_stopoverplace.view.*

class StopOverPlaceAdapter(
    placesClient: PlacesClient,
    private val itemClickListener: ItemClickListener<AutocompletePrediction>?
) : PlacesAdapter(placesClient, R.layout.item_stopoverplace), View.OnClickListener {
    override fun onBindViewHolder(holder: IViewHolder, position: Int) {
        holder.itemView.txtPlace.text = list[position].getPrimaryText(null)
        holder.itemView.tag = holder
        holder.itemView.setOnClickListener(this)

    }

    override fun onClick(v: View?) {
        itemClickListener?.onItemClick(list[(v?.tag as? IViewHolder)?.adapterPosition!!])
    }
}