package com.sachtech.zozocar.ui.profile.details.car.features.adapter

import android.widget.Filter
import android.widget.Filterable
import com.sachtech.zozocar.R
import gurtek.mrgurtekbase.adapter.ItemsBaseAdapter
import kotlinx.android.synthetic.main.item_simple_text.view.*

class SimpleStringAdapter : ItemsBaseAdapter<String>(R.layout.item_simple_text), Filterable {
    override fun onBindViewHolder(holder: IViewHolder, position: Int) {
        holder.itemView.itemText.text = list[position]
        holder.itemView.setOnClickListener {
            val viewPosition = holder.adapterPosition
            itemClickCallback?.onItemViewClicked(list[viewPosition], viewPosition)
        }
    }

    override fun getFilter(): Filter {
        return StringFilter()
    }


    inner class StringFilter : Filter() {
        override fun performFiltering(constraint: CharSequence?): FilterResults {

            val filterResults = FilterResults()
            if (constraint.isNullOrBlank()) {
                filterResults.values = duplicateList
                filterResults.count = duplicateList.size
            } else {
                filterResults.values =
                    duplicateList.filter { it.startsWith(constraint) } as ArrayList<String>
            }
            return filterResults

        }

        override fun publishResults(constraint: CharSequence?, results: FilterResults?) {
            addNewList(results!!.values as ArrayList<String>?)
        }
    }
}