package com.sachtech.zozocar.ui.profile.account

import android.os.Bundle
import android.view.View
import com.sachtech.zozocar.R
import gurtek.mrgurtekbase.KotlinBaseFragment
import kotlinx.android.synthetic.main.fragment_rating.*

class RatingFragment : KotlinBaseFragment(R.layout.fragment_rating) {


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        ratingToolbar.setNavigationOnClickListener { onBackPressed() }
    }
}
