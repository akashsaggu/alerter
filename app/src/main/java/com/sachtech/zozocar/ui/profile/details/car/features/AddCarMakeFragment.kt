package com.sachtech.zozocar.ui.profile.details.car.features

import android.os.Bundle
import android.view.View
import com.sachtech.zozocar.R
import com.sachtech.zozocar.common.helper.CarPreparer
import com.sachtech.zozocar.ui.profile.details.car.features.adapter.SimpleStringAdapter
import gurtek.mrgurtekbase.KotlinBaseFragment
import gurtek.mrgurtekbase.adapter.RecycleItemViewCallback
import kotlinx.android.synthetic.main.fragment_car_make.*

class AddCarMakeFragment : KotlinBaseFragment(R.layout.fragment_car_make) {
    private val popularAdapter by lazy { SimpleStringAdapter() }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setViews()

        /*   if (searchCar.text.isNotEmpty()) {
               carMakeContinue.visible()
               search_text.visible()
           } else {
               carMakeContinue.gone()
               search_text.gone()
       }*/
    }


    private fun setViews() {
        setOnClick()
        setRecyclerViews()
    }

    private fun setRecyclerViews() {
        populerMakesRecycler.adapter = popularAdapter
        popularAdapter.addNewList(arrayListOf("MARUTI", "HYUNDAI", "HONDA", "FORD", "TATA"))
        popularAdapter.itemClickCallback = object : RecycleItemViewCallback<String> {
            override fun onItemViewClicked(item: String?, position: Int) {
                CarPreparer.setCarMaker(item ?: "")
                baseListener.navigateToFragment(AddCarModelFragment::class)
            }

        }
    }

    private fun setOnClick() {
        carToolbar.setNavigationOnClickListener { onBackPressed() }
        searchCar.setOnClickListener { baseListener.navigateToFragment(CarMakeSuggestionFragment::class) }
    }
}
