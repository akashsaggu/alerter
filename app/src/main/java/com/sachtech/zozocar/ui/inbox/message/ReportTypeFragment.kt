package com.sachtech.zozocar.ui.inbox.message

import android.os.Bundle
import android.view.View
import com.sachtech.zozocar.R
import com.sachtech.zozocar.constants.IntentKey
import com.sachtech.zozocar.constants.ReportType
import gurtek.mrgurtekbase.KotlinBaseFragment
import kotlinx.android.synthetic.main.fragment_report_type.*

class ReportTypeFragment : KotlinBaseFragment(R.layout.fragment_report_type) {


    lateinit var arrayList: ArrayList<String>
    lateinit var reportTypeAdapter: ReportTypeAdapter
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        reportTypetoolbar.setNavigationOnClickListener { onBackPressed() }
        arrayList = ArrayList()
        reportType.setText(arguments?.getString(IntentKey.REPORTTYPE))
        if (arguments?.getString(IntentKey.REPORTTYPE)?.equals(ReportType.INAPPROPRIATE)!!)
            arrayList = arrayListOf(
                "Being unsafe on the road",
                "Being aggressive or offensive",
                "Inappropriate advances",
                "Transporting a package",
                "Transporting an unaccompanied animal",
                "A child travelling alone",
                "Illegal behaviour",
                "Sending spam"
            )
        else if (arguments?.getString(IntentKey.REPORTTYPE)?.equals(ReportType.WRONG_WITH_RiDE_OR_PROFILE)!!)
            arrayList = arrayListOf(
                "Duplicate profiles",
                "An offensive photo",
                "Using a photo of someone else",
                "A wrong or unavailable phone number",
                "A suspicious /fake profile",
                "Last minute cancellation"
            )
        else if (arguments?.getString(IntentKey.REPORTTYPE)?.equals(ReportType.SUSPICIOUS_PAYMENTS_OR_PRICE)!!)
            arrayList = arrayListOf(
                "Driving for profit",
                "Charging more than the listing",
                "A suspicious payment method"
            )
        reportTypeAdapter = ReportTypeAdapter(arrayList, baseListener)
        reportTypeRv.adapter=reportTypeAdapter
    }

}





