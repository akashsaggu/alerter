package com.sachtech.zozocar.ui.inbox.message

import android.os.Bundle
import com.sachtech.zozocar.R
import com.sachtech.zozocar.constants.IntentKey
import gurtek.mrgurtekbase.KotlinBaseActivity

class ChatActivity : KotlinBaseActivity(R.id.chatContainer) {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_chat)
        val ride=intent?.getStringExtra(IntentKey.RIDE)
        navigateToFragment(ChatFragment::class,Bundle().apply {
            putString(IntentKey.RIDE,ride)
        })
    }
}

