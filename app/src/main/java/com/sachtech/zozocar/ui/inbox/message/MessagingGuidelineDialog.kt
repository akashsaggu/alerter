package com.sachtech.zozocar.ui.inbox.message

import android.os.Bundle
import android.view.View
import com.sachtech.zozocar.R
import gurtek.mrgurtekbase.KotlinBaseDialogFragment
import kotlinx.android.synthetic.main.dialog_messaging_guideline.*

class MessagingGuidelineDialog :KotlinBaseDialogFragment(R.layout.dialog_messaging_guideline) {
    override fun adjustDisplay(): Boolean {
        return false
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        guidelineOk.setOnClickListener { dismiss() }
    }

}
