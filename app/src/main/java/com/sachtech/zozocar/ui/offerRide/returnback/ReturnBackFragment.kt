package com.sachtech.zozocar.ui.offerRide.returnback

import android.os.Bundle
import android.view.View
import com.sachtech.zozocar.R
import com.sachtech.zozocar.common.helper.RidePreparer
import com.sachtech.zozocar.constants.FlowType
import com.sachtech.zozocar.constants.IntentKey
import com.sachtech.zozocar.ui.offerRide.aboutride.AboutRideFragment
import com.sachtech.zozocar.ui.offerRide.date.DateSelectorFragment
import gurtek.mrgurtekbase.KotlinBaseFragment
import kotlinx.android.synthetic.main.fragment_returninfo.*

class ReturnBackFragment : KotlinBaseFragment(R.layout.fragment_returninfo) {


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        txtYes.setOnClickListener {
            RidePreparer.setHasReturn(true)
            baseListener.addFragment(
                DateSelectorFragment::class,
                animation = true,
                extras = Bundle().apply { putInt(IntentKey.FLOWTYPE, FlowType.Return) },
                tag = System.currentTimeMillis().toString()
            )
        }
        txtNo.setOnClickListener {
            baseListener.addFragment(
                AboutRideFragment::class,
                animation = true,
                tag = System.currentTimeMillis().toString()
            )
        }
        btnClose.setOnClickListener {
            onBackPressed()
        }
    }

    override fun onResume() {
        super.onResume()
        RidePreparer.setHasReturn(false)
    }
}