package com.sachtech.zozocar.ui.profile.account

import android.os.Bundle
import android.view.View
import com.sachtech.zozocar.R
import com.sachtech.zozocar.ui.offerRide.OfferRideActivity
import gurtek.mrgurtekbase.KotlinBaseFragment
import kotlinx.android.synthetic.main.fragment_available_funds.*

class AvailableFundsFragment : KotlinBaseFragment(R.layout.fragment_available_funds) {


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        availableFundToolbar.setNavigationOnClickListener { onBackPressed() }
        offerRide.setOnClickListener { baseListener.openA(OfferRideActivity::class) }
    }
}
