package com.sachtech.zozocar.ui.rides.current.publication.editPublication.passengerContribution

import android.os.Bundle
import android.view.View
import com.sachtech.echelon.util.extension.setSolidRadiusRect
import com.sachtech.zozocar.R
import com.sachtech.zozocar.ui.rides.current.publication.editPublication.passengerContribution.adapter.PriceContributionAdapter
import com.sachtech.zozocar.util.extension.getColorCompat
import gurtek.mrgurtekbase.KotlinBaseFragment
import kotlinx.android.synthetic.main.fragment_passengercontribution.*

class PassengerContributionFragment : KotlinBaseFragment(R.layout.fragment_passengercontribution) {


    private val adapter by lazy { PriceContributionAdapter() }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        btnSave.setSolidRadiusRect(getColorCompat(R.color.colorPrimary), 14)
        recyclerLocationPrice.adapter = adapter
        adapter.addNewList(listOf("300", "40", "150", "140"))
        btnClose.setOnClickListener {
            onBackPressed()
        }
    }
}