package com.sachtech.zozocar.ui.offerRide.middleseat

import android.os.Bundle
import android.view.View
import com.sachtech.zozocar.R
import com.sachtech.zozocar.common.helper.RidePreparer
import com.sachtech.zozocar.ui.offerRide.numberpassenger.NumberPassengerFragment
import gurtek.mrgurtekbase.KotlinBaseFragment
import kotlinx.android.synthetic.main.fragment_middleseat.*

class MiddleSeatFragmnet : KotlinBaseFragment(R.layout.fragment_middleseat) {


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        txtYes.setOnClickListener {
            RidePreparer.setEmptyMiddleSeat(true)
            baseListener.addFragment(NumberPassengerFragment::class, animation = true)
        }
        txtNo.setOnClickListener {
            RidePreparer.setEmptyMiddleSeat(false)
            baseListener.addFragment(NumberPassengerFragment::class, animation = true)
        }
        btnClose.setOnClickListener {
            onBackPressed()
        }
    }
}