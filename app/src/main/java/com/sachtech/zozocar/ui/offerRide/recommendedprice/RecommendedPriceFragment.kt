package com.sachtech.zozocar.ui.offerRide.recommendedprice

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.View
import com.sachtech.echelon.util.extension.setSolidRadiusRect
import com.sachtech.zozocar.R
import com.sachtech.zozocar.common.helper.RidePreparer
import com.sachtech.zozocar.ui.offerRide.returnback.ReturnBackFragment
import com.sachtech.zozocar.util.extension.getColorCompat
import gurtek.mrgurtekbase.KotlinBaseFragment
import kotlinx.android.synthetic.main.fragment_recommendedprice.*
import java.util.*

class RecommendedPriceFragment : KotlinBaseFragment(R.layout.fragment_recommendedprice) {


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        btnContinue.setSolidRadiusRect(getColorCompat(R.color.colorPrimary), radius = 12)
        setRecommendedPrice("545")
        setLocations()
        setViews()
    }

    private fun setViews() {
        btnContinue.setOnClickListener {
            if (icoArrowSource.text.isNotBlank()) {
                RidePreparer.setPerSeatPrice(icoArrowSource.text.toString().substring(1).toFloat())
                baseListener.addFragment(ReturnBackFragment::class, animation = true)
            }
        }
        txtDontagree.setOnClickListener {
            baseListener.addFragment(RecPriceDenyInfoFragment::class, animation = true)
        }
        btnClose.setOnClickListener {
            onBackPressed()
        }
    }

    @SuppressLint("SetTextI18n")
    fun setRecommendedPrice(price: String) {
        icoArrowSource.text = Currency.getInstance(Locale.getDefault()).symbol + price
    }

    private fun setLocations() {
        val ride = RidePreparer.getRide()
        txtLocations.text = "${ride.pickup?.locality ?: ride.pickup?.featureName
        ?: ""} \u00BB ${ride.dropOff?.locality ?: ride.dropOff?.featureName ?: ""}"
    }

}