package com.sachtech.zozocar.ui.offerRide.numberpassenger

import android.os.Bundle
import android.view.View
import com.sachtech.zozocar.R
import com.sachtech.zozocar.common.helper.RidePreparer
import com.sachtech.zozocar.ui.offerRide.instantbooking.InstantBookingFragment
import gurtek.mrgurtekbase.KotlinBaseFragment
import kotlinx.android.synthetic.main.fragment_numberpassanger.*

class NumberPassengerFragment : KotlinBaseFragment(R.layout.fragment_numberpassanger) {


    //todo total number of seats according to car of user, add plus and minus
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        forward.setOnClickListener {
            RidePreparer.setTotalAvailableSeats(txtPassengers.text.toString().toInt())
            baseListener.addFragment(InstantBookingFragment::class, animation = true)
        }
        btnClose.setOnClickListener {
            onBackPressed()
        }
    }
}