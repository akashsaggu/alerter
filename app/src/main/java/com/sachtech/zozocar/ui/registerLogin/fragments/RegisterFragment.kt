package com.sachtech.zozocar.ui.registerLogin.fragments

import android.app.Activity
import android.content.Context
import android.graphics.Rect
import android.os.Bundle
import android.view.View
import android.widget.ArrayAdapter
import com.sachtech.echelon.util.extension.setSolidRadiusRect
import com.sachtech.zozocar.R
import com.sachtech.zozocar.ui.registerLogin.RegisterLoginFragment
import com.sachtech.zozocar.ui.registerLogin.interactors.RegisterLoginInteractor
import com.sachtech.zozocar.util.extension.*
import com.wajahatkarim3.easyvalidation.core.view_ktx.validEmail
import com.wajahatkarim3.easyvalidation.core.view_ktx.validator
import gurtek.mrgurtekbase.KotlinBaseFragment
import kotlinx.android.synthetic.main.fragment_register.*
import java.util.*
import kotlin.collections.ArrayList


/**
 * Created by Sachtech Solutions.
 */
class RegisterFragment : KotlinBaseFragment(R.layout.fragment_register) {
    var registerLoginInteractor: RegisterLoginInteractor? = null


    override fun onAttach(context: Context) {
        super.onAttach(context)
        registerLoginInteractor = parentFragment as? RegisterLoginFragment
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setViews()
    }

    private fun setViews() {
        btnSignUpEmail.setSolidRadiusRect(getColorCompat(R.color.colorPrimary), radius = 12)
        val rect = Rect()
        activity?.window?.decorView?.getWindowVisibleDisplayFrame(rect)
        facebookButton.setPadding(
            ((rect.width() - (facebookButton.getMarginLayoutParams()?.leftMargin ?: 1
            * 2)) * 0.049).toInt(), 0, 0, 0
        )


        fun setSpinner() {
            val genderAdater =
                ArrayAdapter<String>(
                    activity!!,
                    android.R.layout.simple_spinner_item,
                    arrayOf<String>("Male", "Female")
                )

            spinnerGender.setOnSpinnerItemClickListener { position: Int, itemAtPosition: String? ->

            }
            spinnerGender.setAdapter(genderAdater)
            val years = getYearsTillEighteenYearOld()
            val birthAdapter =
                ArrayAdapter<String>(
                    activity!!,
                    android.R.layout.simple_spinner_item,
                    years
                )
            (context as? Activity)?.window
            spinnerBirth.setOnSpinnerItemClickListener { position: Int, itemAtPosition: String? ->

            }
            spinnerBirth.setAdapter(birthAdapter)
        }


        fun setOnclickListeners() {
            btnSignUpEmail.setOnClickListener {
                if (containerRegister.visibility == View.VISIBLE) {
                    submitForm()
                } else {
                    containerRegister.visible()
                }
            }
            textLogin.setOnClickListener {
                registerLoginInteractor?.goToLogin()
            }
            facebookButton.setOnClickListener {
                registerLoginInteractor?.doFacebookLogin()
            }

            txtTerm.makeLinks(
                "T&Cs" to View.OnClickListener { },
                "Privacy Policy" to View.OnClickListener { })
        }
        setSpinner()
        setOnclickListeners()
    }

    private fun getYearsTillEighteenYearOld(): ArrayList<String> {
        val years = ArrayList<String>()
        val thisYear = Calendar.getInstance().get(Calendar.YEAR)
        for (i in 1940..(thisYear - 18)) {
            years.add(i.toString())
        }
        return years
    }

    private fun submitForm() {

        if (validateFields()) {
            registerLoginInteractor?.doEmailRegister(
                spinnerGender.selectedItem,
                edtFname.text.toString(),
                edtLname.text.toString(),
                spinnerBirth.selectedItem,
                edtEmail.text.toString(),
                edtPass.text.toString()
            )
        }
    }

    private fun validateFields(): Boolean {
        var isValid: Boolean = true


        if (spinnerGender.selectedItem == null) {
            warningGender.visible()
            isValid = false
        } else {
            warningGender.gone()
        }

        if (!edtFname.validator().nonEmpty().check()) {
            edtFname.error = getString(R.string.fieldReq)
            isValid = false
        }

        if (!edtLname.validator().nonEmpty().check()) {
            edtLname.error = getString(R.string.fieldReq)
            isValid = false
        }

        if (spinnerBirth.selectedItem == null) {
            warningBirth.visible()
            isValid = false
        } else {
            warningBirth.gone()
        }

        if (!edtEmail.validEmail()) {
            edtEmail.error = getString(R.string.validEmail)
            isValid = false
        }



        if (!edtPass.validator().minLength(8).nonEmpty().check()) {
            edtPass.error = getString(R.string.validminpass)
            isValid = false
        }

        if (edtConfirmPass.text.toString() != edtPass.text.toString()) {
            edtConfirmPass.error = getString(R.string.validConfPass)
            isValid = false
        }



        return isValid
    }


}