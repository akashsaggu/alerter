package com.sachtech.zozocar.ui.profile.account

import android.os.Bundle
import android.view.View
import com.sachtech.zozocar.R
import gurtek.mrgurtekbase.KotlinBaseFragment
import kotlinx.android.synthetic.main.fragment_notification.*

class NotificationFragment : KotlinBaseFragment(R.layout.fragment_notification) {


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        btnClose.setOnClickListener { onBackPressed() }
        pushNotification.setOnClickListener { baseListener.navigateToFragment(PushNotificationFragment::class) }
        email.setOnClickListener { baseListener.navigateToFragment(EmailFragment::class) }
        sms.setOnClickListener { baseListener.navigateToFragment(SmsFragment::class) }
    }

}
