package com.sachtech.zozocar.ui.profile.details.addMobile.vm

import androidx.lifecycle.MutableLiveData
import com.sachtech.zozocar.app.getPref
import com.sachtech.zozocar.base.BaseViewModel
import com.sachtech.zozocar.constants.PrefKey
import com.sachtech.zozocar.networking.model.UserModel

class AddMobileVm : BaseViewModel() {

    private val userLiveData by lazy { MutableLiveData<UserModel.Data>() }
    val mobileLiveData: MutableLiveData<UserModel.Data>
        get() {
            return userLiveData
        }

    fun addMobileNumber(phoneNumber: String) {
        getRemoteRepo().addUserPhone(
            getPref().getprefObject<UserModel.Data>(PrefKey.USER).uId,
            phoneNumber
        ).subscribeWithProgressAndDisposable {
            userLiveData.value = it.data
        }
    }
}