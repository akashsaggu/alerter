package com.sachtech.zozocar.ui

import android.os.Bundle
import com.sachtech.zozocar.R
import com.sachtech.zozocar.ui.profile.ProfileFragment
import gurtek.mrgurtekbase.KotlinBaseActivity

class TestActivity : KotlinBaseActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_test)
        //addFragment(TimeSelectorFragment::class, animation = true)
        addFragment(ProfileFragment::class, animation = true)
    }
}
