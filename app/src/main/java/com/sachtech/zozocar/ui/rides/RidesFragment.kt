package com.sachtech.zozocar.ui.rides

import android.os.Bundle
import android.view.View
import com.sachtech.zozocar.R
import com.sachtech.zozocar.app.getPref
import com.sachtech.zozocar.constants.IntentKey
import com.sachtech.zozocar.constants.PrefKey
import com.sachtech.zozocar.networking.model.UserModel
import com.sachtech.zozocar.ui.registerLogin.RegisterLoginFragment
import com.sachtech.zozocar.ui.registerLogin.interactors.LoginStatusInteractor
import com.sachtech.zozocar.ui.rides.current.CurrentRideFragment
import com.sachtech.zozocar.ui.rides.history.HistoryRideFragment
import com.sachtech.zozocar.util.extension.isNotNull
import gurtek.mrgurtekbase.KotlinBaseFragment
import gurtek.mrgurtekbase.adapter.ViewPagerAdapter
import kotlinx.android.synthetic.main.fragment_rides.*

/**
 * Created by Sachtech Solutions.
 */
class RidesFragment : KotlinBaseFragment(R.layout.fragment_rides), LoginStatusInteractor {


    private val viewPagerAdapter by lazy {
        ViewPagerAdapter(
            childFragmentManager,
            arrayListOf(CurrentRideFragment(), HistoryRideFragment()),
            titles = arrayListOf("Current", "History"),
            showTitle = true
        )
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val user = getPref().getprefObject<UserModel.Data>(PrefKey.USER)
        if (user.isNotNull()) {
            setViewPager()
            childFragmentManager.popBackStackImmediate()
        } else {
            baseListener.addChildFragment(
                childFragmentManager,
                R.id.rootContainer,
                animation = false,
                kClass = RegisterLoginFragment::class,
                bundle = Bundle().apply {
                    putInt(IntentKey.RLTYPE, arguments!!.getInt(IntentKey.RLTYPE))
                }
            )
        }

    }

    private fun setViewPager() {
        ridesViewPager.adapter = viewPagerAdapter
        ridesTablayout.setupWithViewPager(ridesViewPager)
    }


    override fun onLoginSuccess(dismissRegister: Boolean) {
        if (dismissRegister)
        childFragmentManager.popBackStackImmediate()
        setViewPager()
    }


}