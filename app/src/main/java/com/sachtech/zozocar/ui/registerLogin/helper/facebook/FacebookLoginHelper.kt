package com.sachtech.zozocar.ui.registerLogin.helper.facebook

import android.content.Intent
import androidx.fragment.app.Fragment
import com.facebook.CallbackManager
import com.facebook.FacebookCallback
import com.facebook.FacebookException
import com.facebook.login.LoginBehavior
import com.facebook.login.LoginManager
import com.facebook.login.LoginResult
import com.sachtech.appl.ui.loginresgister.helper.facebook.FacebookCallbackListener


class FacebookLoginHelper(
    private val fragment: Fragment,
    private val facebookCallbackListener: FacebookCallbackListener? = null
) :
    FacebookCallback<LoginResult> {

    private val fbInstace = LoginManager.getInstance().apply {
        loginBehavior = LoginBehavior.WEB_VIEW_ONLY
    }
    var callbackManager: CallbackManager? = null

    fun init() {
        initCallback()
        registerCallback()
    }

    private fun initCallback() {
        callbackManager = CallbackManager.Factory.create()
    }

    private fun registerCallback() {
        fbInstace.registerCallback(callbackManager, this)
    }

    fun doLogin() {
        fbInstace.logInWithReadPermissions(
            fragment,
            arrayListOf("public_profile", "email", "user_birthday")
        )
    }

    fun onActivityResult(requestCode: Int, resultcode: Int, data: Intent?) {
        callbackManager?.onActivityResult(requestCode, resultcode, data)
    }

    fun logout() {
        fbInstace.logOut()
    }


    override fun onSuccess(result: LoginResult?) {
        facebookCallbackListener?.onFacebookLoginSuccess(result)
    }

    override fun onCancel() {

        facebookCallbackListener?.onUserCancel()
    }

    override fun onError(error: FacebookException?) {

        facebookCallbackListener?.onError(error)
    }

}