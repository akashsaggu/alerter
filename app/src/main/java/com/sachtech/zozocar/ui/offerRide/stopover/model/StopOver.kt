package com.sachtech.zozocar.ui.offerRide.stopover.model

import android.location.Address

data class StopOver(val address: Address, var selected: Boolean = true)