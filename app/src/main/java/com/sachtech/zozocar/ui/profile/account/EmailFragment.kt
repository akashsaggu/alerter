package com.sachtech.zozocar.ui.profile.account

import android.os.Bundle
import android.view.View
import com.sachtech.zozocar.R
import gurtek.mrgurtekbase.KotlinBaseFragment
import kotlinx.android.synthetic.main.fragment_email.*


class EmailFragment:KotlinBaseFragment(R.layout.fragment_email) {


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setOnClick()
    }
    private fun setOnClick() {
        btnClose.setOnClickListener { onBackPressed() }
        publishing.setOnClickListener {
            if (!publishing.isSelected) {
                publishing.isChecked = true
                publishing.isSelected = true
            } else {
                publishing.isChecked = false
                publishing.isSelected = false
            }
        }

        news.setOnClickListener {
            if (!news.isSelected) {
                news.isChecked = true
                news.isSelected = true
            } else {
                news.isChecked = false
                news.isSelected = false
            }
        }
        messages.setOnClickListener{
            if (!messages.isSelected) {
                messages.isChecked = true
                messages.isSelected = true
            } else {
                messages.isChecked = false
                messages.isSelected = false
            }
        }
        ratings.setOnClickListener {
            if (!ratings.isSelected) {
                ratings.isChecked = true
                ratings.isSelected = true
            } else {
                ratings.isChecked = false
                ratings.isSelected = false
            }
        }
    }
}
