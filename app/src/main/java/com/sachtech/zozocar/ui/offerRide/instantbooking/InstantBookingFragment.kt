package com.sachtech.zozocar.ui.offerRide.instantbooking

import android.os.Bundle
import android.view.View
import com.sachtech.zozocar.R
import com.sachtech.zozocar.common.helper.RidePreparer
import com.sachtech.zozocar.ui.offerRide.recommendedprice.RecommendedPriceFragment
import gurtek.mrgurtekbase.KotlinBaseFragment
import kotlinx.android.synthetic.main.fragment_instantbooking.*

class InstantBookingFragment : KotlinBaseFragment(R.layout.fragment_instantbooking) {


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        txtYes.setOnClickListener {
            RidePreparer.setCanBookInstantly(true)
            baseListener.addFragment(RecommendedPriceFragment::class, animation = true)
        }
        txtNo.setOnClickListener {
            RidePreparer.setCanBookInstantly(false)
            baseListener.addFragment(RecommendedPriceFragment::class, animation = true)
        }
        btnClose.setOnClickListener {
            onBackPressed()
        }
    }

}