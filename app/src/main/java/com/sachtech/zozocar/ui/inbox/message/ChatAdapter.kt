package com.sachtech.zozocar.ui.inbox.message

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.sachtech.zozocar.R
import com.sachtech.zozocar.common.helper.Message
import gurtek.mrgurtekbase.listeners.KotlinBaseListener
import kotlinx.android.synthetic.main.item_chat_receiver.view.*
import kotlinx.android.synthetic.main.item_chat_sender.view.*

class ChatAdapter(
    val context: Context,
    val messages: ArrayList<Message>,
    val baseListener: KotlinBaseListener
) : RecyclerView.Adapter<ChatAdapter.MessageViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MessageViewHolder {
       return if (viewType == 0)
            MessageViewHolder(
                LayoutInflater.from(parent.context!!).inflate(
                    R.layout.item_chat_sender,
                    parent,
                    false
                )
            )
        else MessageViewHolder(
            LayoutInflater.from(parent.context!!).inflate(
                R.layout.item_chat_receiver,
                parent,
                false
            )
        )

    }

    override fun getItemCount(): Int {
        return messages.size
    }

    override fun onBindViewHolder(holder: MessageViewHolder, position: Int) {
        if(messages[position].isBelongToUser)
        {
            holder.itemView.senderMessage.setText(messages[position].message)
            holder.itemView.messageTime.setText(messages[position].messageTime)
        }
        else
        {
            holder.itemView.receiverMessage.setText(messages[position].message)
            holder.itemView.messagetime.setText(messages[position].messageTime)
            holder.itemView.reportMessage.setOnClickListener { baseListener.navigateToFragment(ReportFragment::class) }

        }

    }


    override fun getItemViewType(position: Int): Int {
        val message = messages[position]
        return if (message.isBelongToUser) {
            0
        } else {
            1
        }
    }


    inner class MessageViewHolder(view: View) : RecyclerView.ViewHolder(view)
}
