package com.sachtech.zozocar.ui.profile.details

import android.os.Bundle
import android.view.animation.AnimationUtils
import android.widget.ArrayAdapter
import com.sachtech.zozocar.R
import com.sachtech.zozocar.constants.DetailType
import com.sachtech.zozocar.constants.IntentKey
import com.sachtech.zozocar.util.extension.openActivity
import gurtek.mrgurtekbase.KotlinBaseActivity
import kotlinx.android.synthetic.main.activity_edit_profile.*

class EditProfileActivity : KotlinBaseActivity() {


    var detail:String?=null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_profile)
        detail=intent?.getStringExtra(IntentKey.DETAILTYPE)
        if(detail!=null) {
            if (detail == DetailType.BIO) {
                editProfileScroll.post{
                    bioEditText.requestFocus()
                    editProfileScroll.scrollTo(0,bioEditText.bottom)
                }
            }
            else if (detail == DetailType.EMAIL) {
                editProfileScroll.post {
                    email.requestFocus()
                    editProfileScroll.scrollTo(0,email.bottom)
                }
            }
        }
        setYearSpinner()
        setOnClick()
    }

    private fun setOnClick() {
        editProfileToolbar.setNavigationOnClickListener { onBackPressed() }
        editProfileImage.setOnClickListener { openActivity<ChooseProfilePhotoActivity>() }
        textChoosePhoto.setOnClickListener { openActivity<ChooseProfilePhotoActivity>() }
        bioEditText.setOnFocusChangeListener { v, hasFocus ->
            if(hasFocus)
               textBio.startAnimation(AnimationUtils.loadAnimation(this,R.anim.alerter_slide_in_from_bottom))
        else if(!hasFocus)
                textBio.startAnimation(AnimationUtils.loadAnimation(this,R.anim.alerter_slide_out_to_bottom))
        }
    }

    private fun setYearSpinner() {
        val adapter = ArrayAdapter(this, android.R.layout.simple_spinner_item, getYears())
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerYear.adapter = adapter
    }

    private fun getYears(): ArrayList<Int> {
        val yearArray = ArrayList<Int>()
        for (i in 1919..2001) {
            yearArray.add(i)
        }
        return yearArray
    }
}