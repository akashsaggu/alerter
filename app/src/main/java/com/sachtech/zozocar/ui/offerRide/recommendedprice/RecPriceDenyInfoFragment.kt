package com.sachtech.zozocar.ui.offerRide.recommendedprice

import android.os.Bundle
import android.view.View
import com.sachtech.echelon.util.extension.setSolidRadiusRect
import com.sachtech.zozocar.R
import com.sachtech.zozocar.ui.offerRide.editprice.EditPriceFragment
import com.sachtech.zozocar.util.extension.getColorCompat
import gurtek.mrgurtekbase.KotlinBaseFragment
import kotlinx.android.synthetic.main.fragment_recpricedenyinfo.*

class RecPriceDenyInfoFragment : KotlinBaseFragment(R.layout.fragment_recpricedenyinfo) {


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        btnEditAmt.setSolidRadiusRect(getColorCompat(R.color.colorPrimary), radius = 12)
        btnEditAmt.setOnClickListener {
            baseListener.addFragment(EditPriceFragment::class, animation = true)
        }
        btnClose.setOnClickListener {
            onBackPressed()
        }
    }
}