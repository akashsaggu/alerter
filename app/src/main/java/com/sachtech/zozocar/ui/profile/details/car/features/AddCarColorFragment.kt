package com.sachtech.zozocar.ui.profile.details.car.features

import android.os.Bundle
import android.view.Gravity
import android.view.View
import android.widget.LinearLayout
import android.widget.RadioButton
import androidx.lifecycle.Observer
import com.sachtech.zozocar.R
import com.sachtech.zozocar.networking.model.CarColors
import com.sachtech.zozocar.ui.profile.details.car.vm.AddCarVm
import gurtek.mrgurtekbase.KotlinBaseFragment
import gurtek.mrgurtekbase.gone
import gurtek.mrgurtekbase.visible
import kotlinx.android.synthetic.main.fragment_car_color.*
import org.koin.androidx.viewmodel.ext.android.sharedViewModel

class AddCarColorFragment : KotlinBaseFragment(R.layout.fragment_car_color) {


    private val viewModel: AddCarVm by sharedViewModel()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setEventObserver()
        setViews()
        viewModel.getCarColors()
    }

    private fun setViews() {
        carToolbar.setNavigationOnClickListener { onBackPressed() }
        if (carColorRadioGroup.checkedRadioButtonId != -1) {
            carColorContinue.visible()
        } else {
            carColorContinue.gone()
        }
        carColorRadioGroup.setOnCheckedChangeListener { group, checkedId ->
            /*  val color=(group.checkedRadioButtonId as RadioButton).text.toString()
              CarPreparer.setCarColor(color)*/
            baseListener.navigateToFragment(AddCarYearRegistered::class)
        }
    }

    private fun setEventObserver() {
        viewModel.colorsLd.observe(viewLifecycleOwner, Observer {
            if (!it.isNullOrEmpty())
                setCarColor(it)
        })
    }

    private fun setCarColor(list: List<CarColors.Data>) {
        //val carType = context?.resources?.getStringArray(R.array.car_color)
        for (element in list) {
            val radioButton = RadioButton(context!!)
            val layoutParams = LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT
            )
            radioButton.tag = element
            radioButton.id = View.generateViewId()
            radioButton.layoutParams = layoutParams
            radioButton.text = element.colorName
            radioButton.layoutDirection = View.LAYOUT_DIRECTION_RTL
            radioButton.gravity = Gravity.START or Gravity.CENTER
            radioButton.textAlignment = View.TEXT_ALIGNMENT_GRAVITY
            carColorRadioGroup.addView(radioButton)
        }
    }
}