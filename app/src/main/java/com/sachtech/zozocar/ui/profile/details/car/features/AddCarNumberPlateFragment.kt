package com.sachtech.zozocar.ui.profile.details.car.features

import android.os.Bundle
import android.view.View
import com.sachtech.zozocar.R
import gurtek.mrgurtekbase.KotlinBaseFragment
import kotlinx.android.synthetic.main.fragment_car_number_plate.*

class AddCarNumberPlateFragment : KotlinBaseFragment(R.layout.fragment_car_number_plate) {


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        carToolbar.setNavigationOnClickListener { onBackPressed() }
        carNumberPlateContinue.setOnClickListener { baseListener.navigateToFragment(
            AddCarMakeFragment::class) }
    }
}
