package com.sachtech.zozocar.ui.profile.details.car.features

import android.os.Bundle
import android.view.View
import com.sachtech.zozocar.R
import gurtek.mrgurtekbase.KotlinBaseFragment
import kotlinx.android.synthetic.main.fragment_car_model.*

class AddCarModelFragment :KotlinBaseFragment(R.layout.fragment_car_model){


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setOnClick()

      /*  if (searchCarModel.text.isNotEmpty()) {
            carModelContinue.visible()
            search_text.visible()
            popularModelview.gone()
        } else {
            carModelContinue.gone()
            search_text.gone()
            popularModelview.visible()

        }*/
    }

    private fun setOnClick() {
        carToolbar.setNavigationOnClickListener { onBackPressed() }
        /*searchCarModel.setOnClickListener { baseListener.navigateToFragment(
            CarMakeSuggestionFragment::class) }*/
        carModelContinue.setOnClickListener { baseListener.navigateToFragment(AddCarBodyTypeFragment::class) }
    }

}
