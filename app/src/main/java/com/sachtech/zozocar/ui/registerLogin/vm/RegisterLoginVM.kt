package com.sachtech.zozocar.ui.registerLogin.vm

import androidx.lifecycle.MutableLiveData
import com.sachtech.zozocar.base.BaseViewModel
import com.sachtech.zozocar.networking.model.UserModel
import com.sachtech.zozocar.networking.postModel.UserPostModel

class RegisterLoginVM : BaseViewModel() {
    private val userLiveData: MutableLiveData<UserModel.Data> = MutableLiveData()
    val loginRegisterObserver get() = userLiveData


    fun doSignUp(userPostModel: UserPostModel) {
        getRemoteRepo().registerUser(userPostModel).subscribeWithProgressAndDisposable {
            userLiveData.value = it.data
        }
    }

    fun doLogin(userEmail: String, userPassword: String) {
        getRemoteRepo().loginUser(userEmail, userPassword).subscribeWithProgressAndDisposable {
            userLiveData.value = it.data
        }
    }

}