package com.sachtech.zozocar.ui.registerLogin

import android.content.Intent
import android.os.Bundle
import androidx.annotation.LayoutRes
import com.facebook.AccessToken
import com.facebook.FacebookException
import com.facebook.GraphRequest
import com.facebook.login.LoginResult
import com.sachtech.appl.ui.loginresgister.helper.facebook.FacebookCallbackListener
import com.sachtech.zozocar.ui.registerLogin.helper.facebook.FacebookLoginHelper
import com.sachtech.zozocar.ui.registerLogin.helper.facebook.model.FacebookUser
import gurtek.mrgurtekbase.KotlinBaseFragment

abstract class BaseLoginRegisterFragment(@LayoutRes viewA: Int) : KotlinBaseFragment(viewA),
    FacebookCallbackListener {
    private val facebookLoginHelper by lazy {
        FacebookLoginHelper(this, this)
            .apply { init() }
    }


    fun doFacebookLogin() {
        showProgress()
        facebookLoginHelper.doLogin()
    }

    override fun onFacebookLoginSuccess(result: LoginResult?) {
        val accessToken: AccessToken = result?.accessToken!!
        //val profile: Profile = Profile.getCurrentProfile()

        val request: GraphRequest = GraphRequest.newMeRequest(
            accessToken
        ) { jsonObject, response ->
            hideProgress()
            try {
                val name = jsonObject.getString("name")

                val email = jsonObject.getString("email")
                val picture = jsonObject.getJSONObject("picture")
                val data = picture.getJSONObject("data")
                val url = data.getString("url")
                val token = jsonObject.getString("id")
                var fbUser = FacebookUser(name, email, url, token)
                onFacebookSignInSuccess(fbUser)
            } catch (e: Exception) {
                showErrorMessage(e.localizedMessage ?: "")
            }
        }
        val parameters = Bundle()
        parameters.putString("fields", "id, gender, name, picture.type(large), email")
        request.parameters = parameters
        request.executeAsync()
    }

    abstract fun onFacebookSignInSuccess(fbUser: FacebookUser)

    override fun onUserCancel() {
        hideProgress()

    }

    override fun onError(error: FacebookException?) {
        hideProgress()
        onFacebookError(error)
    }

    abstract fun onFacebookError(error: FacebookException?)

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        facebookLoginHelper.onActivityResult(requestCode, resultCode, data)
    }


}