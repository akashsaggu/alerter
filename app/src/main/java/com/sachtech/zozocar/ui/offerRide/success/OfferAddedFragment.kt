package com.sachtech.zozocar.ui.offerRide.success

import android.os.Bundle
import android.view.View
import com.sachtech.echelon.util.extension.setSolidRadiusRect
import com.sachtech.zozocar.R
import gurtek.mrgurtekbase.KotlinBaseFragment
import kotlinx.android.synthetic.main.fragment_offer_added.*

class OfferAddedFragment : KotlinBaseFragment(R.layout.fragment_offer_added) {


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        btnOk.setSolidRadiusRect(radius = 50)
        btnOk.setOnClickListener {
            activity?.finish()
        }
    }

}