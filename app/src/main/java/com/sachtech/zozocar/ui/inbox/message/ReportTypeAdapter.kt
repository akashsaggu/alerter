package com.sachtech.zozocar.ui.inbox.message

import android.os.Bundle
import com.sachtech.zozocar.R
import com.sachtech.zozocar.constants.IntentKey
import gurtek.mrgurtekbase.adapter.BaseAdapter
import gurtek.mrgurtekbase.listeners.KotlinBaseListener
import kotlinx.android.synthetic.main.item_report_type.view.*

class ReportTypeAdapter(
    val arrayList: ArrayList<String>,
    val baseListener: KotlinBaseListener
) :BaseAdapter<String>(R.layout.item_report_type) {
    override fun onBindViewHolder(holder: IViewHolder, position: Int) {
        holder.itemView.itemReportType.setText(arrayList[position])
        holder.itemView.setOnClickListener { baseListener.navigateToFragment(ReportReasonFragment::class,
            Bundle().apply { putString(IntentKey.REPORTREASON,arrayList[position]) }
        ) }
    }

    override fun getItemCount(): Int {
        return arrayList.size
    }
}