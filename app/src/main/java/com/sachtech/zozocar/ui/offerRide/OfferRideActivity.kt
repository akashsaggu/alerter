package com.sachtech.zozocar.ui.offerRide

import android.os.Bundle
import com.sachtech.zozocar.R
import com.sachtech.zozocar.common.helper.RidePreparer
import com.sachtech.zozocar.ui.offerRide.pickup.PickUpFragment
import gurtek.mrgurtekbase.KotlinBaseActivity

//fixme empty RidePreparer field on each activity resumed
class OfferRideActivity : KotlinBaseActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        RidePreparer.reset()
        setBottomUpAnimation(R.transition.frombottom, R.transition.fromtop)
        addFragment(PickUpFragment::class, animation = false)
    }

    override fun onDestroy() {
        super.onDestroy()
        RidePreparer.reset()
    }
}
