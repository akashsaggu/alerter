package com.sachtech.zozocar.ui.inbox

import android.os.Bundle
import android.view.View
import com.sachtech.zozocar.R
import com.sachtech.zozocar.app.getPref
import com.sachtech.zozocar.constants.IntentKey
import com.sachtech.zozocar.constants.PrefKey
import com.sachtech.zozocar.networking.model.UserModel
import com.sachtech.zozocar.ui.inbox.message.MessageFragment
import com.sachtech.zozocar.ui.inbox.notification.InboxNotificationsFragment
import com.sachtech.zozocar.ui.registerLogin.RegisterLoginFragment
import com.sachtech.zozocar.ui.registerLogin.interactors.LoginStatusInteractor
import com.sachtech.zozocar.util.extension.isNotNull
import gurtek.mrgurtekbase.KotlinBaseFragment
import gurtek.mrgurtekbase.adapter.ViewPagerAdapter
import kotlinx.android.synthetic.main.fragment_inbox.*

class InboxFragment : KotlinBaseFragment(R.layout.fragment_inbox), LoginStatusInteractor {

    private var user = getPref().getprefObject<UserModel.Data>(PrefKey.USER)


    private val viewPagerAdapter by lazy {
        ViewPagerAdapter(
            childFragmentManager,
            arrayListOf(MessageFragment(), InboxNotificationsFragment()),
            titles = arrayListOf("Messages", "Notifications"),
            showTitle = true
        )
    }

    override fun onResume() {
        super.onResume()
        user = getPref().getprefObject<UserModel.Data>(PrefKey.USER)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if (user.isNotNull()) {
            setUpViewPager()
        } else {
            baseListener.addChildFragment(
                childFragmentManager,
                R.id.inboxContainer,
                animation = false,
                kClass = RegisterLoginFragment::class,
                bundle = Bundle().apply {
                    putInt(IntentKey.RLTYPE, arguments!!.getInt(IntentKey.RLTYPE))
                }
            )
        }
    }

    private fun setUpViewPager() {
        inboxViewPager.adapter = viewPagerAdapter
        inboxTablayout.setupWithViewPager(inboxViewPager)
    }

    override fun onLoginSuccess(dismissRegister: Boolean) {
        if (dismissRegister)
        childFragmentManager.popBackStackImmediate()
        setUpViewPager()
    }
}