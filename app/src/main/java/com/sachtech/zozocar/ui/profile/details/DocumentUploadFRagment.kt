package com.sachtech.zozocar.ui.profile.details

import android.os.Bundle
import android.view.View
import com.sachtech.zozocar.R
import gurtek.mrgurtekbase.KotlinBaseFragment
import kotlinx.android.synthetic.main.fragment_upload_document.*

class DocumentUploadFRagment:KotlinBaseFragment(R.layout.fragment_upload_document) {


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        btnClose.setOnClickListener { onBackPressed() }
    }

}
