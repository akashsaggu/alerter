package com.sachtech.zozocar.ui.inbox.message

import android.os.Bundle
import android.view.View
import com.google.gson.Gson
import com.sachtech.zozocar.R
import com.sachtech.zozocar.common.helper.RideBooking
import com.sachtech.zozocar.constants.IntentKey
import com.sachtech.zozocar.util.extension.getDateInformat
import gurtek.mrgurtekbase.KotlinBaseFragment
import kotlinx.android.synthetic.main.fragment_message.*
import java.util.*
import kotlin.collections.ArrayList

class MessageFragment : KotlinBaseFragment(R.layout.fragment_message), OnMessageItemSelected {
    override fun bookingSelected(rideBooking: RideBooking) {
        baseListener.openA(ChatActivity::class, Bundle().apply {
            putString(IntentKey.RIDE, Gson().toJson(rideBooking))
        })
    }

    private lateinit var messageAdapter: MessageAdapter
    lateinit var bookingList: ArrayList<RideBooking>


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        bookingList = ArrayList()
        bookingList.add(
            RideBooking(
                "Mohali",
                "Chandigarh",
                "Sat 30Nov,09-00",
                Date().time.getDateInformat(),
                "RideUser"
            )
        )
        messageAdapter = MessageAdapter(bookingList, this)
        messageRv.adapter = messageAdapter
    }

}