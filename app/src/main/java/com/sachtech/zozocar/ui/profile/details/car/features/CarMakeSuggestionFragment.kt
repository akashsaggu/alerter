package com.sachtech.zozocar.ui.profile.details.car.features

import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import com.sachtech.zozocar.R
import com.sachtech.zozocar.common.helper.CarPreparer
import com.sachtech.zozocar.constants.IntentKey
import com.sachtech.zozocar.ui.profile.details.car.features.adapter.SimpleStringAdapter
import com.sachtech.zozocar.ui.profile.details.car.vm.AddCarVm
import com.sachtech.zozocar.util.extension.observeTextChange
import gurtek.mrgurtekbase.KotlinBaseFragment
import gurtek.mrgurtekbase.adapter.RecycleItemViewCallback
import gurtek.mrgurtekbase.gone
import gurtek.mrgurtekbase.visible
import kotlinx.android.synthetic.main.fragment_car_make_suggestions.*
import org.koin.androidx.viewmodel.ext.android.sharedViewModel


class CarMakeSuggestionFragment : KotlinBaseFragment(R.layout.fragment_car_make_suggestions) {

    companion object {
        const val SEARCHMAKER = 0
        const val SEARCHMODEL = 1
    }

    private val adapter by lazy { SimpleStringAdapter() }

    private val viewModel: AddCarVm by sharedViewModel()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setViews()
        setEventObserver()
        arguments?.let {
            if (it.getInt(IntentKey.CarSearchType, SEARCHMAKER) == SEARCHMAKER) {
                viewModel.getCarMakerBrands()
            } else {
                viewModel.getCarMakerBrandModels(it.getString(IntentKey.CARMAKER, ""))
            }
        }
    }

    private fun setEventObserver() {
        viewModel.carBrandsLd.observe(viewLifecycleOwner, Observer { data ->
            if (!data.isNullOrEmpty()) {
                val brandNameList = data.map { it.carBrand } as ArrayList<String>
                adapter.duplicateList = brandNameList
                adapter.addNewList(brandNameList)
            }
        })

        viewModel.carModelsLd.observe(viewLifecycleOwner, Observer { data ->
            if (!data.isNullOrEmpty()) {
                val brandModelList = data.map { it.carModel } as ArrayList<String>
                adapter.duplicateList = brandModelList
                adapter.addNewList(brandModelList)
            }
        })

    }


    //fixme navigate accoring to search type after click
    private fun setViews() {
        edtCarSearch.observeTextChange {
            adapter.filter.filter(it)
        }
        if (edtCarSearch.text?.isNotEmpty() == false)
            carMakeSuggestionView.visible()
        else carMakeSuggestionView.gone()

        adapter.itemClickCallback = object : RecycleItemViewCallback<String> {
            override fun onItemViewClicked(item: String?, position: Int) {
                arguments?.let {
                    if (it.getInt(IntentKey.CarSearchType, SEARCHMAKER) == SEARCHMAKER) {
                        CarPreparer.setCarMaker(item!!)
                    } else {
                        CarPreparer.setCarModel(item!!)
                    }
                }
            }
        }
        recylerResultCar.adapter = adapter
    }

}
