package com.sachtech.zozocar.ui.registerLogin.interactors

/**
 * Created by Sachtech Solutions.
 */
interface RegisterLoginInteractor {

    fun doFacebookLogin()
    fun doEmailLogin(email: String, password: String)
    fun doEmailRegister(
        gender: String,
        fname: String,
        lname: String,
        dob: String,
        email: String,
        password: String
    )

    fun goToLogin()
    fun goToRegister()
    fun goToAddMobile()

}