package com.sachtech.zozocar.ui.landing

import android.os.Bundle
import android.view.Window
import android.view.WindowManager
import com.sachtech.echelon.util.extension.setSolidRadiusRect
import com.sachtech.zozocar.R
import com.sachtech.zozocar.constants.IntentKey
import com.sachtech.zozocar.ui.home.HomeActivity
import com.sachtech.zozocar.ui.registerLogin.RegisterLoginFragment
import com.sachtech.zozocar.util.extension.delay
import com.sachtech.zozocar.util.extension.getColorCompat
import com.sachtech.zozocar.util.extension.openActivity
import gurtek.mrgurtekbase.KotlinBaseActivity
import kotlinx.android.synthetic.main.activity_landing.*

class LandingActivity : KotlinBaseActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )
        setContentView(R.layout.activity_landing)
        //backgroundImage.loadImage(R.drawable.background)
        btnSignUp.setSolidRadiusRect(getColorCompat(R.color.colorPrimary), radius = 8)
        btnSignUp.setOnClickListener {
            openActivity<HomeActivity> {
                putExtra(IntentKey.RLTYPE, RegisterLoginFragment.Register)
            }
            delay(500) {
                finish()
            }
        }
        btnLogin.setOnClickListener {
            openActivity<HomeActivity> {
                putExtra(IntentKey.RLTYPE, RegisterLoginFragment.Login)
            }
            delay(500) {

                finish()
            }
        }
    }

    override fun containsOnlyFragment(): Boolean {
        return true
    }

}
