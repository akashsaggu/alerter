package com.sachtech.zozocar.ui.registerLogin.helper.facebook.model

data class FacebookUser(val email: String?, val name: String, val url: String, val token: String)