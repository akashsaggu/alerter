package com.sachtech.zozocar.ui.profile.details.car.features

import android.os.Bundle
import android.view.Gravity
import android.view.View
import android.widget.LinearLayout
import android.widget.RadioButton
import androidx.lifecycle.Observer
import com.sachtech.zozocar.R
import com.sachtech.zozocar.common.helper.CarPreparer
import com.sachtech.zozocar.networking.model.CarBodyType
import com.sachtech.zozocar.ui.profile.details.car.vm.AddCarVm
import gurtek.mrgurtekbase.KotlinBaseFragment
import gurtek.mrgurtekbase.gone
import gurtek.mrgurtekbase.visible
import kotlinx.android.synthetic.main.fragment_add_car_type.*
import org.koin.androidx.viewmodel.ext.android.sharedViewModel

class AddCarBodyTypeFragment : KotlinBaseFragment(R.layout.fragment_add_car_type) {

    private val viewModel: AddCarVm by sharedViewModel()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setEventObservers()
        setViews()
        viewModel.getCarBodyType()
    }

    private fun setViews() {
        carToolbar.setNavigationOnClickListener { onBackPressed() }
        if (carTypeRadioGroup.checkedRadioButtonId != -1) {
            carTypeContinue.visible()
        } else {
            carTypeContinue.gone()
        }
        carTypeRadioGroup.setOnCheckedChangeListener { group, checkedId ->
            CarPreparer.setCarType((carTypeRadioGroup.findViewById<RadioButton>(checkedId).tag as CarBodyType.Data))
            baseListener.navigateToFragment(AddCarColorFragment::class)
        }
    }

    private fun setEventObservers() {
        viewModel.bodyTypeLd.observe(viewLifecycleOwner, Observer {
            if (!it.isNullOrEmpty())
                setCarType(it)
        })
    }

    private fun setCarType(list: List<CarBodyType.Data>) {
        /*val carTypeImage = arrayOf(
            R.drawable.ic_hatchback_car,
            R.drawable.ic_sedan_car,
            R.drawable.ic_convertible_car,
            R.drawable.ic_estate_car,
            R.drawable.ic_suv_car,
            R.drawable.ic_station_wagon_car,
            R.drawable.ic_minivan_car,
            R.drawable.ic_van_car
        )
        val carType = resources.getStringArray(R.array.car_type)*/
        for (i in list.indices) {
            val radioButton = RadioButton(context!!)
            val layoutParams = LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT
            )
            radioButton.tag = list[i]
            radioButton.id = View.generateViewId()
            radioButton.layoutParams = layoutParams
            radioButton.text = list[i].typeName
            radioButton.layoutDirection = View.LAYOUT_DIRECTION_RTL
            radioButton.gravity = Gravity.START or Gravity.CENTER
            radioButton.textAlignment = View.TEXT_ALIGNMENT_GRAVITY
            radioButton.compoundDrawablePadding = 16
            //radioButton.setCompoundDrawablesWithIntrinsicBounds(carTypeImage[i], 0, 0, 0)

            carTypeRadioGroup.addView(radioButton)
        }
    }
}