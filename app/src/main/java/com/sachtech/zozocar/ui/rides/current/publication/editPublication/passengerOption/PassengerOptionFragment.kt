package com.sachtech.zozocar.ui.rides.current.publication.editPublication.passengerOption

import android.os.Bundle
import android.view.View
import com.sachtech.echelon.util.extension.setSolidRadiusRect
import com.sachtech.zozocar.R
import com.sachtech.zozocar.util.extension.getColorCompat
import gurtek.mrgurtekbase.KotlinBaseFragment
import kotlinx.android.synthetic.main.fragment_passanger_option.*

class PassengerOptionFragment : KotlinBaseFragment(R.layout.fragment_passanger_option) {


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        btnSave.setSolidRadiusRect(getColorCompat(R.color.colorPrimary), 14)
        btnClose.setOnClickListener {
            onBackPressed()
        }
    }
}