package com.sachtech.zozocar.ui.profile.details

import android.net.Uri
import android.os.Bundle
import android.view.View
import com.mlsdev.rximagepicker.RxImagePicker
import com.mlsdev.rximagepicker.Sources
import com.sachtech.echelon.util.extension.rotateImageLeft
import com.sachtech.echelon.util.extension.rotateImageRight
import com.sachtech.zozocar.R
import com.sachtech.zozocar.util.extension.toast
import gurtek.mrgurtekbase.KotlinBaseFragment
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_profile_photo_upload.*


class ProfilePhotoUploadFragment : KotlinBaseFragment(R.layout.fragment_profile_photo_upload) {
    var uri: Uri? = null


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        uri = Uri.parse(arguments?.getString("uri"))
        cropImage(uri)
        setOnClick()
    }

    private fun setOnClick() {
        rotateLeft.setOnClickListener { crop_view.setImage(rotateImageLeft(activity!!,uri!!)) }
        rotateRight.setOnClickListener { crop_view.setImage(rotateImageRight(activity!!,uri!!)) }
        textChooseAnotherPhoto.setOnClickListener {
            RxImagePicker.with(fragmentManager!!).requestImage(Sources.CHOOSER, "Choose a photo")
                .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    uri=it
                    cropImage(it)
                }, {
                    toast(it.localizedMessage!!)
                })
        }
    }

    private fun cropImage(uri: Uri?) {
        crop_view.setImageUri(uri)
        crop_view.setCropSaveCompleteListener { toast("successful") }
        crop_view.setErrorListener { toast(it.localizedMessage) }
    }

}
