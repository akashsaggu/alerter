package com.sachtech.zozocar.ui.inbox.message

import android.content.Intent
import android.os.Bundle
import android.view.View
import com.sachtech.zozocar.R
import com.sachtech.zozocar.constants.IntentKey
import com.sachtech.zozocar.ui.home.HomeActivity
import com.sachtech.zozocar.util.extension.toast
import gurtek.mrgurtekbase.KotlinBaseFragment
import kotlinx.android.synthetic.main.fragment_report_reason.*

class ReportReasonFragment : KotlinBaseFragment(R.layout.fragment_report_reason) {


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        reportReasonToolbar.setNavigationOnClickListener { onBackPressed() }
        reportReason.setText("You are reporting user for ${arguments?.getString(IntentKey.REPORTREASON)}")
        report.setOnClickListener {
            if(reportReasonText.text.isNotEmpty()) {
                toast("Reported Successfully")
                val intent = Intent(activity!!, HomeActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
                intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
                context?.startActivity(intent)
            }

          }
    }
}