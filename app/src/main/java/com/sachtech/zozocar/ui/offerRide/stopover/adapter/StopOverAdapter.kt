package com.sachtech.zozocar.ui.offerRide.stopover.adapter

import android.view.View
import com.sachtech.zozocar.R
import com.sachtech.zozocar.ui.offerRide.stopover.model.StopOver
import gurtek.mrgurtekbase.adapter.ItemsBaseAdapter
import kotlinx.android.synthetic.main.item_stopover.view.*

class StopOverAdapter : ItemsBaseAdapter<StopOver>(R.layout.item_stopover), View.OnClickListener {
    override fun onBindViewHolder(holder: IViewHolder, position: Int) {
        val data = list[position]
        holder.itemView.checkStopover.isChecked = data.selected
        holder.itemView.txtPlace.text = data.address.getAddressLine(0)
        holder.itemView.tag = holder
        holder.itemView.setOnClickListener(this)

    }

    override fun onClick(v: View?) {
        val viewHolder = v?.tag as? IViewHolder
        viewHolder?.let {
            list[it.adapterPosition].selected = !list[it.adapterPosition].selected
            notifyItemChanged(it.adapterPosition)
        }
    }

}