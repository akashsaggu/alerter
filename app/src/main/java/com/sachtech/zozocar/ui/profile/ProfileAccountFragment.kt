package com.sachtech.zozocar.ui.profile

import android.os.Bundle
import android.view.View
import com.sachtech.zozocar.R
import com.sachtech.zozocar.constants.AccountOption
import com.sachtech.zozocar.constants.IntentKey
import com.sachtech.zozocar.ui.profile.account.AccountActivity
import gurtek.mrgurtekbase.KotlinBaseFragment
import kotlinx.android.synthetic.main.fragment_profile_account.*

class ProfileAccountFragment : KotlinBaseFragment(R.layout.fragment_profile_account) {


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setOnClick()
    }

    private fun setOnClick() {
        seeRating.setOnClickListener { navigateToOption(AccountOption.RATING) }
        notification.setOnClickListener { navigateToOption(AccountOption.NOTIFICATION) }
        changePassword.setOnClickListener { navigateToOption(AccountOption.CHANGEPASSWORD) }
        postalAddress.setOnClickListener { navigateToOption(AccountOption.POSTALADDRESS) }
        availableFunds.setOnClickListener { navigateToOption(AccountOption.AVAILABLEFUNDS) }
        payments.setOnClickListener { navigateToOption(AccountOption.PAYMENTS) }
        bankDetails.setOnClickListener { navigateToOption(AccountOption.BANKDETAILS) }
        pastTransfers.setOnClickListener { navigateToOption(AccountOption.PASTTRANSFERS) }
        help.setOnClickListener { navigateToOption(AccountOption.HELP) }
    }

    private fun navigateToOption(option: String) {
        baseListener.openA(AccountActivity::class, Bundle().apply {
            putString(IntentKey.ACCOUNTOPTION, option)
        })
    }
}