package com.sachtech.zozocar.ui.home

import android.os.Bundle
import androidx.fragment.app.Fragment
import com.sachtech.zozocar.R
import com.sachtech.zozocar.app.getPref
import com.sachtech.zozocar.constants.IntentKey
import com.sachtech.zozocar.constants.PrefKey
import com.sachtech.zozocar.networking.model.UserModel
import com.sachtech.zozocar.ui.findride.FindRideFragment
import com.sachtech.zozocar.ui.inbox.InboxFragment
import com.sachtech.zozocar.ui.offerRide.OfferRideActivity
import com.sachtech.zozocar.ui.profile.ProfileFragment
import com.sachtech.zozocar.ui.registerLogin.RegisterLoginFragment
import com.sachtech.zozocar.ui.registerLogin.interactors.LoginStatusInteractor
import com.sachtech.zozocar.ui.rides.RidesFragment
import com.sachtech.zozocar.util.extension.isNotNull
import com.sachtech.zozocar.util.extension.openActivity
import gurtek.mrgurtekbase.KotlinBaseActivity
import kotlinx.android.synthetic.main.activity_home.*

class HomeActivity : KotlinBaseActivity(R.id.containerMain), LoginStatusInteractor {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        setViews()
        navigateToMenuFragment<RidesFragment>()
    }


    private fun setViews() {
        bottomNavigationView.setOnNavigationItemSelectedListener {
            when (it.itemId) {
                R.id.nav_rides -> {
                    navigateToMenuFragment<RidesFragment>()
                }
                R.id.nav_search -> {
                    navigateToMenuFragment<FindRideFragment>()
                }
                R.id.nav_offer -> {
                    if (getPref().getprefObject<UserModel.Data>(PrefKey.USER).isNotNull()) {
                        openActivity<OfferRideActivity>()
                        return@setOnNavigationItemSelectedListener false
                    } else {
                        navigateToMenuFragment<RegisterLoginFragment>()
                    }
                }
                R.id.nav_inbox -> {
                    navigateToMenuFragment<InboxFragment>()
                }
                R.id.nav_profile -> {
                    navigateToMenuFragment<ProfileFragment>()
                }
            }
            true
        }
    }

    private inline fun <reified T> navigateToMenuFragment() where T : Fragment {
        navigateToFragment<T> {
            intent?.let {
                putInt(
                    IntentKey.RLTYPE,
                    intent.getIntExtra(
                        IntentKey.RLTYPE,
                        RegisterLoginFragment.Login
                    )
                )
            }
        }
    }

    override fun onLoginSuccess(dismissRegister: Boolean) {
        bottomNavigationView.menu.getItem(0).isChecked = true
        navigateToMenuFragment<RidesFragment>()
    }
}
