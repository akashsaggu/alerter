package com.sachtech.zozocar.ui.rides.history.adapter

import androidx.appcompat.widget.PopupMenu
import com.sachtech.zozocar.R
import com.sachtech.zozocar.common.activty.FragmentoActivity
import com.sachtech.zozocar.constants.FragmentoType
import com.sachtech.zozocar.constants.IntentKey
import com.sachtech.zozocar.ui.home.HomeActivity
import com.sachtech.zozocar.util.extension.getActivity
import com.sachtech.zozocar.util.extension.openActivity
import gurtek.mrgurtekbase.adapter.ItemsBaseAdapter
import kotlinx.android.synthetic.main.item_common_ride.view.*

class HistoryAdapter : ItemsBaseAdapter<String>(R.layout.item_common_ride) {

    override fun getItemCount(): Int {
        return 1
    }

    override fun onBindViewHolder(holder: IViewHolder, position: Int) {
        holder.itemView.imgOption.setOnClickListener { v ->
            var popupMenu: PopupMenu? = PopupMenu(v.context, v)
            popupMenu?.menuInflater?.inflate(R.menu.history_item, popupMenu.menu)
            popupMenu?.setOnDismissListener { popupMenu = null }
            popupMenu?.show()
        }
        holder.itemView.setOnClickListener {
            holder.getActivity<HomeActivity>()?.openActivity<FragmentoActivity> {
                putExtra(
                    IntentKey.FRAGMENTOTYPE,
                    FragmentoType.RIDEPLAN
                )
            }
        }
    }


}