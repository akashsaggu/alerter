package com.sachtech.zozocar.ui.findride.ridedetail

import android.os.Bundle
import android.view.View
import com.sachtech.echelon.util.extension.setSolidRadiusRect
import com.sachtech.zozocar.R
import com.sachtech.zozocar.ui.findride.ridedetail.adapter.BookedUserAdapter
import com.sachtech.zozocar.util.extension.getColorCompat
import gurtek.mrgurtekbase.KotlinBaseFragment
import kotlinx.android.synthetic.main.fragment_search_ride_detail.*
import java.util.*

class SearchRideDetailFragment : KotlinBaseFragment(R.layout.fragment_search_ride_detail) {


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setData()
        members_rv.adapter = BookedUserAdapter()
        btnConfirm.setSolidRadiusRect(getColorCompat(R.color.colorPrimary), 14)
    }

    private fun setData() {
        price.text = Currency.getInstance(Locale.getDefault()).symbol + "290"
    }

}