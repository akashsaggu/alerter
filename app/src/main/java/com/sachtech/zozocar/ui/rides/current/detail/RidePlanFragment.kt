package com.sachtech.zozocar.ui.rides.current.detail

import android.os.Bundle
import android.view.View
import com.sachtech.zozocar.R
import com.sachtech.zozocar.ui.rides.current.publication.PublicationFragment
import gurtek.mrgurtekbase.KotlinBaseFragment
import kotlinx.android.synthetic.main.fragmnet_ride_plan.*

class RidePlanFragment : KotlinBaseFragment(R.layout.fragmnet_ride_plan) {


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        btnClose.setOnClickListener {
            onBackPressed()
        }
        containerPublication.setOnClickListener {
            baseListener.addFragment(PublicationFragment::class, animation = false)
        }
    }


}