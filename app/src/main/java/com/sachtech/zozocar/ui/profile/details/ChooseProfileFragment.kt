package com.sachtech.zozocar.ui.profile.details

import android.os.Bundle
import android.view.View
import com.mlsdev.rximagepicker.RxImagePicker
import com.mlsdev.rximagepicker.Sources
import com.sachtech.echelon.util.extension.withPermissions
import com.sachtech.zozocar.R
import com.sachtech.zozocar.util.extension.toast
import gurtek.mrgurtekbase.KotlinBaseFragment
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_choose_profile_photo.*

class ChooseProfileFragment : KotlinBaseFragment(R.layout.fragment_choose_profile_photo) {


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setOnClick()

    }

    private fun setOnClick() {
        choosePhoto.setOnClickListener {
            arrayOf(android.Manifest.permission.READ_EXTERNAL_STORAGE,android.Manifest.permission.CAMERA,android.Manifest.permission.WRITE_EXTERNAL_STORAGE).withPermissions(activity!!,{
                RxImagePicker.with(fragmentManager!!).requestImage(Sources.CHOOSER, "Choose a photo")
                    .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                    .subscribe({
                        baseListener.navigateToFragment(ProfilePhotoUploadFragment::class, Bundle().apply {
                            this.putString("uri", it.toString())
                        })
                    }, {
                        toast(it.localizedMessage!!)
                    })
            },200)

        }
    }

}
