package com.sachtech.zozocar.ui.findride.searchresult

import android.os.Bundle
import android.view.View
import com.sachtech.zozocar.R
import com.sachtech.zozocar.ui.findride.searchresult.adapter.RideSearchAdapter
import gurtek.mrgurtekbase.KotlinBaseFragment
import kotlinx.android.synthetic.main.fragment_ridesearchresult.*

class RideSearchResultFragment : KotlinBaseFragment(R.layout.fragment_ridesearchresult) {

    private val adapter by lazy { RideSearchAdapter() }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        recyclerRides.adapter = adapter
    }

}