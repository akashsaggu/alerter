package com.sachtech.zozocar.ui.profile.account

import android.os.Bundle
import android.view.View
import com.sachtech.zozocar.R
import gurtek.mrgurtekbase.KotlinBaseFragment
import kotlinx.android.synthetic.main.fragment_past_transfers.*

class PastTransfersFragment : KotlinBaseFragment(R.layout.fragment_past_transfers) {


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        pastTransferToolbar.setNavigationOnClickListener { onBackPressed() }
    }
}
