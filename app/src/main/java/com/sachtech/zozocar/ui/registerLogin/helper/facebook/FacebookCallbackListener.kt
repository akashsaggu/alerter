package com.sachtech.appl.ui.loginresgister.helper.facebook

import com.facebook.FacebookException
import com.facebook.login.LoginResult

interface FacebookCallbackListener {
    fun onFacebookLoginSuccess(result: LoginResult?)

    fun onUserCancel()

    fun onError(error: FacebookException?)
}