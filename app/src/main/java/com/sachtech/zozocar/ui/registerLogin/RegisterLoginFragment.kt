package com.sachtech.zozocar.ui.registerLogin

import android.content.Context
import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import com.facebook.FacebookException
import com.sachtech.zozocar.R
import com.sachtech.zozocar.app.getPref
import com.sachtech.zozocar.constants.IntentKey
import com.sachtech.zozocar.constants.PrefKey
import com.sachtech.zozocar.networking.postModel.ProviderType
import com.sachtech.zozocar.networking.postModel.UserPostModel
import com.sachtech.zozocar.ui.profile.details.addMobile.AddMobileFragment
import com.sachtech.zozocar.ui.registerLogin.fragments.LoginFragment
import com.sachtech.zozocar.ui.registerLogin.fragments.RegisterFragment
import com.sachtech.zozocar.ui.registerLogin.helper.facebook.model.FacebookUser
import com.sachtech.zozocar.ui.registerLogin.interactors.LoginStatusInteractor
import com.sachtech.zozocar.ui.registerLogin.interactors.RegisterLoginInteractor
import com.sachtech.zozocar.ui.registerLogin.vm.RegisterLoginVM
import com.sachtech.zozocar.util.extension.isNotNull
import org.koin.androidx.viewmodel.ext.android.viewModel

/**
 * Created by Sachtech Solutions.
 */

class RegisterLoginFragment : BaseLoginRegisterFragment(R.layout.fragment_registerlogin),
    RegisterLoginInteractor {
    private val viewModel: RegisterLoginVM by viewModel()

    private var loginStatusInteractor: LoginStatusInteractor? = null

    companion object {
        const val Login = 0
        const val Register = 1
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        if (arguments != null && arguments?.getInt(IntentKey.RLTYPE, Register) == Register) {
            goToRegister()
        } else {
            goToLogin()
        }
        if (parentFragment.isNotNull()) {
            loginStatusInteractor = parentFragment as? LoginStatusInteractor
        }
        setViewModel(viewModel)
        addEventObservers()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        loginStatusInteractor = context as? LoginStatusInteractor
    }

    private fun addEventObservers() {
        viewModel.loginRegisterObserver.observe(viewLifecycleOwner, Observer {
            getPref().setprefObject(PrefKey.USER, it)
            if (!it?.uPhone.isNullOrBlank()) {
                loginStatusInteractor?.onLoginSuccess()
            } else {
                loginStatusInteractor?.onLoginSuccess(dismissRegister = false)
                goToAddMobile()
            }
        })
    }

    override fun onFacebookSignInSuccess(fbUser: FacebookUser) {
        viewModel.doSignUp(
            UserPostModel(
                provider = ProviderType.FACEBOOK,
                firstName = fbUser.name,
                email = if (fbUser.email.isNullOrBlank()) "-" else fbUser.email,
                fcmToken = "sdf",
                photoUrl = fbUser.url,
                facebookToken = fbUser.token
            )
        )
    }

    override fun onFacebookError(error: FacebookException?) {
        showErrorMessage(error?.localizedMessage ?: "Sorry, something went wrong.")
    }

    override fun doEmailLogin(email: String, password: String) {
        viewModel.doLogin(email, password)
    }

    override fun doEmailRegister(
        gender: String,
        fname: String,
        lname: String,
        dob: String,
        email: String,
        password: String
    ) {
        viewModel.doSignUp(
            UserPostModel(
                provider = ProviderType.EMAIL,
                firstName = fname,
                lastName = lname,
                email = email,
                birthYear = dob,
                fcmToken = "s",
                gender = gender,
                password = password
            )
        )
    }


    override fun goToRegister() {
        baseListener.replaceChildFragment(
            childFragmentManager,
            R.id.registerloginContainer,
            RegisterFragment::class,
            animation = false
        )
    }

    override fun goToLogin() {
        baseListener.replaceChildFragment(
            childFragmentManager,
            R.id.registerloginContainer,
            LoginFragment::class,
            animation = false
        )
    }

    override fun goToAddMobile() {
        baseListener.replaceChildFragment(
            childFragmentManager,
            R.id.registerloginContainer,
            AddMobileFragment::class,
            animation = false
        )
    }


}
