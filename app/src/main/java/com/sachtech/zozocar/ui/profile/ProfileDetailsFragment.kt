package com.sachtech.zozocar.ui.profile

import android.os.Bundle
import android.view.View
import android.widget.PopupMenu
import com.sachtech.zozocar.R
import com.sachtech.zozocar.constants.DetailType
import com.sachtech.zozocar.constants.IntentKey
import com.sachtech.zozocar.ui.profile.details.EditProfileActivity
import com.sachtech.zozocar.ui.profile.details.car.AddCarActivity
import com.sachtech.zozocar.util.extension.openActivity
import gurtek.mrgurtekbase.KotlinBaseFragment
import kotlinx.android.synthetic.main.fragment_profile_details.*

class ProfileDetailsFragment : KotlinBaseFragment(R.layout.fragment_profile_details) {


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setOnClick()
    }


    private fun setOnClick() {
        verification_menu.setOnClickListener { openMenu() }
        addCar.setOnClickListener { navigateToOption(DetailType.CAR) }
        addPreferences.setOnClickListener { navigateToOption(DetailType.PREFERENCES) }
        verifyMyId.setOnClickListener { navigateToOption(DetailType.VERIFY) }
        contact.setOnClickListener { navigateToOption(DetailType.MOBILE) }
        mini_bio.setOnClickListener {
            baseListener.openA(EditProfileActivity::class, Bundle().apply {
                putString(IntentKey.DETAILTYPE, DetailType.BIO)
            })
        }
        publicProfile.setOnClickListener { navigateToOption(DetailType.PUBLICPROFILE) }
    }


    private fun navigateToOption(option: String) {
        /*baseListener.openA(DetailActivity::class, Bundle().apply {
            putString(IntentKey.DETAILTYPE, option)
        })*/
        openActivity<AddCarActivity>()
    }

    private fun openMenu() {
        PopupMenu(context!!, verification_menu).apply {
            setOnMenuItemClickListener {
                when (it.itemId) {
                    R.id.item_edit_email -> {
                        baseListener.openA(EditProfileActivity::class, Bundle().apply {
                            putString(IntentKey.DETAILTYPE, DetailType.EMAIL)
                        })
                        return@setOnMenuItemClickListener true
                    }
                    R.id.item_edit_phone -> {
                        navigateToOption(DetailType.MOBILE)
                        return@setOnMenuItemClickListener true
                    }
                    else -> return@setOnMenuItemClickListener false
                }
            }
            inflate(R.menu.edit_verification_menu)
            show()
        }
    }
}