package com.sachtech.zozocar.ui.rides.current.publication

import android.os.Bundle
import android.view.View
import com.sachtech.zozocar.R
import com.sachtech.zozocar.ui.rides.current.publication.editPublication.passengerContribution.PassengerContributionFragment
import com.sachtech.zozocar.ui.rides.current.publication.editPublication.passengerOption.PassengerOptionFragment
import gurtek.mrgurtekbase.KotlinBaseFragment
import kotlinx.android.synthetic.main.fragment_your_publication.*

class PublicationFragment : KotlinBaseFragment(R.layout.fragment_your_publication) {


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        btnClose.setOnClickListener {
            onBackPressed()
        }
        containerPrice.setOnClickListener {
            baseListener.addFragment(PassengerContributionFragment::class)
        }
        containerOptions.setOnClickListener {
            baseListener.addFragment(PassengerOptionFragment::class)
        }
    }
}