package com.sachtech.zozocar.ui.profile

import android.os.Bundle
import android.view.View
import android.widget.PopupMenu
import androidx.core.content.res.ResourcesCompat
import com.google.android.material.appbar.AppBarLayout
import com.sachtech.zozocar.R
import com.sachtech.zozocar.app.getPref
import com.sachtech.zozocar.constants.IntentKey
import com.sachtech.zozocar.constants.PrefKey
import com.sachtech.zozocar.networking.model.UserModel
import com.sachtech.zozocar.ui.profile.details.ChooseProfilePhotoActivity
import com.sachtech.zozocar.ui.profile.details.EditProfileActivity
import com.sachtech.zozocar.ui.registerLogin.RegisterLoginFragment
import com.sachtech.zozocar.ui.registerLogin.interactors.LoginStatusInteractor
import com.sachtech.zozocar.util.extension.isNotNull
import gurtek.mrgurtekbase.KotlinBaseFragment
import gurtek.mrgurtekbase.adapter.ViewPagerAdapter
import gurtek.mrgurtekbase.gone
import kotlinx.android.synthetic.main.fragment_profile.*

class ProfileFragment : KotlinBaseFragment(R.layout.fragment_profile), LoginStatusInteractor {

    private var user = getPref().getprefObject<UserModel.Data>(PrefKey.USER)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if (user.isNotNull()) {
            setViews()
        } else {
            baseListener.addChildFragment(
                childFragmentManager,
                R.id.profileContainer,
                animation = false,
                kClass = RegisterLoginFragment::class,
                bundle = Bundle().apply {
                    putInt(IntentKey.RLTYPE, arguments!!.getInt(IntentKey.RLTYPE))
                }
            )
        }
    }

    private fun setViews() {
        setAppBarOffset()
        setUpViewPager()
        setOnClick()
    }

    override fun onResume() {
        super.onResume()
        user = getPref().getprefObject<UserModel.Data>(PrefKey.USER)
    }


    private fun setAppBarOffset() {
        val sp = ResourcesCompat.getFont(activity!!, R.font.montserrat_regular)
        collapseToolbartLayout.setCollapsedTitleTypeface(sp)
        collapseToolbartLayout.setExpandedTitleTypeface(sp)
        profile_appbar.addOnOffsetChangedListener(object : AppBarLayout.OnOffsetChangedListener {
            var isShow = false
            var scrollRange = -1
            override fun onOffsetChanged(p0: AppBarLayout?, p1: Int) {
                if (scrollRange == -1) {
                    scrollRange = p0?.totalScrollRange!!
                }
                if (scrollRange + p1 == 0) {
                    isShow = true
                    collapseToolbartLayout.title = profile_username.text.toString()
                    collapseToolbartLayout.isTitleEnabled = true

                } else if (isShow) {
                    isShow = false
                    profile_title.gone()
                    htab_toolbar.title = "Profile"
                    collapseToolbartLayout.isTitleEnabled = false
                }
            }
        })
    }

    private fun setOnClick() {
        htab_header_image.setOnClickListener { baseListener.openA(ChooseProfilePhotoActivity::class) }
        profile_username.setOnClickListener { baseListener.openA(EditProfileActivity::class) }
        htab_menu.setOnClickListener {
            openOptionsMenu()
        }
    }

    private fun openOptionsMenu() {
        PopupMenu(context!!, htab_menu).apply {
            setOnMenuItemClickListener {
                when (it.itemId) {
                    R.id.item_edit_details -> {
                        baseListener.openA(EditProfileActivity::class, Bundle())
                        return@setOnMenuItemClickListener true
                    }
                    R.id.item_edit_photo -> {
                        baseListener.openA(ChooseProfilePhotoActivity::class)
                        return@setOnMenuItemClickListener true
                    }
                    else -> return@setOnMenuItemClickListener false
                }
            }
            inflate(R.menu.edit_profile_menu)
            show()
        }
    }

    private fun setUpViewPager() {
        var adapter = ViewPagerAdapter(
            childFragmentManager,
            arrayListOf(
                ProfileDetailsFragment(),
                ProfileAccountFragment()
            )
            ,
            arrayListOf(
                "DETAILS",
                "ACCOUNT"
            ),
            true
        )

        profile_viewpager.adapter = adapter
        profile_tabs.setupWithViewPager(profile_viewpager)

    }


    override fun onLoginSuccess(dismissRegister: Boolean) {
        if (dismissRegister)
        childFragmentManager.popBackStackImmediate()
        setViews()
    }

}



