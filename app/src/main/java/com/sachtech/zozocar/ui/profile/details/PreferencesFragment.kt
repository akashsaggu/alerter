package com.sachtech.zozocar.ui.profile.details

import android.os.Bundle
import android.view.View
import com.sachtech.zozocar.R
import gurtek.mrgurtekbase.KotlinBaseFragment
import kotlinx.android.synthetic.main.fragment_preferences.*

class PreferencesFragment:KotlinBaseFragment(R.layout.fragment_preferences) {


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        editPrefsToolbar.setNavigationOnClickListener { onBackPressed() }
    }
}