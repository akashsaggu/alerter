package com.sachtech.zozocar.ui.inbox.message

import com.sachtech.zozocar.R
import com.sachtech.zozocar.common.helper.RideBooking
import gurtek.mrgurtekbase.adapter.BaseAdapter
import kotlinx.android.synthetic.main.item_message.view.*

class MessageAdapter(
    val bookingList: ArrayList<RideBooking>,
    val onMessageItemSelected: OnMessageItemSelected
) :
    BaseAdapter<String>(R.layout.item_message) {
    override fun onBindViewHolder(holder: IViewHolder, position: Int) {
        val item = holder.itemView
        item.messageUsername.text = bookingList[position].rider
        item.sourceLocation.text = bookingList[position].pickOff
        item.destination.text = bookingList[position].dropOff
        item.bookingTime.text = bookingList[position].bookingTime
        item.rideTime.text = bookingList[position].rideDateAndTime

        item.setOnClickListener {
            onMessageItemSelected.bookingSelected(bookingList[position])
        }
    }

    override fun getItemCount(): Int {
        return bookingList.size
    }
}

interface OnMessageItemSelected {
    fun bookingSelected(rideBooking: RideBooking)
}

