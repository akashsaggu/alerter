package com.sachtech.zozocar.app

import android.app.Application
import com.facebook.FacebookSdk
import com.google.firebase.FirebaseApp
import com.sachtech.zozocar.di.common
import com.sachtech.zozocar.di.viewModels
import com.sachtech.zozocar.util.preference.SharedPreferenceUtils
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin


/**
 * Created by Akash Saggu(R4X)
 */

class App : Application() {

    val preference by lazy { SharedPreferenceUtils(this) }

    override fun onCreate() {
        super.onCreate()
        application = this
        FacebookSdk.sdkInitialize(applicationContext)
        FirebaseApp.initializeApp(getApp())
        startKoin {
            androidContext(this@App)
            modules(listOf(viewModels, common))
        }
        /* delay(1000) {

         FirebaseInstanceId.getInstance().instanceId.addOnCompleteListener {
             if (it.isSuccessful && it.result != null) {
                 getPref().set(PrefKey.FCMTOKEN, it.result?.token!!)
             }
         }
     }*/


    }

    companion object {
        lateinit var application: App
        @JvmStatic
        fun getApp() = application
    }

}

fun Any.getPref(): SharedPreferenceUtils {
    return App.getApp().preference
}