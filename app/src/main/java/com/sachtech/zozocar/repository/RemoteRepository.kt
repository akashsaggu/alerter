package com.sachtech.zozocar.repository

import com.sachtech.zozocar.networking.ApiInterface
import com.sachtech.zozocar.networking.apiHitter
import com.sachtech.zozocar.networking.model.*
import com.sachtech.zozocar.networking.postModel.UserPostModel
import com.sachtech.zozocar.util.rxkotlin.onNetwork
import io.reactivex.Single

class RemoteRepository : ApiInterface {
    override fun registerUser(userPostModel: UserPostModel): Single<UserModel> {
        return apiHitter().registerUser(userPostModel).onNetwork()
    }

    override fun loginUser(userEmail: String, userPassword: String): Single<UserModel> {
        return apiHitter().loginUser(userEmail, userPassword).onNetwork()
    }

    override fun addUserPhone(userId: String, userPhone: String): Single<UserModel> {
        return apiHitter().addUserPhone(userId, userPhone).onNetwork()
    }

    override fun getCarBodyTypes(): Single<CarBodyType> {
        return apiHitter().getCarBodyTypes().onNetwork()
    }

    override fun getCarColors(): Single<CarColors> {
        return apiHitter().getCarColors().onNetwork()
    }

    override fun getCarBrands(): Single<CarMakerBrand> {
        return apiHitter().getCarBrands().onNetwork()
    }

    override fun getCarBrandModel(carBrand: String): Single<CarBrandModels> {
        return apiHitter().getCarBrandModel(carBrand).onNetwork()
    }
}
