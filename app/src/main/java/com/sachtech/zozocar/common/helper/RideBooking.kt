package com.sachtech.zozocar.common.helper

data class RideBooking(
    var pickOff:String?=null,
    var dropOff:String?=null,
    var rideDateAndTime:String?=null,
    var bookingTime:String?=null,
    var rider:String?=null
)