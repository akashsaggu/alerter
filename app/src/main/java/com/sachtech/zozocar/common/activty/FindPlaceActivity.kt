package com.sachtech.zozocar.common.activty

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.core.content.ContextCompat
import com.google.android.libraries.places.api.Places
import com.google.android.libraries.places.api.model.AutocompletePrediction
import com.sachtech.zozocar.R
import com.sachtech.zozocar.app.App
import com.sachtech.zozocar.constants.IntentKey
import com.sachtech.zozocar.ui.findride.FindRideFragment
import com.sachtech.zozocar.ui.offerRide.adapter.HistoryPlaceAdapter
import com.sachtech.zozocar.util.extension.delay
import com.sachtech.zozocar.util.extension.getAddress
import com.sachtech.zozocar.util.extension.gone
import com.sachtech.zozocar.util.extension.observeTextChange
import com.sachtech.zozocar.util.recyclerview.ItemClickListener
import com.sachtech.zozocar.widget.ClearEditText.OnTextClearedListener
import gurtek.mrgurtekbase.KotlinBaseActivity
import kotlinx.android.synthetic.main.activity_find_place.*

class FindPlaceActivity : KotlinBaseActivity(), ItemClickListener<AutocompletePrediction> {


    companion object {
        const val FINDRIDEREQUEST = 789
    }

    private val placeAdapter by lazy {
        if (!Places.isInitialized()) {
            Places.initialize(App.getApp(), "AIzaSyDLDmvcZpQKHnbkEg76bk9jMd4QB_kZw4c")
        }
        HistoryPlaceAdapter(Places.createClient(this), this)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_find_place)
        setBottomUpAnimation(R.transition.frombottom, R.transition.fromtop)
//        delay(250) {
//            openKeyboard()
//            edtPlace.requestFocus()
//        }
        edtPlace.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                edtPlace.setmLeftIconDrawable(
                    ContextCompat.getDrawable(
                        this,
                        R.drawable.ic_left_arrow
                    )
                )
                delay(500) {
                    edtPlace.requestFocus()
                }
            }
        }

        placesRecycler.adapter = placeAdapter
        edtPlace.observeTextChange {
            placeAdapter.filter.filter(it)
        }
        edtPlace.setOnTextClearedListener(object : OnTextClearedListener {
            override fun onTextCleared() {

            }

            override fun onLeftIconClick() {
                finish()
            }
        })
    }

    override fun onItemClick(item: AutocompletePrediction) {
        forceHideKeyboard()
        edtPlace.clearFocus()
        edtPlace.setmLeftIconDrawable(
            ContextCompat.getDrawable(
                this,
                R.drawable.ic_search_small
            )
        )
        placesRecycler.gone()
        delay(250) {
            setResult(
                Activity.RESULT_OK,
                Intent().apply {
                    putExtra(
                        IntentKey.LOCATIONTYPE,
                        intent.getIntExtra(IntentKey.LOCATIONTYPE, FindRideFragment.LocationType.GOINGTO)
                    )
                    putExtra(
                        IntentKey.ADDRESS,
                        item.getAddress()
                    )
                })

            finish()
        }
    }
}
