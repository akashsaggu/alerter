package com.sachtech.zozocar.common.helper

import com.sachtech.zozocar.networking.model.CarBodyType

data class Car(
    var carColor: String? = null,
    var carMaker: String? = null,
    var carModel: String? = null,
    var carNumber: String? = null,
    var carType: CarBodyType.Data? = null,
    var carRegisteredYear: String? = null
)

object CarPreparer {
    private var car: Car = Car()

    fun setCarColor(color: String) {
        car.carColor = color
    }

    fun setCarMaker(maker: String) {
        car.carMaker = maker
    }

    fun setCarModel(model: String) {
        car.carModel = model
    }

    fun setCarNumberPlate(number: String) {
        car.carNumber = number
    }

    fun setCarType(type: CarBodyType.Data) {
        car.carType = type
    }

    fun setCarRegisteredYear(year: String) {
        car.carRegisteredYear = year
    }


    fun getCar(): Car {
        return car
    }

    fun clear() {
        car = Car()
    }
}