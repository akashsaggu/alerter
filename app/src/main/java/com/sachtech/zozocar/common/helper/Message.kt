package com.sachtech.zozocar.common.helper

data class Message(
    var message: String?=null,
    var isBelongToUser:Boolean=false,
    var messageTime:String?=null,
    var userName:String?=null
)