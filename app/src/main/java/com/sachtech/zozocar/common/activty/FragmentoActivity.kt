package com.sachtech.zozocar.common.activty

import android.os.Bundle
import com.sachtech.zozocar.R
import com.sachtech.zozocar.constants.FlowType
import com.sachtech.zozocar.constants.FragmentoType
import com.sachtech.zozocar.constants.IntentKey
import com.sachtech.zozocar.ui.findride.ridedetail.SearchRideDetailFragment
import com.sachtech.zozocar.ui.findride.searchresult.RideSearchResultFragment
import com.sachtech.zozocar.ui.offerRide.date.DateSelectorFragment
import com.sachtech.zozocar.ui.offerRide.success.OfferAddedFragment
import com.sachtech.zozocar.ui.rides.current.detail.RidePlanFragment
import gurtek.mrgurtekbase.KotlinBaseActivity

class FragmentoActivity : KotlinBaseActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setBottomUpAnimation(R.transition.frombottom, R.transition.fromtop)
        intent?.let {
            when (it.getIntExtra(IntentKey.FRAGMENTOTYPE, FragmentoType.DATETIMEPICKER)) {
                FragmentoType.DATETIMEPICKER -> {
                    addFragment(
                        DateSelectorFragment::class,
                        animation = false,
                        extras = Bundle().apply { putInt(IntentKey.FLOWTYPE, FlowType.PICKER) })
                }

                FragmentoType.SEARCHRESULT -> {
                    addFragment(RideSearchResultFragment::class, animation = false)
                }

                FragmentoType.RIDEPLAN -> {
                    addFragment(RidePlanFragment::class, animation = false)
                }
                FragmentoType.SEARCHDETAIL -> {
                    addFragment(SearchRideDetailFragment::class)
                }
                FragmentoType.RideOfferSuccess -> {
                    addFragment(OfferAddedFragment::class)
                }
            }

        }
    }

    override fun containsOnlyFragment(): Boolean {
        return true
    }

}
