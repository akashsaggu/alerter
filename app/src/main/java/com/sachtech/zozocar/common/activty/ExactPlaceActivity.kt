package com.sachtech.zozocar.common.activty

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.animation.AccelerateDecelerateInterpolator
import androidx.core.content.ContextCompat
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MapStyleOptions
import com.google.android.libraries.places.api.Places
import com.google.android.libraries.places.api.model.AutocompletePrediction
import com.sachtech.zozocar.R
import com.sachtech.zozocar.app.App
import com.sachtech.zozocar.common.activty.dialog.SpecificLocationDialog
import com.sachtech.zozocar.constants.IntentKey
import com.sachtech.zozocar.ui.offerRide.adapter.HistoryPlaceAdapter
import com.sachtech.zozocar.util.extension.*
import com.sachtech.zozocar.util.recyclerview.ItemClickListener
import com.sachtech.zozocar.widget.ClearEditText.OnTextClearedListener
import gurtek.mrgurtekbase.KotlinBaseActivity
import kotlinx.android.synthetic.main.activity_exact_place.*


class ExactPlaceActivity : KotlinBaseActivity(), OnMapReadyCallback,
    ItemClickListener<AutocompletePrediction> {


    companion object {
        const val EXACTADDRESSREQUEST = 456
    }

    override fun onItemClick(item: AutocompletePrediction) {

        setResult(Activity.RESULT_OK, Intent().apply {
            putExtra(IntentKey.ADDRESS, item.getAddress())
        })
        finish()
    }

    private var mGoogleMap: GoogleMap? = null
    private val placeAdapter by lazy {
        if (!Places.isInitialized()) {
            Places.initialize(App.getApp(), "AIzaSyDLDmvcZpQKHnbkEg76bk9jMd4QB_kZw4c")
        }
        HistoryPlaceAdapter(Places.createClient(this), this)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_exact_place)
        edtPickUp.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                startAnimation()
                edtPickUp.setmLeftIconDrawable(
                    ContextCompat.getDrawable(
                        this,
                        R.drawable.ic_left_arrow
                    )
                )
                delay(500) {
                    edtPickUp.requestFocus()
                }
            }
        }
        btnBack.setOnClickListener {
            finish()
        }
        containerInfo.setOnClickListener {
            showDialogFragment<SpecificLocationDialog> { }
        }
        edtPickUp.setOnTextClearedListener(object : OnTextClearedListener {
            override fun onTextCleared() {
            }

            override fun onLeftIconClick() {
                forceHideKeyboard()
                reverseAnimation()
                edtPickUp.setmLeftIconDrawable(
                    ContextCompat.getDrawable(
                        applicationContext,
                        R.drawable.ic_search_small
                    )
                )
            }

        })
        placesRecycler.adapter = placeAdapter
        edtPickUp.observeTextChange {
            placeAdapter.filter.filter(it)
        }

        val googleMapFragment =
            supportFragmentManager.findFragmentById(R.id.map) as SupportMapFragment
        googleMapFragment.getMapAsync(this)
    }


    private fun startAnimation() {
        btnBack.animate().translationY(-(btnBack.top + btnBack.height).toFloat())
            .setDuration(300).setInterpolator(AccelerateDecelerateInterpolator())
            .alpha(0.5f).start()
        txtTitle.animate().translationY(-(txtTitle.top + txtTitle.height).toFloat())
            .setDuration(500).setInterpolator(AccelerateDecelerateInterpolator())
            .alpha(0.5f).start()
        edtPickUp.animate().translationY(-(edtPickUp.top - dpToPx(20).toFloat()))
            .setDuration(500).setInterpolator(AccelerateDecelerateInterpolator()).start()
        mapContainer.animate().translationY(mapContainer.height.toFloat()).setDuration(700)
            .setInterpolator(AccelerateDecelerateInterpolator()).start()
        containerInfo.animate().translationY(-(containerInfo.top).toFloat()).alpha(0.0f)
            .setDuration(500).setInterpolator(AccelerateDecelerateInterpolator()).start()
        delay(250) {
            placesRecycler.visible()
        }
        placesRecycler.animate()
            .translationY(-(edtPickUp.top).toFloat())
            .alpha(1.0f).setDuration(500)
            .setInterpolator(AccelerateDecelerateInterpolator()).start()
    }

    private fun reverseAnimation() {
        btnBack.animate().translationY(0f)
            .setDuration(555).setInterpolator(AccelerateDecelerateInterpolator())
            .alpha(1f).start()
        containerInfo.animate().translationY(0f).alpha(1.0f)
            .setDuration(500).setInterpolator(AccelerateDecelerateInterpolator()).start()
        txtTitle.animate().translationY(0f).setDuration(500)
            .setInterpolator(AccelerateDecelerateInterpolator()).alpha(1f).start()
        edtPickUp.animate().translationY(0f).setDuration(500)
            .setInterpolator(AccelerateDecelerateInterpolator()).start()
        mapContainer.animate().translationY(0f).setDuration(700)
            .setInterpolator(AccelerateDecelerateInterpolator()).start()
        placesRecycler.animate().translationY(0f).alpha(0.0f).setDuration(500)
            .setInterpolator(AccelerateDecelerateInterpolator()).start()
        delay(400) {
            placesRecycler.gone()
        }
    }

    private fun fullMapAnimation() {
        btnBack.animate().translationY(-(btnBack.top + btnBack.height).toFloat())
            .setDuration(300).setInterpolator(AccelerateDecelerateInterpolator())
            .alpha(0.5f).start()
        txtTitle.animate().translationY(-(txtTitle.top + txtTitle.height).toFloat())
            .setDuration(500).setInterpolator(AccelerateDecelerateInterpolator())
            .alpha(0.5f).start()
        edtPickUp.animate().translationY(-(edtPickUp.top - dpToPx(20).toFloat()))
            .setDuration(500).setInterpolator(AccelerateDecelerateInterpolator()).start()

        mapContainer.animate().translationY(-(edtPickUp.top - dpToPx(16)).toFloat())
            .setDuration(700)
            .setInterpolator(AccelerateDecelerateInterpolator()).start()
    }

    override fun onMapReady(googleMap: GoogleMap?) {
        mGoogleMap = googleMap
        mGoogleMap?.setMapStyle(
            MapStyleOptions.loadRawResourceStyle(
                this, R.raw.mapstyle
            )
        )
        mGoogleMap?.animateCamera(CameraUpdateFactory.newLatLngZoom(LatLng(30.7046, 76.7179), 11f))
        mGoogleMap?.setOnMapClickListener {
            //fullMapAnimation()
        }
    }


}
