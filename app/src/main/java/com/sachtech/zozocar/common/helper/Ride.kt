package com.sachtech.zozocar.common.helper

import android.location.Address
import com.sachtech.zozocar.util.extension.isNull
import java.util.*


object RidePreparer {
    private var ride: Ride =
        Ride()
    private var returnRide: Ride? = null

    fun addPickUp(address: Address) {
        ride.pickup = address
    }

    fun addDropOff(address: Address) {
        ride.dropOff = address
    }

    fun addStopOver(address: Address) {
        ride.stopOvers.add(
            address
        )
    }

    fun addStopOvers(address: List<Address>) {
        ride.stopOvers.addAll(
            address
        )
    }

    fun getRide(): Ride {
        return ride
    }

    fun getReturnRide(): Ride {
        return returnRide!!
    }

    fun setEmptyMiddleSeat(bool: Boolean) {
        ride.rideMiddleSeatEmpty = bool
    }

    fun setTotalAvailableSeats(noPassengers: Int) {
        ride.rideTotalPassengers = noPassengers
    }

    fun setCanBookInstantly(bool: Boolean) {
        ride.rideBookInstantly = bool
    }

    fun setPerSeatPrice(price: Float) {
        ride.ridePerSeatPrice = price
    }

    fun setRideAbout(rideAbout: String) {
        ride.rideAbout = rideAbout
    }

    fun setRideDate(date: String) {
        ride.rideDate = date
    }

    fun setRideTime(time: String, timeStamp: Long) {
        ride.rideTime = time
        ride.timeStamp = timeStamp
    }

    fun setHasReturn(bool: Boolean) {
        when (bool) {
            true -> {
                returnRide =
                    Ride()
                with(
                    returnRide!!,
                    ride
                ) {
                    pickup = it.dropOff

                    dropOff = it.pickup
                    val stopOver = arrayListOf<Address>()
                    Collections.copy(it.stopOvers, stopOver)
                    stopOvers = stopOver.apply { reverse() }
                    rideMiddleSeatEmpty = it.rideMiddleSeatEmpty
                    rideTotalPassengers = it.rideTotalPassengers
                    rideBookInstantly = it.rideBookInstantly
                    ridePerSeatPrice = it.ridePerSeatPrice
                    rideAbout = it.rideAbout
                }
            }
            false -> {
                returnRide = null
            }
        }
    }

    fun setReturnDate(date: String) {
        returnRide?.rideDate = date
    }

    fun setReturnTime(time: String, timeStamp: Long) {
        returnRide?.rideTime = time
        returnRide?.timeStamp = timeStamp
    }

    fun complete(): Rides {
        with(ride) {
            if (pickup.isNull())
                return Rides(false, "PickUp address required")

            if (dropOff.isNull())
                return Rides(false, "DropOff address required")

            if (rideDate.isBlank())
                return Rides(false, "Date required")

            if (rideTime.isBlank())
                return Rides(false, "Time required")

            if (ridePerSeatPrice == 0.0f)
                return Rides(false, "Price required")
        }
        returnRide?.let {
            if (ride.timeStamp > it.timeStamp) {
                return Rides(
                    false,
                    "Return date should be greater than ride date."
                )
            }
        }
        return Rides(
            true,
            "Ride Created Successfully",
            rides = arrayListOf<Ride>().apply {
                add(ride)
                returnRide?.let {
                    add(it)
                }
            })
    }


    fun reset() {
        ride =
            Ride()
        returnRide = null
    }
}

inline fun <T, K, R> with(receiver: T, another: K, block: T.(K) -> R): R {
    return receiver.block(another)
}

data class Ride(
    var pickup: Address? = null,
    var dropOff: Address? = null,
    var stopOvers: ArrayList<Address> = arrayListOf(),
    var rideDate: String = "",
    var rideTime: String = "",
    var rideMiddleSeatEmpty: Boolean = true,
    var rideTotalPassengers: Int = 3,
    var timeStamp: Long = 0,
    var rideBookInstantly: Boolean = true,
    var ridePerSeatPrice: Float = 0.0f,
    var rideAbout: String = ""
)


data class Rides(
    val success: Boolean,
    val message: String,
    val rides: ArrayList<Ride> = arrayListOf()
)