package com.sachtech.zozocar.common.adapter


import android.location.Location
import android.widget.Filter
import android.widget.Filterable
import androidx.annotation.LayoutRes
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.tasks.Tasks
import com.google.android.libraries.places.api.model.*
import com.google.android.libraries.places.api.net.FindAutocompletePredictionsRequest
import com.google.android.libraries.places.api.net.PlacesClient
import gurtek.mrgurtekbase.adapter.ItemsBaseAdapter
import java.util.*
import java.util.concurrent.ExecutionException
import java.util.concurrent.TimeUnit
import java.util.concurrent.TimeoutException
import com.google.android.gms.maps.model.LatLngBounds



abstract class PlacesAdapter(private val placesClient: PlacesClient, @LayoutRes private val itemLayout: Int) :
    ItemsBaseAdapter<AutocompletePrediction>(
        itemLayout
    ), Filterable {

    abstract override fun onBindViewHolder(holder: IViewHolder, position: Int)


    override fun getFilter(): Filter {
        return object : Filter() {
            override fun performFiltering(charSequence: CharSequence?): Filter.FilterResults {

                val results = Filter.FilterResults()

                // We need a separate list to store the results, since
                // this is run asynchronously.
                var filterData: List<AutocompletePrediction>? = ArrayList()

                // Skip the autocomplete query if no constraints are given.
                if (charSequence != null) {
                    // Query the autocomplete API for the (constraint) search string.
                    filterData = getAutocomplete(charSequence)
                }

                results.values = filterData
                if (filterData != null) {
                    results.count = filterData.size
                } else {
                    results.count = 0
                }

                return results
            }

            override fun publishResults(
                charSequence: CharSequence?,
                results: Filter.FilterResults?
            ) {

                charSequence?.let {
                    if (results != null && results.count > 0) {
                        // The API returned at least one result, update the data.
                        addNewList(results.values as List<AutocompletePrediction>)

                    } else {
                        // The API did not return any results, invalidate the data set.
                        removeAll()
                    }
                }
            }

            override fun convertResultToString(resultValue: Any): CharSequence {
                // Override this method to display a readable result in the AutocompleteTextView
                // when clicked.
                return (resultValue as? AutocompletePrediction)?.getFullText(null)
                    ?: super.convertResultToString(resultValue)
            }
        }
    }

    private fun getAutocomplete(constraint: CharSequence): List<AutocompletePrediction> {

        //Create a RectangularBounds object.
        val bounds = RectangularBounds.newInstance(
            LatLng(-33.880490, 151.184363),
            LatLng(-33.858754, 151.229596)
        )


        val requestBuilder = FindAutocompletePredictionsRequest.builder()
            .setQuery(constraint.toString())
            .setCountry("IN") //Use only in specific country
            // Call either setLocationBias() OR setLocationRestriction().
            .setLocationBias(bounds)
            //                        .setLocationRestriction(bounds)
            .setSessionToken(AutocompleteSessionToken.newInstance())
            .setTypeFilter(TypeFilter.REGIONS)

        val results = placesClient.findAutocompletePredictions(requestBuilder.build())


        //Wait to get results.
        try {
            Tasks.await(results, 60, TimeUnit.SECONDS)
        } catch (e: ExecutionException) {
            e.printStackTrace()
        } catch (e: InterruptedException) {
            e.printStackTrace()
        } catch (e: TimeoutException) {
            e.printStackTrace()
        }

        return (if (results.isSuccessful) {
            if (results.result != null) {
                results.result!!.autocompletePredictions
            } else null
        } else {
            null
        })!!
    }
}