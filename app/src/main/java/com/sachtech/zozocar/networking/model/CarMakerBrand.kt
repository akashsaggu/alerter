package com.sachtech.zozocar.networking.model


import com.google.gson.annotations.SerializedName

data class CarMakerBrand(
    @SerializedName("data")
    val `data`: List<Data>
) : CommonResponse() {
    data class Data(
        @SerializedName("car_brand")
        val carBrand: String
    )
}