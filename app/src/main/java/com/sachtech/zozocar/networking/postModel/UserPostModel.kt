package com.sachtech.zozocar.networking.postModel

import com.google.gson.annotations.SerializedName

data class UserPostModel(
    @SerializedName("provider") var provider: String = ProviderType.EMAIL,
    @SerializedName("gender") var gender: String = "",
    @SerializedName("firstname") var firstName: String = "",
    @SerializedName("lastname") var lastName: String = "",
    @SerializedName("birthyear") var birthYear: String = "",
    @SerializedName("email") var email: String = "",
    @SerializedName("password") var password: String = "",
    @SerializedName("fcm_token") var fcmToken: String = "",
    @SerializedName("photourl") var photoUrl: String = "",
    @SerializedName("fb_id") var facebookToken: String = ""
)


object ProviderType {
    const val EMAIL = "email"
    const val FACEBOOK = "facebook"
}