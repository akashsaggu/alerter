package com.sachtech.zozocar.networking.model


import com.google.gson.annotations.SerializedName

data class CarBrandModels(
    @SerializedName("data")
    val `data`: List<Data>
) : CommonResponse() {
    data class Data(
        @SerializedName("car_added_on")
        val carAddedOn: String,
        @SerializedName("car_brand")
        val carBrand: String,
        @SerializedName("car_id")
        val carId: String,
        @SerializedName("car_model")
        val carModel: String,
        @SerializedName("car_status")
        val carStatus: String,
        @SerializedName("car_updated_on")
        val carUpdatedOn: String
    )
}