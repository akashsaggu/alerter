package com.sachtech.zozocar.networking.model


import com.google.gson.annotations.SerializedName

data class CarColors(
    @SerializedName("data")
    val `data`: List<Data>
) : CommonResponse() {
    data class Data(
        @SerializedName("color_added_on")
        val colorAddedOn: String,
        @SerializedName("color_code")
        val colorCode: String,
        @SerializedName("color_id")
        val colorId: String,
        @SerializedName("color_name")
        val colorName: String
    )
}