package com.sachtech.zozocar.networking

import io.reactivex.Observer
import io.reactivex.SingleObserver
import io.reactivex.disposables.Disposable
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.HttpException
import java.io.IOException
import java.net.SocketTimeoutException


class SingleResponseConsumer<T>(
    val onRequestSuccess: (T) -> Unit,
    val onRequestError: (String) -> Unit
) :
    SingleObserver<T> {

    override fun onSuccess(t: T) {
        onRequestSuccess(t)
    }

    override fun onSubscribe(d: Disposable) {}

    override fun onError(e: Throwable) {
        when (e) {
            is HttpException -> {
                val responseBody = e.response()?.errorBody()
                onRequestError(getErrorMessage(responseBody!!))
            }
            is SocketTimeoutException -> onRequestError("Request Timeout")
            is IOException -> onRequestError("Network Error, Please check your internet connection.")
            else -> onRequestError(e.localizedMessage ?: "Unknown exception")
        }
    }

    private fun getErrorMessage(responseBody: ResponseBody): String {
        return try {
            val jsonObject = JSONObject(responseBody.string())
            jsonObject.getString("Message")
        } catch (e: Exception) {
            e.message!!
        }

    }

}

class ResponseConsumer<T>(
    private val onRequestSuccess: (T) -> Unit,
    private val onRequestError: (String) -> Unit,
    private val onSubscribe: (() -> Unit)? = null,
    private val onComplete: (() -> Unit)? = null
) : Observer<T> {
    override fun onComplete() {
        onComplete?.invoke()
    }

    override fun onSubscribe(d: Disposable) {
        onSubscribe?.invoke()
    }

    override fun onNext(t: T) {
        onRequestSuccess(t)
    }

    override fun onError(e: Throwable) {
        when (e) {
            is HttpException -> {
                val responseBody = e.response()?.errorBody()
                onRequestError(getErrorMessage(responseBody!!))
            }
            is SocketTimeoutException -> onRequestError("Request Timeout")
            is IOException -> onRequestError("Network Error, Please check your internet connection.")
            else -> onRequestError(e.localizedMessage ?: "Unknown exception")
        }

    }

    private fun getErrorMessage(responseBody: ResponseBody): String {
        return try {
            val jsonObject = JSONObject(responseBody.string())
            jsonObject.getString("Message")
        } catch (e: Exception) {
            e.message!!
        }

    }

}