package com.sachtech.zozocar.networking.model


import com.google.gson.annotations.SerializedName

data class CarBodyType(
    @SerializedName("data")
    val `data`: List<Data>
) : CommonResponse() {
    data class Data(
        @SerializedName("type_added_on")
        val typeAddedOn: String,
        @SerializedName("type_id")
        val typeId: String,
        @SerializedName("type_name")
        val typeName: String,
        @SerializedName("type_status")
        val typeStatus: String,
        @SerializedName("type_updated_on")
        val typeUpdatedOn: String
    )
}