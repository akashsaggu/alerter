package com.sachtech.zozocar.networking.model


import com.google.gson.annotations.SerializedName

data class UserModel(
    @SerializedName("data")
    val `data`: Data
) : CommonResponse() {
    data class Data(
        @SerializedName("u_added_on")
        val uAddedOn: String,
        @SerializedName("u_bio")
        val uBio: String,
        @SerializedName("u_birth_year")
        val uBirthYear: String,
        @SerializedName("u_doc_status")
        val uDocStatus: String,
        @SerializedName("u_docs")
        val uDocs: List<UDoc>,
        @SerializedName("u_email")
        val uEmail: String,
        @SerializedName("u_email_verfied")
        val uEmailVerfied: String,
        @SerializedName("u_fcm_token")
        val uFcmToken: String,
        @SerializedName("u_first_name")
        val uFirstName: String,
        @SerializedName("u_gender")
        val uGender: String,
        @SerializedName("u_id")
        val uId: String,
        @SerializedName("u_is_doc_uploaded")
        val uIsDocUploaded: String,
        @SerializedName("u_last_name")
        val uLastName: String,
        @SerializedName("u_login_source")
        val uLoginSource: String,
        @SerializedName("u_password")
        val uPassword: String,
        @SerializedName("u_phone")
        val uPhone: String,
        @SerializedName("u_phone_verified")
        val uPhoneVerified: String,
        @SerializedName("u_profile_pic")
        val uProfilePic: String,
        @SerializedName("u_status")
        val uStatus: String,
        @SerializedName("u_updated_on")
        val uUpdatedOn: String
    ) {
        data class UDoc(
            @SerializedName("doc_added_on")
            val docAddedOn: String,
            @SerializedName("doc_file_back_link")
            val docFileBackLink: String,
            @SerializedName("doc_file_front_link")
            val docFileFrontLink: String,
            @SerializedName("doc_id")
            val docId: String,
            @SerializedName("doc_type")
            val docType: String,
            @SerializedName("doc_user_id")
            val docUserId: String
        )
    }
}


object UserData {
    var userData: UserModel.Data? = null
}




