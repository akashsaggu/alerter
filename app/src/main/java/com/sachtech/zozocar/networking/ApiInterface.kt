package com.sachtech.zozocar.networking

import com.sachtech.zozocar.networking.model.*
import com.sachtech.zozocar.networking.postModel.UserPostModel
import io.reactivex.Single
import retrofit2.http.Body
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST


interface ApiInterface {

    companion object {
        const val KEY = "zozo@123*"
    }


    @POST("users/registeruser")
    fun registerUser(@Body userPostModel: UserPostModel): Single<UserModel>

    @FormUrlEncoded
    @POST("users/login")
    fun loginUser(@Field("user_email") userEmail: String, @Field("user_password") userPassword: String): Single<UserModel>

    @FormUrlEncoded
    @POST("users/addphone")
    fun addUserPhone(@Field("user_id") userId: String, @Field("user_phone") userPhone: String): Single<UserModel>


    @POST("car/carbody")
    fun getCarBodyTypes(): Single<CarBodyType>


    @POST("car/carcolor")
    fun getCarColors(): Single<CarColors>


    @POST("car/getallbrands")
    fun getCarBrands(): Single<CarMakerBrand>

    @FormUrlEncoded
    @POST("car/getcarsofbrand")
    fun getCarBrandModel(@Field("car_brand") carBrand: String): Single<CarBrandModels>

}