package com.sachtech.zozocar.di

import com.sachtech.zozocar.base.BaseViewModel
import com.sachtech.zozocar.repository.RemoteRepository
import com.sachtech.zozocar.ui.profile.details.addMobile.vm.AddMobileVm
import com.sachtech.zozocar.ui.profile.details.car.vm.AddCarVm
import com.sachtech.zozocar.ui.registerLogin.vm.RegisterLoginVM
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val viewModels = module {
    viewModel { BaseViewModel() }
    viewModel { RegisterLoginVM() }
    viewModel { AddMobileVm() }
    viewModel { AddCarVm() }
}

val common = module {
    single { RemoteRepository() }
}