package gurtek.mrgurtekbase

import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.transition.TransitionInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import androidx.annotation.*
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import gurtek.mrgurtekbase.listeners.KotlinBaseListener
import gurtek.mrgurtekbase.navigator.Navigator
import gurtek.mrgurtekbase.viewmodel.KotlinBaseViewModel
import gurtek.mrgurtekbase.viewmodel.common.ErrorType
import java.util.*
import kotlin.reflect.KClass


/**
 * * Created by Gurtek Singh on 1/1/2018.
 * Sachtech Solution
 * gurtek@protonmail.ch
 */
@Keep
abstract class KotlinBaseActivity(@IdRes private val container: Int = android.R.id.content) :
    AppCompatActivity(), KotlinBaseListener {

    private val progress: Dialog by lazy {
        Dialog(this).apply {
            setContentView(R.layout.progress_layout)
            setCancelable(false)
            window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        }
    }

    private var baseViewModel: KotlinBaseViewModel? = null

    fun setViewModel(vm: KotlinBaseViewModel) {
        baseViewModel = vm
        lifecycle.addObserver(baseViewModel!!)
        observeProgress()
        listenForError()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initBackStackListener()
    }


    // Fragment
    private fun initBackStackListener() {

        with(supportFragmentManager) {
            addOnBackStackChangedListener {
                val fragment = findFragmentById(container)
                navigator.lastFragmentChanged(fragment = fragment as KotlinBaseFragment)
            }
        }
    }

    fun setBottomUpAnimation(@TransitionRes enterAnim: Int, @TransitionRes exitAnim: Int) {
        (window.decorView as ViewGroup).isTransitionGroup = false
        val enter = TransitionInflater.from(this).inflateTransition(enterAnim)
        val exit = TransitionInflater.from(this).inflateTransition(exitAnim)
        exit.excludeTarget(android.R.id.statusBarBackground, true)
        exit.excludeTarget(android.R.id.navigationBarBackground, true)
        window.enterTransition = enter
        window.exitTransition = exit
        window.returnTransition = exit
    }

    private val manager: FragmentManager by lazy {
        supportFragmentManager
    }

    private val navigator: Navigator by lazy { Navigator(this, container) }


    //Default Animation slideInLeft and slideOutRight
    fun setCustomAnimation(@AnimatorRes @AnimRes enterAnim: Int, @AnimatorRes @AnimRes exitAnim: Int) {
        navigator.setCustomeAnimation(enterAnim, exitAnim)
    }

    override fun getFragment(kClass: KClass<out Fragment>): Fragment? {
        return supportFragmentManager.findFragmentByTag(kClass.java.simpleName)
    }


    override fun openAForResult(kClass: KClass<out AppCompatActivity>, bundle: Bundle, code: Int) {
        navigator.openAForResult(kClass, bundle, code)
    }


    override fun navigateToFragment(
        java: KClass<out Fragment>,
        extras: Bundle?,
        animation: Boolean
    ) {
        navigateToFragment(
            java.java,
            extras,
            transitionView = null,
            userTag = "",
            animation = animation
        )
    }

    override fun navigateToFragment(
        fragment: KClass<out Fragment>,
        extras: Bundle?,
        userTag: String,
        animation: Boolean
    ) {
        navigateToFragment(
            fragment.java,
            extras,
            transitionView = null,
            userTag = userTag,
            animation = animation
        )
    }

    override fun addFragment(
        fragment: KClass<out Fragment>,
        extras: Bundle?,
        tag: String,
        animation: Boolean
    ) {
        navigator.addFragment(fragment.java, tag = tag, bundle = extras, animation = animation)
    }

    override fun addFragment(fragment: Fragment, extras: Bundle?, tag: String, animation: Boolean) {
        navigator.addFragment(fragment, tag = tag, bundle = extras, animation = animation)

    }

    inline fun <reified T : Fragment> navigateToFragment(
        animation: Boolean = false,
        extras: Bundle.() -> Unit = {}
    ) {
        navigateToFragment(T::class.java, Bundle().apply { extras() }, animation = animation)
    }

    fun forceHideKeyboard() {
        val view: View? = findViewById(android.R.id.content)
        if (view != null) {
            val imm =
                getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(view.windowToken, 0)
        }
    }

    fun navigateToFragment(
        clazz: Class<out Fragment>,
        bundle: Bundle? = null,
        transitionView: View? = null,
        userTag: String = "",
        animation: Boolean = false
    ) {
        navigator.replaceFragment(clazz, bundle, transitionView, userTag, animation)
    }


    fun switchFragment(
        clazz: Class<out Fragment>,
        bundle: Bundle? = null,
        transitionView: View? = null
    ) {
        navigator.addFragment(clazz, bundle, transitionView)
    }


    override fun onBackPressed() {
        if (containsOnlyFragment()) {
            if (manager.backStackEntryCount == 1) {
                finish()
            } else {
                super.onBackPressed()
            }
        } else {
            super.onBackPressed()
        }
    }

    open fun containsOnlyFragment(): Boolean {
        return true
    }

    fun currentVisibleFragmentTag(): String? {
        return navigator.getCurrentFragmentTag()
    }

    inline fun <reified T : Fragment> getFragment(): T? {
        val fragment = supportFragmentManager.findFragmentByTag(T::class.java.simpleName)
        return if (fragment != null) fragment as T else null
    }


    override fun addChildFragment(
        childFragmentManager: FragmentManager,
        container: Int,
        kClass: KClass<out Fragment>,
        bundle: Bundle
        , animation: Boolean
    ) {
        navigator.addChildFragment(childFragmentManager, container, kClass, bundle, animation)
    }

    override fun replaceChildFragment(
        childFragmentManager: FragmentManager,
        container: Int,
        kClass: KClass<out Fragment>,
        bundle: Bundle
        , animation: Boolean
    ) {
        navigator.replaceChildFragment(childFragmentManager, container, kClass, bundle, animation)
    }

    fun bringToFront(kClass: KClass<out KotlinBaseFragment>) {
        navigator.bringFragmentToFrontIfPresentOrCreate(kClass.java)
    }

    fun getLastAddedFragment(): KotlinBaseFragment? {
        return navigator.getLastAddedFragment()
    }

    override fun openA(kClass: KClass<out AppCompatActivity>, extras: Bundle?) {
        navigator.openA(kClass, extras!!)
    }

    // Message
    fun showDialog(
        clazz: Class<out KotlinBaseDialogFragment>,
        bundle: Bundle? = Bundle()
    ): Fragment {
        val tag = clazz.simpleName
        val ft = supportFragmentManager.beginTransaction()
        var fragment = supportFragmentManager.findFragmentByTag(tag)
        if (fragment != null) {
            ft.remove(fragment)
        }
        ft.addToBackStack(tag)

        // Create and show the dialog.
        fragment = clazz.newInstance()
        fragment.arguments = bundle
        fragment.show(ft, tag)

        return fragment
    }

    override fun showProgress() {
        if (progress.isShowing)
            progress.hide()
        progress.show()
    }

    override fun hideProgress() {
        if (progress.isShowing)
            progress.hide()
    }

    private fun observeProgress() {
        baseViewModel?.progressObserver?.observe(this, androidx.lifecycle.Observer {
            when (it) {
                true -> showProgress()
                false -> hideProgress()
            }
        })
    }

    private fun listenForError() {
        baseViewModel?.errorObserver?.observe(this, androidx.lifecycle.Observer {
            when (it.errorType) {
                ErrorType.SNACKBAR -> snack(it.msg ?: "")
                ErrorType.DIALOG -> showAlert(it.msg)
            }
        })
    }


    // night
    fun isNight(): Boolean {
        var isNightModeEnable = false
        val c = Calendar.getInstance()
        val timeOfDay = c.get(Calendar.HOUR_OF_DAY)
        if (timeOfDay in 6..18) {
            isNightModeEnable = false
        } else if (timeOfDay in 0..5 || timeOfDay in 19..23) {
            isNightModeEnable = true
        }
        return isNightModeEnable
    }


}
