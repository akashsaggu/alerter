package gurtek.mrgurtekbase.adapter;

import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import java.util.List;

public abstract class BaseSpinnerAdapter<T> extends BaseAdapter {
    List<T> list;

    public void clearSpinner() {
        if (list != null)
            list.clear();
        notifyDataSetChanged();
    }


    public List<T> getList() {
        return list;
    }

    public void setList(List<T> campuses) {
        this.list = campuses;
        notifyDataSetChanged();
    }


    public T getListItem(int i) {
        return list.get(i);
    }

    @Override
    public int getCount() {
        if (list == null)
            return 0;
        else
            return list.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return setViewAndData(position);
    }

    public abstract View setViewAndData(int position);

    public void setTitle(T item) {
        list.add(0, item);
        notifyDataSetChanged();
    }

}
