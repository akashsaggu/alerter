package gurtek.mrgurtekbase.transformers;

import android.view.View;

import androidx.viewpager.widget.ViewPager;

/**
 * Created by Daichi Furiya on 2015/02/27.
 */

public class CardSlideTransformer implements ViewPager.PageTransformer {

    @Override
    public void transformPage(View page, float position) {
        if (position <= 0) {
            int pageWidth = page.getWidth();
            float translateValue = position * -pageWidth;
            if (translateValue > -pageWidth) {
                page.setTranslationX(translateValue);
            } else {
                page.setTranslationX(0);
            }
        }
    }
}