package gurtek.mrgurtekbase.viewmodel.common

enum class ErrorType {
    DIALOG,
    SNACKBAR
}

data class CommonError(val msg: String?, val errorType: ErrorType = ErrorType.SNACKBAR)