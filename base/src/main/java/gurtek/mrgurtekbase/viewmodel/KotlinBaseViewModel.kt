package gurtek.mrgurtekbase.viewmodel

import androidx.lifecycle.*
import gurtek.mrgurtekbase.viewmodel.common.CommonError
import gurtek.mrgurtekbase.viewmodel.common.ErrorType
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

open class KotlinBaseViewModel : ViewModel(), LifecycleObserver {
    lateinit var disposables: CompositeDisposable
    private val errorLiveData by lazy { MutableLiveData<CommonError>() }
    val errorObserver get() = errorLiveData
    private val progressLiveData by lazy { MutableLiveData<Boolean>() }
    val progressObserver get() = progressLiveData


    override fun onCleared() {
        disposables.clear()
        super.onCleared()
    }

    fun addToDisposable(disposable: Disposable?) {
        disposable?.let { disposables.add(it) }
    }

    fun showError(errorMSg: String?, errorType: ErrorType = ErrorType.SNACKBAR) {
        errorMSg?.let { errorLiveData.value = CommonError(it, errorType) }
    }

    fun showProgress() {
        progressLiveData.value = true
    }

    fun hideProgress() {
        progressLiveData.value = false
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_CREATE)
    fun onCreate() {
        disposables = CompositeDisposable()
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    fun onDestroy() {
        disposables.clear()
    }

}