package gurtek.mrgurtekbase

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.util.DisplayMetrics
import android.view.*
import androidx.annotation.LayoutRes
import androidx.fragment.app.DialogFragment


/**
 * * Created by Gurtek Singh on 1/1/2018.
 * Sachtech Solution
 * gurtek@protonmail.ch
 */
abstract class KotlinBaseDialogFragment(@LayoutRes val view: Int) : DialogFragment() {


    private lateinit var contentView: View
    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = super.onCreateDialog(savedInstanceState)
        dialog.window?.requestFeature(Window.FEATURE_NO_TITLE)
        dialog.window?.setBackgroundDrawableResource(android.R.color.transparent)
        return dialog
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        contentView = inflater.inflate(view, container, false)
        return contentView
    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        if (adjustDisplay()) setDisplay()
    }

    abstract fun adjustDisplay(): Boolean

    open fun setTitle(titleTxt: String) {

    }

    private fun setDisplay() {

        val display = activity?.windowManager?.defaultDisplay
        val outMetrics = DisplayMetrics()
        display?.getMetrics(outMetrics)
        val attrs = dialog?.window?.attributes
        attrs?.width = (375 * outMetrics.density).toInt()

    }


    fun getScreenHeightWidth(): Pair<Int, Int> {
        val displayMetrics = context!!.resources.displayMetrics
        val dpHeight =
            displayMetrics.heightPixels//(displayMetrics.heightPixels / displayMetrics.density).toInt()
        val dpWidth =
            displayMetrics.widthPixels//(displayMetrics.widthPixels / displayMetrics.density).toInt()
        /*val dip = 14f
        val r = resources
        val px = TypedValue.applyDimension(
            TypedValue.COMPLEX_UNIT_DIP,
            dip,
            r.displayMetrics
        )*/
        return dpWidth to dpHeight
    }

    fun getDeviceMetrics(): DisplayMetrics {
        val metrics = DisplayMetrics()
        val wm = context?.getSystemService(Context.WINDOW_SERVICE) as WindowManager
        val display = wm.defaultDisplay
        display.getMetrics(metrics)
        return metrics
    }


}